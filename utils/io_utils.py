import os
import json


def save_json(path, filename, dictionary, overwrite=True):
    try:
        if not isinstance(dictionary, dict):
            raise ValueError('{} is not a dictionary.'.format(dictionary))

        complete_filename = os.path.join(path, filename)
        if not os.path.isfile(filename):
            with open(complete_filename, 'w') as output_file:
                json.dump(dictionary, output_file,  indent=4, sort_keys=True)
                output_file.close()
        else:
            if overwrite:
                with open(complete_filename, 'w') as output_file:
                    json.dump(dictionary, output_file, indent=4, sort_keys=True)
                    output_file.close()
                print('Warning: {} was overwritten.'.format(filename))
            else:
                print('Warning: {} was NOT overwritten.'.format(filename))

    except (TypeError, ValueError) as err:
        return "Error: {}".format(err)


def load_json(path=None, filename=None, complete_filename=None):
    try:
        if path is None and filename is None:
            if complete_filename is None:
                raise ValueError("Specify complete path.")
            else:
                spec_filename = complete_filename
        elif complete_filename is None:
            if path is None and filename is None:
                raise ValueError("Specify path and filename.")
            else:
                spec_filename = os.path.join(path, filename)
        else:
            raise ValueError("No complete filename, nor path + filename given.")

        with open(spec_filename) as data_file:
            data = json.load(data_file, object_hook=custom_hook)
            data_file.close()
        return data
    except (TypeError, ValueError) as err:
        return "Error: {}".format(err)


def custom_hook(dct):
    # We need this because the JSON serialization does not allow for integer dict keys and tuples.
    if 'time_windows' in dct:
        for k in dct['time_windows']:
            v = dct['time_windows'][k]
            del dct['time_windows'][k]
            dct['time_windows'].update({int(k): (v[0], v[1])})

    return dct


if __name__ == "__main__":
    params = load_json(complete_filename="/Users/mvalenti/Downloads/temp.json")

    print params
