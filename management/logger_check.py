import os
from default_configs import PROJECT_DIR

if __name__ == "__main__":
    experiments_dir = os.path.join(PROJECT_DIR, "experiments_completed")
    log_unique_string = "training"

    for dir_name, subdir_list, file_list in os.walk(experiments_dir):
        for f in file_list:
            if log_unique_string in f:
                log_file_path = os.path.join(dir_name, f)
                with open(log_file_path, "r") as open_file:
                    for line in open_file:
                        if "Check" in line:
                            best_f1 = line.split(" ")[-1]
                            if float(best_f1) >= 1:
                                print("Found f1 higher than one at: {}".format(log_file_path))
