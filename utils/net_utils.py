import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from netcreator.architectures.autoencoders import KerasConvAutoEncoder
from keras import backend as K
from keras import optimizers
from keras.layers import Input, Flatten, Cropping1D
from keras.models import Model
from utils.custom_layers import AverageCombine, WeightScaleAndAdd
import numpy as np


def get_joint_network(net_dict, max_power_dict, mean_scale_factor=0., std_scale_factor=1., max_power_sum=0., stride=1,
                      constraint_window='max', freeze_layers=False):

    output_layers_dict = {}

    if constraint_window == "max":
        n_windows_dict, max_sequence_length = solve_window_problem(net_dict=net_dict, stride=stride)

        main_input = Input(shape=(max_sequence_length, net_dict[net_dict.keys()[-1]].model.input_shape[-1]),
                           name='main_input')

        for appliance, net in net_dict.items():
            input_layers_list = []
            appliance_input_length = net.model.input_shape[1]
            if n_windows_dict[appliance] == 1:
                input_layers_list.append(main_input)
            else:
                for i in range(n_windows_dict[appliance]):
                    left_cropping = i * stride
                    right_cropping = max_sequence_length - (appliance_input_length + i * stride)
                    input_layers_list.append(Cropping1D(cropping=(left_cropping, right_cropping))(main_input))

            output_layers_list = []
            for layer_nr, layer in enumerate(net.model.layers[1:]):
                if freeze_layers:
                    if layer_nr != len(net.model.layers):
                        layer.trainable = False

                for input_layer in input_layers_list:
                    output_layers_list.append(layer(input_layer))

                input_layers_list = output_layers_list
                output_layers_list = []

            output_layers_dict.update({appliance: input_layers_list})

        # Here we perform average concatenation for each sub-network
        for appliance, output_list in output_layers_dict.items():
            if len(output_list) == 1:
                output_layers_dict[appliance] = Flatten(name='{}_output'.format(appliance))(output_list[0])
            elif len(output_list) > 1:
                for layer_nr in range(len(output_list)):
                    output_list[layer_nr] = Flatten()(output_list[layer_nr])
                output_layers_dict[appliance] = AverageCombine(hop=stride,
                                                               name='{}_output'.format(appliance))(output_list)
            else:
                raise ValueError('Invalid number of outputs {} for appliance {}'
                                 ''.format(len(output_layers_dict[appliance]), appliance))

    elif constraint_window == 'min':
        min_length = 100000
        for appliance, net in net_dict.items():
            if net.model.input_shape[1] < min_length:
                min_length = net.model.input_shape[1]

        main_input = Input(shape=(min_length, net_dict[net_dict.keys()[-1]].model.input_shape[-1]),
                           name='main_input')

        x = main_input

        for appliance, net in net_dict.items():
            net_params = net.get_spec("net_params")
            net_name = str(net.get_spec("name"))
            net_input = [-1, min_length, net_dict[net_dict.keys()[-1]].model.input_shape[-1]]
            input_dims = {net_name: net_input}

            print("Preparing copy network with new input shape...")
            net_copy = KerasConvAutoEncoder(name=net_name, input_dims=input_dims, input_params=net_params)
            weights = net.model.get_weights()
            net_copy.model.set_weights(weights=weights)
            print("Done.")

            for layer in net_copy.model.layers[1:]:

                x = layer(x)

            output_layers_dict[appliance] = Flatten(name='{}_output'.format(appliance))(x)

    else:
        raise ValueError("Invalid constraint window: {}.".format(constraint_window))

    # Setup of all output layers: one per appliance's output and one for the weighted sum of such outputs
    outputs = []
    output_appliance_list = []
    max_power_list = []
    for appliance, output in output_layers_dict.items():
        outputs.append(output)
        max_power_list.append(max_power_dict[appliance])
        output_appliance_list.append(appliance)

    sum_layer = WeightScaleAndAdd(max_power_list,
                                  mean=mean_scale_factor,
                                  std=std_scale_factor,
                                  maximum=max_power_sum,
                                  name='sum_output')(outputs)

    outputs.append(sum_layer)

    # Instantiate model
    model = Model(inputs=main_input, outputs=outputs)

    return model, output_appliance_list


def get_constrained_models(net_dict, max_power_dict, appliance_list, stride=1, mean_scale_factor=0.,
                           std_scale_factor=1., max_power_sum=0.):

    model_dict = {}
    constrained_model_dict = {}

    n_windows_dict, max_sequence_length = solve_window_problem(net_dict=net_dict, stride=stride)

    for appliance, net in net_dict.items():
        model_input = Input(shape=(max_sequence_length, net.model.input_shape[-1]), name='{}_input'.format(appliance))

        input_layer_list = []
        appliance_input_length = net.model.input_shape[1]
        if n_windows_dict[appliance] == 1:
            input_layer_list.append(model_input)
        else:
            for i in range(n_windows_dict[appliance]):
                left_cropping = i * stride
                right_cropping = max_sequence_length - (appliance_input_length + i * stride)
                input_layer_list.append(Cropping1D(cropping=(left_cropping, right_cropping))(model_input))

        output_layer_list = []
        for layer_nr, layer in enumerate(net.model.layers[1:]):
            for input_layer in input_layer_list:
                output_layer_list.append(layer(input_layer))

            input_layer_list = output_layer_list
            output_layer_list = []

        output_layer = None
        if len(input_layer_list) == 1:
            output_layer = Flatten(name='{}_output'.format(appliance))(input_layer_list[0])
        elif len(input_layer_list) > 1:
            for layer_nr in range(len(input_layer_list)):
                input_layer_list[layer_nr] = Flatten()(input_layer_list[layer_nr])
                output_layer = AverageCombine(hop=stride, name='{}_output'.format(appliance))(input_layer_list)
        else:
            raise ValueError('Invalid number of outputs {} for appliance {}'
                             ''.format(len(input_layer_list), appliance))

        model_dict[appliance] = Model(inputs=model_input, outputs=output_layer)

        # Generate second model for back-propagating only the constraint loss.
        aggregate_inputs = []
        max_power_list = []
        for aggregate_input_appliance in appliance_list:
            if aggregate_input_appliance != appliance:
                aggregate_inputs.append(Input(shape=(max_sequence_length,),
                                              name='{}_input'.format(aggregate_input_appliance)))
            else:
                aggregate_inputs.append(output_layer)

            max_power_list.append(max_power_dict[aggregate_input_appliance])

        sum_layer = WeightScaleAndAdd(max_power_list,
                                      mean=mean_scale_factor,
                                      std=std_scale_factor,
                                      maximum=max_power_sum,
                                      name='{}_sum_output'.format(appliance))(aggregate_inputs)

        constrained_model_inputs = aggregate_inputs
        constrained_model_inputs[0] = model_input
        constrained_model_dict[appliance] = Model(inputs=constrained_model_inputs, outputs=sum_layer)

    return model_dict, constrained_model_dict


def compile_joined_net(model, output_appliance_list, constraint_params):
    optimizer = optimizers.get(constraint_params["optimizer"])

    # Prepare dictionary for model compilation
    loss_dict = {}
    loss_weight_dict = {}
    for appliance in output_appliance_list:
        dict_key = '{}_output'.format(appliance)
        loss_dict[dict_key] = anti_mean_squared_error
        loss_weight_dict[dict_key] = constraint_params["appliance_loss_weight"]

    loss_dict['sum_output'] = 'mse'
    loss_weight_dict['sum_output'] = constraint_params["sum_loss_weight"]

    model.compile(optimizer=optimizer, loss=loss_dict, loss_weights=loss_weight_dict)

    # optimizer.lr.set_value(constraint_params["learning_rate"])
    K.set_value(optimizer.lr, constraint_params["learning_rate"])

    return model


def solve_window_problem(net_dict, stride):
    # Check that all input dimensions and stride can give a feasible combination
    input_len_dict = {}
    max_len = 0
    for appliance, net in net_dict.items():
        input_len = net.model.input_shape[1]
        input_len_dict.update({appliance: input_len})
        if input_len > max_len:
            max_len = input_len

    max_attempts = 100  # Number of attempts to find a feasible combination
    attempt = 0
    solved = False
    n_windows = {}
    while attempt < max_attempts and not solved:
        attempted_max_len = max_len + stride * attempt
        for appliance, length in input_len_dict.items():
            if (attempted_max_len - length) % stride == 0:
                n_windows.update({appliance: (attempted_max_len - length) / stride + 1})
            else:
                break
        if len(n_windows) == len(input_len_dict):
            solved = True
        else:
            attempt += 1

    return n_windows, max_len


def calculate_energy_based_scores(y_true, y_pred, logger):
    n = min(len(y_true), len(y_pred))

    y_true = y_true[:n]
    y_pred = y_pred[:n]

    y_pred[y_pred < 0] = 0.

    sum_y_true = np.sum(y_true)
    sum_y_pred = np.sum(y_pred)

    flag = y_pred < y_true
    true_positives = np.sum(y_pred[flag]) + np.sum(y_true[~flag])

    try:
        eb_precision = float(true_positives / sum_y_pred)
    except Exception as exc:
        logger.info("WARNING, exception for energy based precision: {}.".format(exc))
        eb_precision = 1

    try:
        eb_recall = float(true_positives / sum_y_true)
    except Exception as exc:
        logger.info("WARNING, exception for energy based recall: {}.".format(exc))
        eb_recall = 1

    try:
        eb_f1_score = float(2 * eb_precision * eb_recall / (eb_precision + eb_recall))
    except Exception as exc:
        logger.info("WARNING, exception for energy based f1-score: {}.".format(exc))
        eb_f1_score = 0

    return eb_f1_score, eb_precision, eb_recall


def anti_mean_squared_error(y_true, y_pred):
    # return -T.mean(T.square(y_pred - y_true), axis=-1)
    return -K.mean(K.square(y_pred - y_true), axis=-1)


def train_loop(model, experiment_params, train_dataset, target_appliance, logger, x_valid, y_valid):
    learning_rate = experiment_params["learning_rate"]

    best_weights = model.get_weights()
    train_losses = []

    # F1 scores will all be referred to validation data
    f1_scores = []
    best_f1_score = 0
    no_improvement_counter = 0
    epoch = 1
    stop = False

    logger.info('Start training.')

    while epoch <= experiment_params["epochs"] and not stop:

        x_train, y_train = train_dataset.get_batch(target_appliance)
        train_losses.append(model.train_on_batch(x_train, y_train))

        # save data to compare
        # np.save("x_train_" + str(epoch) + ".npz", x_train)
        # np.save("y_train_" + str(epoch) + ".npz", y_train)

        if epoch % experiment_params["epoch_check"] == 0:
            no_improvement_counter += 1

            y_pred = model.predict(x_valid)
            f1_score, _, _ = calculate_energy_based_scores(y_true=y_valid, y_pred=y_pred, logger=logger)
            f1_scores.append(f1_score)

            logger.info("Check for epoch {} gave valid f1 score {}, best was: {} (AVG loss: {})".format(epoch, f1_scores[-1], best_f1_score, sum(train_losses)/len(train_losses)))
            train_losses = list()

            if f1_scores[-1] > best_f1_score:
                no_improvement_counter = 0
                best_f1_score = f1_scores[-1]
                best_weights = model.get_weights()
            else:
                no_improvement_counter += 1

            if no_improvement_counter == experiment_params["early_stopping"]:
                # stop = True
                if no_improvement_counter == experiment_params["early_stopping"]:
                    if experiment_params["auto_learning_rate_change"]:
                        valid_iteration = len(f1_scores)
                        local_min_f1_score = np.min(f1_scores[valid_iteration - experiment_params[
                            "early_stopping"] + 1:valid_iteration])

                        no_improvement_delta = (best_f1_score - local_min_f1_score)
                        # TODO change to relative non-improvement

                        logger.info("No improvement delta is {} for iteration {}.".format(str(no_improvement_delta),
                                                                                          str(len(f1_scores))))
                        if no_improvement_delta > experiment_params["max_no_improvement_delta"]:
                            no_improvement_counter = 0
                            learning_rate /= 10

                            model.set_weights(best_weights)

                            # model.optimizer.lr.set_value(learning_rate)
                            # optimizer = optimizers.sgd(lr=learning_rate, momentum=0.9, nesterov=True)
                            # net.compile(optimizer=optimizer, loss=experiment_params["loss"])

                            # with TF -> https://github.com/keras-team/keras/issues/898
                            K.set_value(model.optimizer.lr, learning_rate)

                            logger.info("Decreased learning rate to {} at iteration {}.".format(str(K.eval(model.optimizer.lr)), epoch))  # eval from optimizer to confirm lr reduction

                        else:
                            stop = True
                    else:
                        stop = True
        epoch += 1

    if stop:
        logger.info("Early stopping at iteration: {}".format(epoch - 1))

    train_output_dict = {"best_weights": best_weights, "last_epoch": epoch - 1}

    return train_output_dict


# if __name__ == "__main__":
#     from netcreator.searcher import get_network
#     from scripts.main_config import *
#
#     # Test code here
#     params = DEFAULT_PARAMS
#
#     appliance_info = {
#         'fridge': {
#             'input_dim': (None, 45, 1),
#             'output_dim': (None, 45, 1),
#         },
#         'heat_pump': {
#             'input_dim': (None, 90, 1),
#             'output_dim': (None, 90, 1),
#         }
#     }
#
#     net_dict = {}
#     for appliance in ['fridge', 'heat_pump']:
#         net = get_network(architecture=params["experiment"]["architecture"],
#                           input_dims=appliance_info[appliance]['input_dim'],
#                           output_dims=appliance_info[appliance]['output_dim'],
#                           input_params=params["configurations"][appliance],
#                           name=appliance)
#
#         net_dict[appliance] = net
#
#     mean = np.array([1100.0], dtype=np.float32)
#     std = np.array([950.0], dtype=np.float32)
#
#     model, appliances = join_nets(net_dict, {'fridge': 500, 'heat_pump': 2500}, 0., std[0], 3000., stride=15)
#
#     model.summary()
#
#     optimizer = optimizers.sgd(lr=0.01, momentum=0.9, nesterov=True)
#     loss = {'fridge_output': anti_mean_squared_error, 'heat_pump_output': anti_mean_squared_error,
#             'sum_output': 'mse'}
#
#     loss_weights = {'fridge_output': 0.01, 'heat_pump_output': 0.01, 'sum_output': 0.01}
#     model.compile(optimizer=optimizer, loss=loss, loss_weights=loss_weights)
#
#     x = np.ones((4, 90, 2), float)
#     y = [np.ones((1, 90), dtype=float), np.ones((1, 90), dtype=float), np.ones((1, 90), dtype=float)]
#
#     model.fit(x[0:1, :, :], y)
#     predictions = model.predict(x[0:1, :, :])
#
#     print predictions
