import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import yaml
from default_configs import DB_DIR
from utils.source_metadata import get_dataset_metadata
from tables.file import _open_files
import numpy as np
from utils.dataset_utils import get_dataset, read_dataset
from neuralnilm.metrics import run_metrics
from nilmtk import DataSet
import pandas as pd
from datetime import timedelta
from utils.interpolation import interp
import time


_NUS_CACHE = dict()  # key: bulding_1, buildin_2


def convert_csv_to_np(params):

    for appliance in params["appliances"]:

        test_dataset = get_dataset(params["experiment"]["dataset"])

        test_buildings = test_dataset.get_test_buildings(appliance=appliance, test_mode=params["test"]["test_mode"])

        for building in test_buildings:

            estimates_filename = os.path.join(params["paths"]["test_paths"][appliance],
                                              '{}_estimates_building_{}.csv'.format(appliance, str(building)))

            print("Converting:", estimates_filename)

            if not os.path.isfile(estimates_filename):
                print("Missing file.")
                continue

            estimates_filename_npy = os.path.join(params["paths"]["test_paths"][appliance],
                                                  '{}_estimates_building_{}.npy'.format(appliance, str(building)))

            if os.path.isfile(estimates_filename_npy):
                print("Already converted.")
                continue

            predictions = np.loadtxt(estimates_filename, delimiter=',')
            np.save(estimates_filename_npy, predictions)

            print("Done!")


def subsampled_predictions_from_os_to_nus(predictions, dataset_name, window, building, nus_subsampling_ratio=10, nus_expansion_ratio=10):

    sample_period = get_dataset_metadata(dataset_name, 'sample_period')

    temp_dataset_filename = os.path.join(DB_DIR, dataset_name + ".h5")

    dataset = DataSet(temp_dataset_filename)
    dataset.set_window(*window)
    elec = dataset.buildings[building].elec
    dataset_tz = dataset.metadata['timezone']

    os_indexes = pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period), tz=dataset_tz, closed='left')
    predictions = pd.Series(predictions[:len(os_indexes)], index=os_indexes)

    if 'building_{}'.format(building) not in _NUS_CACHE.keys():
        os_labels = pd.Series(0, index=os_indexes)

        # set to 1 the samples at subsample ratio
        subsampled_ratio_indexes = pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period * nus_subsampling_ratio),
                                                 tz=dataset.metadata.get('timezone'), closed='left')
        os_labels[subsampled_ratio_indexes] = 1

        # load ground truth for each appliance and specific building
        # for appl in APPLIANCES[dataset_name]:
        for appl in get_dataset_metadata(dataset_name, 'appliances_list'):

            # for REDD dataset missing "washer dryer" in building 2
            if dataset_name == 'redd' and building == 2 and appl == 'washer_dryer':
                continue

            csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.splitext(os.path.join(temp_dataset_filename))[0], building, appl.replace(' ', '_'))

            # avoid ambigous error for date near dst
            appliance_ground_truth = pd.Series.from_csv(csv_filename).tz_localize('UTC')
            w0 = pd.Timestamp(window[0]).tz_localize(tz=dataset_tz).tz_convert('UTC')
            w1 = pd.Timestamp(window[1]).tz_localize(tz=dataset_tz).tz_convert('UTC')
            appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index < w0], inplace=True)
            appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index > w1], inplace=True)
            appliance_ground_truth = appliance_ground_truth.tz_convert(tz=dataset_tz)

            for each in range(-nus_expansion_ratio // 2, nus_expansion_ratio // 2):
                # subsampled_labels[appliance_ground_truth.index + timedelta(seconds=each * sample_period)] = 1
                expansion_indices = appliance_ground_truth.index + timedelta(seconds=each * sample_period)
                # check if expansion indices in subsampled_labels range, remove outliers
                expansion_indices = expansion_indices[(expansion_indices >= os_labels.index[0]) & (expansion_indices <= os_labels.index[-1])]
                os_labels[expansion_indices] = 1

        nus_labels = os_labels[os_labels == 1].index
        _NUS_CACHE.update({'building_{}'.format(building): nus_labels})  # caching data

    else:
        nus_labels = _NUS_CACHE.get('building_{}'.format(building))  # retrieve data from cache

    series = predictions[nus_labels]

    return series.values


def evaluate(params):

    try:
        for appliance in params["appliances"]:

            scores_files = list()

            test_dataset = get_dataset(params["experiment"]["dataset"])

            test_buildings = test_dataset.get_test_buildings(appliance=appliance, test_mode=params["test"]["test_mode"])

            for building in test_buildings:
                print('Calculating scores for {}, building {}.'.format(appliance, building))

                scores_filename = os.path.join(params["paths"]["test_paths"][appliance],
                                               '{}_scores_building_{}_nus_sr{}_sr{}.yaml'.format(
                                                   appliance, building, params['apply_nus_subsampling_rate'], params['apply_nus_expansion_ratio']))

                if os.path.isfile(scores_filename):
                    print("Score file exist...skip.")
                    continue

                estimates_filename = os.path.join(params["paths"]["test_paths"][appliance],
                                                  '{}_estimates_building_{}.npy'.format(appliance, str(building)))  # or .csv

                if not os.path.isfile(estimates_filename):
                    print("File does not exist:", estimates_filename)
                    continue

                # t0 = time.time()
                # predictions = np.loadtxt(estimates_filename, delimiter=',')
                predictions = np.load(estimates_filename)
                # print("Load NPY time:", time.time() - t0)

                if params['us_enabled']:
                    # upsample data to OS before NUS eval
                    r = params.get("us_subsampling_ratio")
                    predictions = interp(predictions, r, alpha=0.5)
                    predictions[predictions < 0] = 0

                # apply nus decision to OS
                # t0 = time.time()
                predictions = subsampled_predictions_from_os_to_nus(predictions,
                                                                    dataset_name=params["experiment"]["dataset"],
                                                                    window=params["test"]["time_windows"][building],
                                                                    building=building,
                                                                    nus_subsampling_ratio=params.get("apply_nus_subsampling_rate"),
                                                                    nus_expansion_ratio=params.get("apply_nus_expansion_ratio"))

                # print("Ground1 time:", time.time() - t0)
                # t0 = time.time()

                ground_truth = read_dataset(dataset_name=params["experiment"]["dataset"],
                                            window=params["test"]["time_windows"][building],
                                            trace=[appliance],
                                            building=building,
                                            experiment_id=params["ids"]["hashed_id"],
                                            nus_enabled=True,
                                            nus_subsampling_ratio=params.get("apply_nus_subsampling_rate"),
                                            nus_expansion_ratio=params.get("apply_nus_expansion_ratio"))

                mains = list()

                # print("Ground2 time:", time.time() - t0)
                # t0 = time.time()

                scores = run_metrics(y_true=ground_truth, y_pred=predictions, mains=mains)
                # print("Scores time:", time.time() - t0)

                with open(scores_filename, 'w') as fh:
                    yaml.dump(scores, stream=fh, default_flow_style=False)
                    fh.close()
                # print("Scores saved.")
                # print("Energy based precision: {}".format(scores['precision_score_(energy_based)']))
                # print("Energy based recall: {}".format(scores['recall_score_(energy_based)']))
                # print("Energy based f1: {}".format(scores['f1_score_(energy_based)']))

                scores_files.append(scores_filename)

            if len(test_buildings) > 1 and len(scores_files) > 1:
                # avg scores

                # scores_files = os.listdir(os.path.join(params["paths"]["test_paths"][appliance]))
                precision = list()
                recall = list()
                appliance_name = ""

                for test_file in scores_files:

                    with open(test_file, 'r') as f:
                        scores = yaml.load(f)

                    precision.append(scores['precision_score_(energy_based)'])
                    recall.append(scores['recall_score_(energy_based)'])

                # average scores
                mean_precision = sum(precision) / len(precision)
                mean_recall = sum(recall) / len(recall)

                if mean_precision == 0 or mean_recall == 0:
                    mean_f1 = 0
                else:
                    mean_f1 = (2 * mean_precision * mean_recall) / (mean_recall + mean_precision)

                avg_scores = {
                    'precision_score_(energy_based)': mean_precision,
                    'recall_score_(energy_based)': mean_recall,
                    'f1_score_(energy_based)': mean_f1
                }

                avg_scores_filename = os.path.join(params["paths"]["test_paths"][appliance],
                                                   '{}_scores_avg_nus_sr{}_sr{}.yaml'.format(
                                                       appliance, params['apply_nus_subsampling_rate'], params['apply_nus_expansion_ratio']))

                with open(avg_scores_filename, 'w') as f:
                    yaml.dump(avg_scores, stream=f, default_flow_style=False)

                print("AVG scores:")
                print("Energy based precision: {}".format(avg_scores['precision_score_(energy_based)']))
                print("Energy based recall: {}".format(avg_scores['recall_score_(energy_based)']))
                print("Energy based f1: {}".format(avg_scores['f1_score_(energy_based)']))

    except Exception as err:
        print("Unhandled error:")
        print(err)


if __name__ == '__main__':

    EXP_DIR = '/home/marco/Workspace/Univ/NILM/Test vari/experiments_resutls_upsample_subsampling/experiments_ukdale_denoised_sr20/'

    strides = [1, 8, 16, 32]
    test_modes = ['seen', 'unseen']
    disag_types = ['median', 'mean']

    for stride in strides:
        for test_mode in test_modes:
            for disag_type in disag_types:
                for dir in os.listdir(EXP_DIR):
                    if os.path.isdir(os.path.join(EXP_DIR, dir)):
                        for appliance in os.listdir(os.path.join(EXP_DIR, dir)):
                            if os.path.isdir(os.path.join(EXP_DIR, dir, appliance)) and 'test' not in appliance.split('_'):

                                file_config = os.path.join(EXP_DIR, dir, appliance, 'config.yaml')
                                with open(file_config, 'r') as f:
                                    params = yaml.load(f)

                                params["phases"]["train"] = False
                                params["experiment"]["disag_type"] = disag_type  # 'median' or 'mean'

                                # fix paths
                                params['paths']['dataset_path'] = DB_DIR
                                params['paths']['appliance_paths'][appliance] = os.path.join(EXP_DIR, dir, appliance)
                                params['paths']['base_paths'] = EXP_DIR
                                params['paths']['run_paths'][appliance] = os.path.join(EXP_DIR, dir)

                                params["test"]["stride"] = stride
                                params["test"]["test_mode"] = test_mode

                                appliance_test_folder = os.path.join(EXP_DIR, dir, appliance + '_test_' + test_mode + '_' + disag_type + '_stride_' + str(stride))
                                params['paths']['test_paths'][appliance] = appliance_test_folder

                                params["ids"]["hashed_id"] = 'test_' + test_mode + '_' + disag_type + '_stride_' + str(stride)

                                params["experiment"]["source_type"] = "same_location_source"

                                # overload parameters to get sub-sampled (uniform or non-uniform) ground_truth
                                # or keep original params
                                # params['nus_enabled'] = False
                                # params['nus_subsampling_ratio'] = 20
                                # params['nus_expansion_ratio'] = 20
                                #
                                # params['us_enabled'] = True
                                # params['us_subsampling_ratio'] = 10

                                if params['nus_enabled']:
                                    raise ValueError("This experiment can not be performed over NUS data.")

                                params['apply_nus_subsampling_rate'] = 20
                                params['apply_nus_expansion_ratio'] = 20
                                # 5-5, 10-5, 10-10, 20-10, 20-20

                                print(appliance_test_folder)

                                metadata = get_dataset_metadata(params['experiment']['dataset'], appliance)

                                # dir exist
                                if os.path.isdir(appliance_test_folder):

                                    print("\n\nEvaluating " + appliance + "\n\n")
                                    evaluate(params)

                                # free memory from h5 temp data
                                _open_files.close_all()

                                # convert_csv_to_np(params)

    print('DONE.')
