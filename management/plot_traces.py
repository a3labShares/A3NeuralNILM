import os
import matplotlib.pyplot as plt
from default_configs import PROJECT_DIR
from utils.dataset_utils import read_dataset

traces = ["washing_machine", "aggregate"]
power_types = ["active"]
mains_mode = 'noised'
dataset = 'ukdale'
building = 2
time_window = ('2013-09-27', '2013-10-10')

start_end = (0, -1)

output_dir = os.path.join(PROJECT_DIR, 'plots')

plt.figure(1, figsize=(12, 5), dpi=300)
legend = []
figure_name = "{}_build_{}_start_{}_end_{}".format(dataset, building, time_window[0], time_window[1])

for trace in traces:
    plt.figure(1, figsize=(12, 5), dpi=300)
    legend = []
    figure_name = "{}_{}_build_{}_start_{}_end_{}".format(dataset, trace, building, start_end[0], start_end[1])

    for power_type in power_types:
        data = read_dataset(
            dataset_name=dataset,
            trace=trace,
            building=building,
            experiment_id=42,
            window=time_window,
            power_type=power_type
        )

        plt.plot(data[start_end[0]: start_end[1]])

        legend.append("{}_{}".format(trace, power_type))

    plt.legend(legend, loc='upper right')

    figure_name = os.path.join(
        output_dir,
        figure_name)

    plt.savefig(figure_name)

    plt.close()
