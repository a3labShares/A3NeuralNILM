import yaml
import os
from utils.io_utils import load_json

PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

if __name__ == "__main__":
    temp_run_paths = os.listdir(os.path.join(PROJECT_DIR, "experiments"))
    run_paths = []
    for temp_run_path in temp_run_paths:
        if temp_run_path.split('_')[0] == 'config':
            run_paths.append(temp_run_path)

    best_f1_scores = {}

    for run_path in run_paths:
        complete_run_path = os.path.join(PROJECT_DIR, "experiments", run_path)
        temp_paths = os.listdir(os.path.join(PROJECT_DIR, "experiments", run_path))
        complete_test_paths = []
        for temp_path in temp_paths:
            if temp_path.split('_')[0] == 'test':
                complete_test_paths.append(os.path.join(complete_run_path, temp_path))

        for complete_test_path in complete_test_paths:
            test_files = os.listdir(complete_test_path)
            for test_file in test_files:
                if 'scores' in test_file.split('_'):
                    with open(os.path.join(complete_test_path, test_file), 'r') as stream:
                        try:
                            scores = yaml.load(stream)
                        except yaml.YAMLError as exc:
                            raise Exception(exc)

                    f1_score = scores['f1_score_(energy_based)']

                    run_params = load_json(path=complete_run_path, filename='run.json')
                    test_params = load_json(path=complete_test_path, filename='test.json')

                    dataset = run_params["experiment"]["dataset"]

                    # 'u' stands for 'unconstrained' and 'c' for 'constrained'
                    if run_params["experiment"]["constrained_train"]:
                        if test_params["test"]["constrained_test"]:
                            score_field = "c_train_c_test"
                        else:
                            score_field = "c_train_u_test"
                    else:
                        if test_params["test"]["constrained_test"]:
                            score_field = "u_train_c_test"
                        else:
                            score_field = "u_train_u_test"

                    i = 1
                    appliance = test_file.split('_')[0]
                    while test_file.split('_')[i] != 'scores':
                        appliance += '_{}'.format(test_file.split('_')[i])
                        i += 1

                    if dataset not in best_f1_scores.keys():
                        best_f1_scores[dataset] = {}

                    if score_field not in best_f1_scores[dataset].keys():
                        best_f1_scores[dataset][score_field] = {}

                    if appliance not in best_f1_scores[dataset][score_field]:
                        best_f1_scores[dataset][score_field][appliance] = {'f1': 0,
                                                                           'precision': 0,
                                                                           'recall': 0,
                                                                           'test_params': {},
                                                                           'run_params': {},
                                                                           'test_path': ''}

                    if best_f1_scores[dataset][score_field][appliance]['f1'] < f1_score <= 1:
                        best_f1_scores[dataset][score_field][appliance]['f1'] = f1_score
                        best_f1_scores[dataset][score_field][appliance]['precision'] = scores['precision_score_(energy_based)']
                        best_f1_scores[dataset][score_field][appliance]['recall'] = scores['recall_score_(energy_based)']
                        best_f1_scores[dataset][score_field][appliance]['test_params'] = test_params
                        best_f1_scores[dataset][score_field][appliance]['run_params'] = run_params
                        best_f1_scores[dataset][score_field][appliance]['test_path'] = complete_test_path

    for dataset in best_f1_scores.keys():
        print('DATASET: {}'.format(dataset))
        print('')
        for score_field in best_f1_scores[dataset].keys():
            print('Train/Test type: {}. (u = unconstrained / c = constrained)'.format(score_field))
            print('')
            for appliance in best_f1_scores[dataset][score_field].keys():
                print('Appliance: {}'.format(appliance))
                print('Precision: {}.'.format(best_f1_scores[dataset][score_field][appliance]['precision']))
                print('Recall: {}.'.format(best_f1_scores[dataset][score_field][appliance]['recall']))
                print('F1: {}.'.format(best_f1_scores[dataset][score_field][appliance]['f1']))
                print('Test path: {}:'.format(best_f1_scores[dataset][score_field][appliance]['test_path']))
                print('')
