from __future__ import division
import yaml
import os


def avg_scores(scores_dir, not_in='upsampled.yaml'):

    scores_files = os.listdir(scores_dir)
    precision = list()
    recall = list()
    appliance_name = ""

    for test_file in scores_files:
        if 'scores' in test_file.split('_') and not_in not in test_file.split('_'):

            if not appliance_name:
                appliance_name = test_file.split('_scores_')[0]

            with open(os.path.join(scores_dir, test_file), 'r') as stream:
                try:
                    scores = yaml.load(stream)
                except yaml.YAMLError as exc:
                    raise Exception(exc)

            precision.append(scores['precision_score_(energy_based)'])
            recall.append(scores['recall_score_(energy_based)'])

    # average scores
    if precision and recall:
        mean_precision = sum(precision) / len(precision)
        mean_recall = sum(recall) / len(recall)
    else:
        print("Empty folder: " + scores_dir)
        return

    if mean_precision == 0 or mean_recall == 0:
        mean_f1 = 0
    else:
        mean_f1 = (2 * mean_precision * mean_recall) / (mean_recall + mean_precision)

    avg_scores = {
        'precision_score_(energy_based)': mean_precision,
        'recall_score_(energy_based)': mean_recall,
        'f1_score_(energy_based)': mean_f1
    }

    with open(os.path.join(scores_dir, appliance_name + '_scores_avg.yaml'), 'w') as f:
        yaml.dump(avg_scores, stream=f, default_flow_style=False)


if __name__ == '__main__':

    # PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    EXP_DIR = '/home/marco/Workspace/Univ/NILM/Test vari/REDD_experiments_results_upsample_subsampling/experiments_redd_denoised_sr20_er20/'

    strides = [1, 8, 16, 32]
    test_modes = ['seen']
    disag_types = ['median', 'mean']

    for stride in strides:
        for test_mode in test_modes:
            for disag_type in disag_types:
                for dir in os.listdir(EXP_DIR):
                    if os.path.isdir(os.path.join(EXP_DIR, dir)):
                        for appliance in os.listdir(os.path.join(EXP_DIR, dir)):
                            if os.path.isdir(os.path.join(EXP_DIR, dir, appliance)) and 'test' not in appliance.split('_'):

                                appliance_test_folder = os.path.join(EXP_DIR, dir, appliance + '_test_' + test_mode + "_" + disag_type + '_stride_' + str(stride))
                                # appliance_test_folder = os.path.join(EXP_DIR, dir, appliance + '_test_' + disag_type + '_stride_' + str(stride))  # testing

                                if os.path.isdir(appliance_test_folder):
                                    print(appliance_test_folder)
                                    avg_scores(appliance_test_folder)
