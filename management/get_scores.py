import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import argparse
import yaml
import string
from collections import OrderedDict
from default_configs import PROJECT_DIR


def print_ordered_scores(scores_dict, appliance_to_print='all', scores_to_print=None):

    score_String = ''

    output_string = '#' * 200 + '\n'
    output_string += 'Scores legend:\n'
    for i, v in enumerate(scores_to_print):
        output_string += string.ascii_uppercase[i] + ': ' + v + '\n'
        score_String += '{:<20}'.format(string.ascii_uppercase[i])

    print_appliance = True

    best_score = dict()

    for each_config, config_value in scores_dict.iteritems():
        print_config = True
        for each_test, test_value in config_value.iteritems():
            for each_appliance, appliance_value in test_value.iteritems():

                if appliance_to_print is 'all' or each_appliance in appliance_to_print:

                    if print_appliance:

                        output_string += '#' * 200 + '\n'
                        output_string += each_appliance + '\n'
                        output_string += '{:<40}'.format('') + score_String + '\n'
                        output_string += '-' * 200 + '\n'
                        print_appliance = False

                    if print_config:
                        output_string += each_config + '\n'
                        output_string += '-' * 200 + '\n'
                        print_config = False

                    for each_building, scores in appliance_value.iteritems():
                        # scores
                        output_string += each_test + '\n'
                        output_string += each_building + '{:<30}'.format('') + ''.join('{:<20}'.format(x) for x in scores) + '\n'  # round(x, 4)

                        if scores[0] > 1.0:
                            # avoid comparison if f1_score is wrong
                            continue

                        else:
                            if not best_score:
                                best_score = {'config': each_config,
                                              'test': each_test,
                                              'scores': scores,
                                              'scores type': scores_to_print}
                            else:
                                # compare f1 scores, save new
                                if best_score['scores'][0] < scores[0]:
                                    best_score = {'config': each_config,
                                                  'test': each_test,
                                                  'scores': scores,
                                                  'scores type': scores_to_print}

                    output_string += '-' * 200 + '\n'

    output_string += '-' * 200 + '\n'
    output_string += '-' * 200 + '\n'
    output_string = ''
    output_string += '{:<60}'.format('Best score') + score_String + '\n'
    output_string += best_score['config'] + '/' + best_score['test'] + '{:<9}\n'.format('')  # +''.join('{:<20}\n'.format(x) for x in best_score['scores']) + '\n'  # round(x, 4)
    output_string += ''.join('{:<20}\n'.format(best_score['scores'][1]))  # precision
    output_string += ''.join('{:<20}\n'.format(best_score['scores'][2]))  # recall
    output_string += ''.join('{:<20}\n'.format(best_score['scores'][0]))  # f1
    output_string += '-' * 200 + '\n'
    output_string += '-' * 200 + '\n'

    print(output_string)


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='NeuralNILM Launcher')
    parser.add_argument('--dataset', default='ampds')  # used to get appliances list
    parser.add_argument('--appliance', default='all')
    parser.add_argument('--exp-dir', default=os.path.join(PROJECT_DIR, 'experiments'))
    parser.add_argument('--exp-prefix', default='config')
    parser.add_argument('--exp-prefix-test', default='test_unseen')  # test_seen_median, test_seen_mean, test_unseen_median, test_unseen_mean
    parser.add_argument('--multi-buildings', action='store_true', default=False)  # if present arg --multi-building set to True (as bool), otherwise False
    params = parser.parse_args()

    # testing
    params.exp_dir = '/home/marco/Workspace/Univ/NILM/Test vari/REDD_experiments_results_upsample_subsampling/experiments_redd_denoised_sr20_er20/'
    params.multi_buildings = False
    params.exp_prefix_test = 'test_unseen'
    # for alt_suffix in ['_nus_sr5_sr5', '_nus_sr10_sr5', '_nus_sr10_sr10', '_nus_sr20_sr10', '_nus_sr20_sr20']:
    for alt_suffix in ['_upsampled.yaml']:
    # for alt_suffix in ['.yaml']:
        # alt_suffix = '_nus_sr20_sr20'  # '_us_sr5'

        # collect all config dirs
        scores_dict = dict()
        # scores_order = ['accuracy_score', 'f1_score', 'f1_score_(energy_based)', 'mean_absolute_error', 'precision_score', 'precision_score_(energy_based)',
        #                 'recall_score', 'recall_score_(energy_based)', 'relative_error_in_total_energy', 'sum_abs_diff', 'total_energy_correctly_assigned']

        scores_order = ['f1_score_(energy_based)', 'precision_score_(energy_based)', 'recall_score_(energy_based)']
        appliances_list = list()

        for each_config in os.listdir(params.exp_dir):
            if each_config.split('_')[0] == params.exp_prefix:

                scores_dict.update({each_config: OrderedDict()})

                for each_test in os.listdir(os.path.join(params.exp_dir, each_config)):
                    # if params.exp_prefix_test in each_test.split('_'):
                    if params.exp_prefix_test in each_test:

                        scores_dict[each_config].update({each_test: OrderedDict()})

                        for each_appliance in os.listdir(os.path.join(params.exp_dir, each_config, each_test)):

                            if (params.appliance == 'all' or params.appliance in each_appliance) and 'scores' in each_appliance:

                                appliance_name = each_appliance.split('_scores_')[0]
                                if params.multi_buildings and 'avg' + alt_suffix in each_appliance:
                                    building_name = 'avg'

                                elif not params.multi_buildings and alt_suffix in each_appliance and 'avg' not in each_appliance: #and 'upsampled' not in each_appliance:
                                    building_name = each_appliance.split('_scores_')[1].split('.yaml')[0]
                                else:
                                    continue

                                if appliance_name not in appliances_list + ['']:
                                    appliances_list.append(appliance_name)

                                # print(os.path.join(params.exp_dir, each_config, each_test, each_appliance))

                                tmp_scores_list = list()
                                with open(os.path.join(params.exp_dir, each_config, each_test, each_appliance), 'r') as f:
                                    try:
                                        tmp = yaml.load(f)
                                        for k in scores_order:
                                            tmp_scores_list.append(tmp.get(k))

                                    except yaml.YAMLError as exc:
                                        raise Exception(exc)

                                # if scores_dict.get(each_config).get(each_test).get(each_appliance):
                                #     # if appliance exist insert new building
                                #     scores_dict[each_config][each_test][appliance_name].update({building_name: tmp_scores_list})
                                # else:
                                # insert building
                                scores_dict[each_config][each_test].update({appliance_name: {building_name: tmp_scores_list}})

                                break

        for appliance in appliances_list:
            print(appliance)
            print_ordered_scores(scores_dict, appliance_to_print=appliance, scores_to_print=scores_order)

        print("\n")

