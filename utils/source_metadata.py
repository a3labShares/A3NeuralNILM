import numpy as np


def get_dataset_metadata(dataset_name=None, mtype=None):
    if dataset_name == 'ukdale':
        return get_ukdale_metadata(mtype)

    elif dataset_name == 'ampds':
        return get_ampds_metadata(mtype)

    elif dataset_name == 'redd':
        return get_redd_metadata(mtype)

    else:
        raise ValueError('{} dataset not supported.'.format(dataset_name))


def get_ampds_metadata(mtype=None):

    SAMPLE_PERIOD = 60
    APPLIANCES = ["dish_washer", "electric_oven", "fridge", "heat_pump", "tumble_dryer", "washing_machine"]
    TIME_WINDOWS = {
        "train": {1: ('2012-10-01', '2014-04-01')},
        "test": {1: ('2012-04-01', '2012-10-01')}
    }

    APPLIANCE_METADATA = {
        'input_stats': {
            'mean': [np.array([1100.0], dtype=np.float32), np.array([200.0], dtype=np.float32)],
            'std': [np.array([950.0], dtype=np.float32), np.array([100.0], dtype=np.float32)]
        },
        'train_building_real': None,
    }

    if mtype == 'appliances_list':
        return APPLIANCES

    elif mtype == 'time_windows':
        return TIME_WINDOWS

    elif mtype == 'sample_period':
        return SAMPLE_PERIOD

    elif mtype == 'dish_washer':
        APPLIANCE_METADATA.update(
            {
                'seq_length': 210,
                'train_buildings': [1],
                'validation_buildings': [1],
                'test_buildings': {
                    'seen': [1],
                },
                'appliance': 'dish washer',
                'max_appliance_power': 900,
                'on_power_threshold': 2,
                'min_on_duration': 300,
                'min_off_duration': 480,
            }
        )
        return APPLIANCE_METADATA

    elif mtype == 'electric_oven':
        APPLIANCE_METADATA.update(
            {
                'seq_length': 120,
                'train_buildings': [1],
                'validation_buildings': [1],
                'test_buildings': {
                    'seen': [1],
                },
                'appliance': 'electric oven',
                'max_appliance_power': 3900,
                'on_power_threshold': 30,
                'min_on_duration': 120,
                'min_off_duration': 480,
            }
        )
        return APPLIANCE_METADATA

    elif mtype == 'fridge':
        APPLIANCE_METADATA.update(
            {
                'seq_length': 45,
                'train_buildings': [1],
                'validation_buildings': [1],
                'test_buildings': {
                    'seen': [1],
                },
                'appliance': 'fridge',
                'max_appliance_power': 500,
                'on_power_threshold': 50,
                'min_on_duration': 60,
                'min_off_duration': 60,
            }
        )
        return APPLIANCE_METADATA

    elif mtype == 'heat_pump':
        APPLIANCE_METADATA.update(
            {
                'seq_length': 90,
                'train_buildings': [1],
                'validation_buildings': [1],
                'test_buildings': {
                    'seen': [1],
                },
                'appliance': 'heat pump',
                'max_appliance_power': 2500,
                'on_power_threshold': 60,
                'min_on_duration': 60,
                'min_off_duration': 60,
            }
        )
        return APPLIANCE_METADATA

    elif mtype == 'tumble_dryer':
        APPLIANCE_METADATA.update(
            {
                'seq_length': 75,
                'train_buildings': [1],
                'validation_buildings': [1],
                'test_buildings': {
                    'seen': [1],
                },
                'appliance': 'tumble dryer',
                'max_appliance_power': 5000,
                'on_power_threshold': 150,
                'min_on_duration': 60,
                'min_off_duration': 600,
            }
        )
        return APPLIANCE_METADATA

    elif mtype == 'washing_machine':
        APPLIANCE_METADATA.update(
            {
                'seq_length': 120,
                'train_buildings': [1],
                'validation_buildings': [1],
                'test_buildings': {
                    'seen': [1],
                },
                'appliance': 'washing machine',
                'max_appliance_power': 600,
                'on_power_threshold': 20,
                'min_on_duration': 120,
                'min_off_duration': 540,
            }
        )
        return APPLIANCE_METADATA

    else:
        raise ValueError('{} metadata type or appliance not supported.'.format(mtype))


def get_redd_metadata(mtype=None):

    # tmp: REDD subsampled at 6 sec, thus seq_length reduced by an half
    SAMPLE_PERIOD = 3 * 2
    APPLIANCES = ["fridge", "microwave", "dish_washer", "washer_dryer"]
    TIME_WINDOWS = {
        "train": {
            1: ("2011-04-18", "2011-05-20"),
            2: ("2011-04-17", "2011-04-29"),
            3: ("2011-04-16", "2011-04-28")
        },
        "test": {
            1: ("2011-05-21", "2011-05-24"),
            2: ("2011-04-30", "2011-05-01"),
            3: ("2011-05-21", "2011-05-27")
        }
    }

    APPLIANCE_METADATA = {
        'input_stats': {
            'mean': [np.array([216.0955], dtype=np.float32)],  # missing reactive stats
            'std': [np.array([345.5635], dtype=np.float32)]
        },
        'train_building_real': None,
    }

    if mtype == 'appliances_list':
        return APPLIANCES

    elif mtype == 'time_windows':
        return TIME_WINDOWS

    elif mtype == 'sample_period':
        return SAMPLE_PERIOD

    elif mtype == 'dish_washer':
        APPLIANCE_METADATA.update(
            {
                'seq_length': 2304 // 2,
                'train_buildings': [1, 2],
                'validation_buildings': [1, 2],
                'test_buildings': {
                    'seen': [1, 2],
                    'unseen': [3]
                },
                'appliance': 'dish washer',
                'max_appliance_power': 1500,
                'on_power_threshold': 80,
                'min_on_duration': 1200,
                'min_off_duration': 20000,
            }
        )
        return APPLIANCE_METADATA

    elif mtype == 'microwave':
        APPLIANCE_METADATA.update(
            {
                'seq_length': 96 // 2,
                'train_buildings': [1, 2],
                'validation_buildings': [1, 2],
                'test_buildings': {
                    'seen': [1, 2],
                    'unseen': [3]
                },
                'appliance': 'microwave',
                'max_appliance_power': 2000,
                'on_power_threshold': 70,
                'min_on_duration': 10,
                'min_off_duration': 10,
            }
        )
        return APPLIANCE_METADATA

    elif mtype == 'fridge':
        APPLIANCE_METADATA.update(
            {
                'seq_length': 496 // 2,
                'train_buildings': [1, 2],
                'validation_buildings': [1, 2],
                'test_buildings': {
                    'seen': [1, 2],
                    'unseen': [3]
                },
                'appliance': ['fridge freezer', 'fridge', 'freezer'],
                'max_appliance_power': 2400,
                'on_power_threshold': 70,
                'min_on_duration': 450,
                'min_off_duration': 1100,
            }
        )
        return APPLIANCE_METADATA

    elif mtype == 'washer_dryer':
        APPLIANCE_METADATA.update(
            {
                'seq_length': 1536 // 2,
                'train_buildings': [1],
                'validation_buildings': [1],
                'test_buildings': {
                    'seen': [1],
                    'unseen': [3]
                },
                'appliance': 'washer dryer',
                'max_appliance_power': 3200,
                'on_power_threshold': 500,
                'min_on_duration': 1200,
                'min_off_duration': 1800,
            }
        )
        return APPLIANCE_METADATA

    else:
        raise ValueError('{} metadata type or appliance not supported.'.format(mtype))


def get_ukdale_metadata(mtype=None):

    SAMPLE_PERIOD = 6
    APPLIANCES = ["fridge", "microwave", "kettle", "dish_washer", "washing_machine"]
    TIME_WINDOWS = {
        "train": {
            1: ('2013-04-12', '2014-10-21'),
            2: ('2013-05-22', '2013-09-26'),
            3: ('2013-02-27', '2013-03-25'),
            4: ('2013-03-09', '2013-09-11'),
            5: ('2014-06-29', '2014-09-01')
        },
        "test": {
            1: ('2014-10-22', '2014-12-15'),
            2: ('2013-09-27', '2013-10-10'),
            3: ('2013-03-25', '2013-04-01'),
            4: ('2013-09-11', '2013-10-01'),
            5: ('2014-09-01', '2014-09-07')
        }
    }

    APPLIANCE_METADATA = {
        'input_stats': {
            'mean': [np.array([297.87216187], dtype=np.float32), np.array([174.0], dtype=np.float32)],
            'std': [np.array([374.43884277], dtype=np.float32), np.array([151.0], dtype=np.float32)]
        },
        'train_building_real': None,
    }

    if mtype == 'appliances_list':
        return APPLIANCES

    elif mtype == 'time_windows':
        return TIME_WINDOWS

    elif mtype == 'sample_period':
        return SAMPLE_PERIOD

    elif mtype == 'microwave':
        APPLIANCE_METADATA.update(
            {
                'seq_length': 288,
                'train_buildings': [1, 2],
                'validation_buildings': [1, 2],
                'test_buildings': {
                    'seen': [1, 2],
                    'unseen': [5]
                },
                'appliance': 'microwave',
                'max_appliance_power': 3000,
                'on_power_threshold': 200,
                'min_on_duration': 12,
                'min_off_duration': 30,
            }
        )
        return APPLIANCE_METADATA

    elif mtype == 'washing_machine':
        APPLIANCE_METADATA.update(
            {
                'seq_length': 1024,
                'train_buildings': [1, 5],
                'validation_buildings': [1, 5],
                'test_buildings': {
                    'seen': [1, 5],
                    'unseen': [2]
                },
                'appliance': ['washer dryer', 'washing machine'],
                'max_appliance_power': 2500,
                'on_power_threshold': 20,
                'min_on_duration': 1800,
                'min_off_duration': 160,
            }
        )
        return APPLIANCE_METADATA

    elif mtype == 'fridge':
        APPLIANCE_METADATA.update(
            {
                'seq_length': 512,
                'train_buildings': [1, 2],
                'validation_buildings': [1, 2],
                'test_buildings': {
                    'seen': [1, 2],
                    'unseen': [5]
                },
                'appliance': ['fridge freezer', 'fridge', 'freezer'],
                'max_appliance_power': 300,
                'on_power_threshold': 50,
                'min_on_duration': 60,
                'min_off_duration': 12,
            }
        )
        return APPLIANCE_METADATA

    elif mtype == 'kettle':
        APPLIANCE_METADATA.update(
            {
                'seq_length': 128,
                'train_buildings': [1, 2],
                'validation_buildings': [1, 2],
                'test_buildings': {
                    'seen': [1, 2],
                    'unseen': [5]
                },
                'appliance': 'kettle',
                'max_appliance_power': 3100,
                'on_power_threshold': 2000,
                'min_on_duration': 12,
                'min_off_duration': 0,
            }
        )
        return APPLIANCE_METADATA

    elif mtype == 'dish_washer':
        APPLIANCE_METADATA.update(
            {
                'seq_length': 1024 + 512,
                'train_buildings': [1, 2],
                'validation_buildings': [1, 2],
                'test_buildings': {
                    'seen': [1, 2],
                    'unseen': [5]
                },
                'appliance': 'dish washer',
                'max_appliance_power': 2500,
                'on_power_threshold': 10,
                'min_on_duration': 1800,
                'min_off_duration': 1800,
            }
        )
        return APPLIANCE_METADATA

    else:
        raise ValueError('{} metadata type or appliance not supported.'.format(mtype))
