import os
import traceback
import numpy as np
import pandas as pd
from nilmtk import DataSet
import matplotlib.pyplot as plt
from default_configs import DB_DIR

# script description: in this script, each dataset is analyzed in order to generate the state transition sequence for each appliance ground truth consumption trace.
# in order to generate this output, the working state consumption for each appliance, in each building, has been given
# callable function: working_state_extr(datasets_list_name, all_appliance_working_states)
# output: statetransition_series, pd.Series() with 1st sample index after state transition

# list of datasets to be analyzed
datasets_list = {'REDD': 'redd.h5',
                 'AMPds': 'ampds.h5',
                 'UK-DALE': 'ukdale.h5',
                 }

datasets_samples_period = {'REDD': 6,  # tmp: subsample origin code
                           'UK-DALE': 6,
                           'AMPds': 60
                           }

all_appliance_working_states = {'UK-DALE': {1: {'fridge': {'mu': [87.57, 0],
                                                           'std': [21.25, 0.08],
                                                           },
                                                'washing machine': {'mu': [1900.83, 359.84, 179.79, 0],
                                                                    'std': [150.54, 83.72, 43.85, 24.40],
                                                                    },
                                                'dish washer': {'mu': [2343.57, 121.88, 0],
                                                                'std': [63.85, 12.53, 11.31],
                                                                },
                                                'kettle': {'mu': [2334.42, 0],
                                                           'std': [44.05, 33.88],
                                                           },
                                                'microwave': {'mu':  [1371.48, 0],
                                                              'std': [209.015, 50.55],
                                                              },
                                                },
                                            2: {'fridge': {'mu': [90.64, 0],
                                                           'std': [55.48, 0.56],
                                                           },
                                                'washing machine': {'mu': [2144.17,	387.89,	147.20, 0],
                                                                    'std': [100.40, 111.46, 52.19, 21.90],
                                                                    },
                                                'dish washer': {'mu': [1992.19, 105.75, 0],
                                                                'std': [31.65, 40.62, 6.87],
                                                                },
                                                'kettle': {'mu': [2922.62, 0],
                                                           'std': [176.41, 48.96],
                                                           },
                                                'microwave': {'mu': [1290.09, 0],
                                                              'std': [37.22, 28.99],
                                                              },
                                                },
                                            4: {'fridge': {'mu': [92.09, 0],
                                                           'std': [31.19, 0.06],
                                                           },
                                                'kettle': {'mu': [2837.12, 0],
                                                           'std': [157.80, 59.23],
                                                           },
                                                },
                                            5: {'fridge': {'mu': [109.51, 0],
                                                           'std': [7.22, 1.54],
                                                           },
                                                'washing machine': {'mu': [2086.05, 1445.02, 340.71, 0],
                                                                    'std': [115.89,	40.88, 164.48, 30.70],
                                                                    },
                                                'dish washer': {'mu': [1661.46, 97.45, 0],
                                                                'std': [24.52, 25.23, 4.02],
                                                                },
                                                'kettle': {'mu': [2868.29, 0],
                                                           'std': [86.69, 65.34],
                                                           },
                                                'microwave': {'mu':  [1474.17, 0],
                                                              'std': [56.14, 32.25],
                                                              },
                                                },
                                            },
                                'AMPds': {1: {'fridge': {'mu': [127.84, 0],
                                                         'std': [7.51, 3.07],
                                                         },
                                              'tumble dryer': {'mu': [4593.19, 247.42, 0],
                                                               'std': [70.326, 8.46, 2.55],
                                                               },
                                              'washing machine': {'mu': [909.11, 531.10, 142.97, 0],
                                                                  'std': [74.44, 51.12,	43.38, 20.55],
                                                                  },
                                              'dish washer': {'mu': [753.23, 137.75, 0],
                                                              'std': [15.46, 7.72, 5.74],
                                                              },
                                              'electric oven': {'mu': [3160.26, 122.13, 0],
                                                                'std': [370.19,	118.42,	20.96],
                                                                },
                                              'heat pump': {'mu': [1792.93, 37.23, 0],
                                                            'std': [104.67,	1.17, 0.38],
                                                            },
                                              }
                                          },
                                'REDD': {1: {'fridge': {'mu': [213.53, 0],
                                                        'std': [67.81, 5.56],
                                                        },
                                             'washer dryer': {'mu': [2683.65, 0],
                                                              'std': [40.90, 1.58],
                                                              },
                                             'dish washer': {'mu': [1095.22,	243.63,	0],
                                                             'std': [32.68, 79.19, 15.98],
                                                             },
                                             'microwave': {'mu':  [1457.86,	0],
                                                           'std': [63.79, 62.13],
                                                           },
                                             },
                                         2: {'fridge': {'mu': [162.85, 0],
                                                        'std': [6.87, 0.95],
                                                        },
                                             'dish washer': {'mu': [1202.61,	247.45,	0],
                                                             'std': [16.24, 16.70, 14.95],
                                                             },
                                             'microwave': {'mu':  [1774.09,	0],
                                                           'std': [22.36, 42.49],
                                                           },
                                             },
                                         3: {'fridge': {'mu': [118.28, 0],
                                                        'std': [4.82, 0.58],
                                                        },
                                             'washer dryer': {'mu': [2504.58, 262.03, 0],
                                                              'std': [61.38, 23.74, 1.78],
                                                              },
                                             'dish washer': {'mu': [737.19, 195.38, 0],
                                                             'std': [10.52, 32.48, 2.36],
                                                             },
                                             'microwave': {'mu':  [1350.14, 0],
                                                           'std': [23.55, 70.32],
                                                           },
                                             }
                                         },
                                }

# datasets_list_name = ['UK-DALE', 'REDD', 'AMPds']
datasets_list_name = ['UK-DALE']


def working_state_extr(datasets_list_name, all_appliance_working_states):

    time_indexes = pd.date_range(start='2012-12-15 08:20:42+0000', end='2012-12-16 08:20:42+0000', freq='6S', closed='left')
    aggregate_data = pd.Series(0, index=time_indexes)
    state_change_data = pd.Series(0, index=time_indexes)
    disag_data = list()
    disag_change_state = list()

    try:

        for dataset_name in datasets_list_name:

            print "Selected dataset: " + dataset_name

            DS_FILE = datasets_list[dataset_name]
            dataset = DataSet(os.path.join(DB_DIR, DS_FILE))
            for building in dataset.buildings.values():  # all the buildings in the dataset

                appliance_working_states = all_appliance_working_states[dataset_name].get(building.identifier.instance)

                if not appliance_working_states:
                    continue

                elec = building.elec
                # elec.draw_wiring_graph()

                for appl, states in appliance_working_states.items():

                    print "Analyzing appliance {} in building {}".format(appl, building.identifier.instance)

                    meter = elec[appl]  # TODO check when it is MeterGroup (not only ElecMeter)

                    ac_types_meter = meter.available_ac_types('power')
                    appl_power_series = meter.power_series_all_data(ac_type='active', sample_period=datasets_samples_period[dataset_name])  # type pd.Series(), TODO choose one of the ac_type available (active/reactive/apparent), PER ORA fare solo active !!!
                    appl_power_series = appl_power_series.dropna()

                    appl_state_series = state_assignment(appl_power_series, states['mu'])

                    # plot_states_over_power_series(appl_power_series, appl_state_series, states)

                    state_transition_series = transition_detection(appl_state_series)

                    csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.splitext(os.path.join(DB_DIR, DS_FILE))[0], building.identifier.instance, appl.replace(' ', '_'))
                    # pkl_filename = "{}-{}-{}.pkl".format(os.path.splitext(os.path.join(DS_DIR, DS_FILE))[0], building.identifier.instance, appl.replace(' ', '_'))
                    print 'Saving activation series in ' + csv_filename
                    # appl_power_series.to_csv(csv_filename)
                    state_transition_series.to_pickle(pkl_filename)

                #     # Plot: set interval
                #     aggregate_data += appl_power_series[time_indexes]
                #     disag_data.append(appl_power_series[time_indexes])
                #     # state_change_data += state_transition_series[time_indexes]
                #     state_change_data[state_transition_series[state_change_data.index[0]:state_change_data.index[-1]].index] = 1
                #     disag_change_state.append(state_transition_series[state_change_data.index[0]:state_change_data.index[-1]])
                #
                # plot_figure2(aggregate_data, state_change_data, disag_data, disag_change_state)

    except:

        print "Error during analysis of dataset {} for appliance {} for building {}".format(dataset_name, appl, building.identifier.instance)
        print "Error details:"
        traceback.print_exc()


def state_assignment(appl_power_series, centroid):
    centroid = np.asarray(centroid)
    data = appl_power_series.values

    T = appl_power_series.size
    states_num = centroid.shape[0]  # TODO check if it works with multi-dim centroid, matlab like (row Pa, col Pr)
    n_dim = 1  # TODO parametrize it depending on the number of appl_power_series dimensionality, ex. Pa+Pr

    centroid_ = np.reshape(centroid, (n_dim, 1, states_num))
    centroid_ = np.tile(centroid_, np.array([1, T, 1]))

    data_ = np.reshape(data, (n_dim, T, 1))
    data_ = np.tile(data_, np.array([1, 1, states_num]))

    diff_ = data_ - centroid_
    diff__ = np.zeros((1, T, states_num))
    for idxDim in np.arange(n_dim):
        diff__ += np.square(diff_[idxDim, :, :])
    diff__ = np.sqrt(diff__)  # diff__[~,i,j] = norma euclidea della distanza tra i-th campione  e j-th pLevel
    clusterIdx = np.argmin(diff__, axis=2)
    clusterIdx = np.reshape(clusterIdx, (T,))

    appl_state_series = pd.Series(clusterIdx, index=appl_power_series.index)
    return appl_state_series


def plot_states_over_power_series(appl_power_series, appl_state_series, states):
    samples_to_plot_start = 6000
    samples_to_plot_end = 9000
    #samples_to_plot_end = appl_power_series.shape[0]

    samples = pd.Index(np.arange(samples_to_plot_start, samples_to_plot_end))

    plt.figure()
    plt.plot(appl_power_series[samples], 'k')

    color_map = ['c', 'm', 'y', 'g', 'b', 'r']
    states_num = len(states['mu'])

    for idx in np.arange(states_num):
        timeIdx = appl_state_series[samples] == idx
        plt.stem(appl_power_series[samples][timeIdx].index, appl_power_series[samples][timeIdx], linefmt=' ', basefmt=' ', markerfmt=color_map[idx] + 'o')

        plt.axhline(y=states['mu'][idx], linestyle='--', color=color_map[idx])
        plt.axhline(y=(states['mu'][idx] + states['std'][idx]), linestyle='-', color=color_map[idx])
        plt.axhline(y=(states['mu'][idx] - states['std'][idx]), linestyle='-', color=color_map[idx])

    plt.show()


def transition_detection(appl_state_series):
    # old version
    # appl_state_series_values = appl_state_series.values
    # appl_state_series_values = np.insert(appl_state_series_values[0:-1], 0, 0)
    # appl_state_series_shifted = pd.Series(appl_state_series_values, index=appl_state_series.index)
    #
    # statetransition_diff = appl_state_series - appl_state_series_shifted
    # statetransition_index = statetransition_diff != 0
    #
    # statetransition_series = appl_state_series[statetransition_index]

    # improved version, remove first element
    diff_series = appl_state_series.diff().fillna(0)
    statetransition_series = appl_state_series[diff_series != 0]

    return statetransition_series


def plot_figure(appliance_power, state_transition):

    from datetime import timedelta
    import matplotlib.transforms as mtransforms

    sampling_period = 6
    nus_subsampling_ratio = 10
    nus_expansion_ratio = 10

    window_center1 = state_transition[state_transition == 0].index[2]
    window_center2 = state_transition.index[state_transition.index.get_loc(window_center1)+1]
    plot_window = pd.date_range(window_center1 - timedelta(seconds=sampling_period * 20), window_center1 + timedelta(seconds=sampling_period * 35), freq='6S', closed='left')

    subsampling_points = appliance_power[plot_window].asfreq('{:d}S'.format(sampling_period * nus_subsampling_ratio)).fillna(0)
    if subsampling_points.index[0] != (sampling_period * nus_subsampling_ratio) % 60:
        subsampling_points = appliance_power[subsampling_points.shift(freq='{:d}S'.format((sampling_period * nus_subsampling_ratio) - subsampling_points.index[0].second)).index[:-1]]

    expansion_window_points1 = appliance_power[window_center1 - timedelta(seconds=sampling_period * (nus_expansion_ratio // 2)):
                                               window_center1 + timedelta(seconds=sampling_period * ((nus_expansion_ratio // 2) - 1))]
    expansion_window_points2 = appliance_power[window_center2 - timedelta(seconds=sampling_period * (nus_expansion_ratio // 2)):
                                               window_center2 + timedelta(seconds=sampling_period * ((nus_expansion_ratio // 2) - 1))]

    plot_values = appliance_power[plot_window]  #.values

    plt.figure(figsize=(10, 3))
    plt.plot(plot_values, label='Appl. original signal')
    plt.plot(subsampling_points, 'go', label='Uniform sampling points')  # draw uniform subsampling point
    plt.plot(expansion_window_points1, 'ro-', label='Non-uniform sampling points')  # draw expansion window1 points
    plt.legend(loc='upper left')

    plt.plot(expansion_window_points1, 'r-')  # color expansion window1 data
    plt.plot(expansion_window_points2, 'ro-')  # draw expansion window2 points
    plt.plot(expansion_window_points2, 'r-')  # color expansion window2 points

    # plt.legend('Appliance original signal', )

    # plt.plot([window_center - timedelta(seconds=sampling_period // 2 * nus_expansion_ratio), window_center - timedelta(seconds=sampling_period // 2 * nus_expansion_ratio)],
    #          plt.ylim(), 'k-', lw=2)  # draw upper expansion window bounds
    # plt.plot([window_center + timedelta(seconds=sampling_period // 2 * nus_expansion_ratio), window_center + timedelta(seconds=sampling_period // 2 * nus_expansion_ratio)],
    #          plt.ylim(), 'k-', lw=2)  # draw lower expansion window bounds

    # plt.xticks(rotation='vertical')
    # plt.tick_params()
    # plt.grid()
    # plt.gca().xaxis.grid(True)
    plt.ylabel('P (W)')
    plt.xlabel('Timestamp (hh:mm:ss)')

    ax = plt.gca()
    ax.xaxis.grid(True)
    trans = mtransforms.blended_transform_factory(ax.transData, ax.transAxes)
    ax.fill_between(plot_values.index, 0, 1, where=(plot_values.index >= expansion_window_points1.index[0]) & (plot_values.index <= expansion_window_points1.index[-1]), facecolor='red', alpha=0.5, transform=trans)
    ax.fill_between(plot_values.index, 0, 1, where=(plot_values.index >= expansion_window_points2.index[0]) & (plot_values.index <= expansion_window_points2.index[-1]), facecolor='red', alpha=0.5, transform=trans)

    plt.tight_layout()
    plt.savefig("nus_example.png")
    plt.show()


def plot_figure2(aggregate_data=None, state_change_data=None, disag_data=None, disag_change_state=None):

    aggregate_data = pd.Series.from_csv('aggregate_data.csv', index_col=0, parse_dates=True)
    state_change_data = pd.Series.from_csv('state_change_data_for_plot.csv', index_col=0, parse_dates=True)

    appliances_data = list()
    for index in range(5):
        appliances_data.append(pd.Series.from_csv('appliance_{}.csv'.format(index), index_col=0, parse_dates=True))

    # # save data
    # for index, data in enumerate(disag_data):
    #     data.to_csv('appliance_{}.csv'.format(index))
    #
    # for index, data in enumerate(disag_change_state):
    #     data.to_csv('appliance_{}_state.csv'.format(index))
    #
    # aggregate_data.to_csv('aggregate_data.csv')

    # aggregate_data.index = aggregate_data.index.tz_localize('UTC')
    # state_change_data.index = state_change_data.index.tz_localize('UTC')

    from datetime import timedelta
    import matplotlib.transforms as mtransforms

    sampling_period = 6
    nus_subsampling_ratio = 10
    nus_expansion_ratio = 5

    # window_center1 = state_change_data[state_change_data == 1].index[2]
    # window_center2 = state_change_data.index[state_change_data.index.get_loc(window_center1)+1]
    # plot_window = pd.date_range(window_center1 - timedelta(seconds=sampling_period * 20), window_center1 + timedelta(seconds=sampling_period * 35), freq='6S', closed='left')
    # start_plot_window = aggregate_data[aggregate_data==0].index[0]
    # plot_window = pd.date_range(aggregate_data.index[7600], aggregate_data.index[7750], freq='6S', closed='left')
    # plot_window = pd.date_range(aggregate_data.index[2700], aggregate_data.index[3400], freq='6S', closed='left')
    plot_window = pd.date_range(aggregate_data.index[7600], aggregate_data.index[7800], freq='6S', closed='left')

    # fill expansion windows
    tmp_state_change_data = state_change_data[plot_window]
    for each in range(-nus_expansion_ratio // 2, nus_expansion_ratio // 2):
        expansion_indices = tmp_state_change_data[tmp_state_change_data == 1].index + timedelta(seconds=each * sampling_period)
        state_change_data[expansion_indices] = 1

    # plt.figure(figsize=(10, 3))
    # plt.plot(aggregate_data[plot_window])
    # plt.tight_layout()
    # plt.plot(state_change_data[plot_window])
    # plt.show()

    plot_values = aggregate_data[plot_window]
    expansion_windows_points = state_change_data[plot_window]

    subsampling_points = aggregate_data[plot_window].asfreq('{:d}S'.format(sampling_period * nus_subsampling_ratio)).fillna(0)
    if subsampling_points.index[0] != (sampling_period * nus_subsampling_ratio) % 60:
        subsampling_points = aggregate_data[subsampling_points.shift(freq='{:d}S'.format((sampling_period * nus_subsampling_ratio) - subsampling_points.index[0].second)).index[:-1]]

    expansion_windows_points = aggregate_data[plot_window][state_change_data[plot_window][state_change_data[plot_window] == 1].index]
    #
    # expansion_window_points1 = appliance_power[window_center1 - timedelta(seconds=sampling_period * (nus_expansion_ratio // 2)):
    #                                            window_center1 + timedelta(seconds=sampling_period * ((nus_expansion_ratio // 2) - 1))]
    # expansion_window_points2 = appliance_power[window_center2 - timedelta(seconds=sampling_period * (nus_expansion_ratio // 2)):
    #                                            window_center2 + timedelta(seconds=sampling_period * ((nus_expansion_ratio // 2) - 1))]
    #
    # plot_values = appliance_power[plot_window]  #.values
    #
    plt.figure(figsize=(15, 3))
    plt.plot(plot_values, label='Appl. original signal')
    plt.plot(subsampling_points, 'go', label='Uniform sampling points')  # draw uniform subsampling point
    plt.plot(expansion_windows_points, 'ro', label='Non-uniform sampling points')  # draw expansion window1 points

    plt.gca().xaxis.grid(True)
    plt.axes().get_xaxis().set_ticks(subsampling_points.index)
    # plt.axes().get_yaxis().set_ticks([])
    plt.ylabel('Consumption (W)')
    plt.xlabel('Time (s)')
    plt.axes().get_xaxis().set_ticklabels([])
    # plt.axes().get_yaxis().set_ticks([])

    # plt.plot(state_change_data[plot_window])
    # plt.legend(loc='upper left')
    #
    # plt.plot(expansion_window_points1, 'r-')  # color expansion window1 data
    # plt.plot(expansion_window_points2, 'ro-')  # draw expansion window2 points
    # plt.plot(expansion_window_points2, 'r-')  # color expansion window2 points
    #
    # # plt.legend('Appliance original signal', )
    #
    # # plt.plot([window_center - timedelta(seconds=sampling_period // 2 * nus_expansion_ratio), window_center - timedelta(seconds=sampling_period // 2 * nus_expansion_ratio)],
    # #          plt.ylim(), 'k-', lw=2)  # draw upper expansion window bounds
    # # plt.plot([window_center + timedelta(seconds=sampling_period // 2 * nus_expansion_ratio), window_center + timedelta(seconds=sampling_period // 2 * nus_expansion_ratio)],
    # #          plt.ylim(), 'k-', lw=2)  # draw lower expansion window bounds
    #
    # # plt.xticks(rotation='vertical')
    # # plt.tick_params()
    # # plt.grid()
    # # plt.gca().xaxis.grid(True)
    # plt.ylabel('P (W)')
    # plt.xlabel('Timestamp (hh:mm:ss)')
    #
    # ax = plt.gca()
    # ax.xaxis.grid(True)
    # trans = mtransforms.blended_transform_factory(ax.transData, ax.transAxes)
    # ax.fill_between(plot_values.index, 0, 1, where=plot_values[expansion_windows_points] > 1, facecolor='red', alpha=0.5, transform=trans)
    # ax.fill_between(plot_values.index, 0, 1, where=(plot_values.index >= expansion_window_points2.index[0]) & (plot_values.index <= expansion_window_points2.index[-1]), facecolor='red', alpha=0.5, transform=trans)
    # ax.fill_between(expansion_windows_points.index, 0, 1, where=expansion_windows_points>0, facecolor='red', alpha=0.5, transform=trans)
    #
    #
    plt.tight_layout()
    plt.savefig("nus_aggregated.eps", format='eps', dpi=1000)

    plt.figure(figsize=(15, 3))
    # for fig in appliances_data:
    #     plt.plot(fig[plot_window])

    appl_num = 2
    # appl_num:
    # 0: kettle
    # 1: dish washer
    # 2: fridge
    # 3: microwave
    # 4: washing machine

    plt.plot(appliances_data[appl_num][plot_window])
    # plt.plot(appliances_data[4][plot_window])
    # plt.plot(appliances_data[0][plot_window])
    # plt.plot(appliances_data[1][plot_window])
    # plt.plot(appliances_data[3][plot_window])

    subsampling_points = appliances_data[appl_num][plot_window].asfreq('{:d}S'.format(sampling_period * nus_subsampling_ratio)).fillna(0)
    if subsampling_points.index[0] != (sampling_period * nus_subsampling_ratio) % 60:
        subsampling_points = appliances_data[appl_num][subsampling_points.shift(freq='{:d}S'.format((sampling_period * nus_subsampling_ratio) - subsampling_points.index[0].second)).index[:-1]]

    expansion_windows_points = appliances_data[appl_num][plot_window][state_change_data[plot_window][state_change_data[plot_window] == 1].index]
    plt.plot(subsampling_points, 'go', label='Uniform sampling points')  # draw uniform subsampling point
    plt.plot(expansion_windows_points, 'ro', label='Non-uniform sampling points')  # draw expansion window1 points

    # plt.axes().get_xaxis().set_ticks([])
    # plt.axes().get_yaxis().set_ticks([])
    # plt.axes().tick_params(axis='x', which='major', pad=1500)
    plt.gca().xaxis.grid(True)
    plt.axes().get_xaxis().set_ticks(subsampling_points.index)
    # plt.axes().get_yaxis().set_ticks([])
    plt.axes().get_xaxis().set_ticklabels([])
    plt.ylabel('Consumption (W)')
    plt.xlabel('Time (s)')

    plt.tight_layout()
    plt.savefig("nus_disaggregated.eps", format='eps', dpi=1000)
    plt.show()


if __name__ == '__main__':

    # working_state_extr(datasets_list_name, all_appliance_working_states)
    plot_figure2()
