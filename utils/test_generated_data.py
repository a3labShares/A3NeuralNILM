import numpy as np


for epoch in range(1, 50):

    x_train_original = np.load("/home/marco/Documents/UnivPM/Projects/NILM/test_data/test_original_code/x_train_" + str(epoch) + ".npz.npy")
    y_train_original = np.load("/home/marco/Documents/UnivPM/Projects/NILM/test_data/test_original_code/y_train_" + str(epoch) + ".npz.npy")

    x_train_new = np.load("/home/marco/Documents/UnivPM/Projects/NILM/test_data/test_new_code_quater/x_train_" + str(epoch) + ".npz.npy")
    y_train_new = np.load("/home/marco/Documents/UnivPM/Projects/NILM/test_data/test_new_code_quater/y_train_" + str(epoch) + ".npz.npy")

    # x_train_original_bis = np.load("/home/marco/Documents/UnivPM/Projects/NILM/test_data/test_original_code_bis/x_train_" + str(epoch) + ".npz.npy")
    # y_train_original_bis = np.load("/home/marco/Documents/UnivPM/Projects/NILM/test_data/test_original_code/y_train_" + str(epoch) + ".npz.npy")

    # x_train_new = np.load("/home/marco/Documents/UnivPM/Projects/NILM/test_data/test_new_code/x_train_" + str(epoch) + ".npz.npy")
    # y_train_new = np.load("/home/marco/Documents/UnivPM/Projects/NILM/test_data/test_new_code/y_train_" + str(epoch) + ".npz.npy")

    # x_train_new_bis = np.load("/home/marco/Documents/UnivPM/Projects/NILM/test_data/test_new_code_bis/x_train_" + str(epoch) + ".npz.npy")
    # y_train_new_bis = np.load("/home/marco/Documents/UnivPM/Projects/NILM/test_data/test_new_code_bis/y_train_" + str(epoch) + ".npz.npy")
    #
    # x_train_new_tris = np.load("/home/marco/Documents/UnivPM/Projects/NILM/test_data/test_new_code_tris/x_train_" + str(epoch) + ".npz.npy")
    # y_train_new_tris= np.load("/home/marco/Documents/UnivPM/Projects/NILM/test_data/test_new_code_tris/y_train_" + str(epoch) + ".npz.npy")
    #
    x_diff = x_train_original == x_train_new
    y_diff = y_train_original == y_train_new
    #
    # x_diff_ori = x_train_original == x_train_original_bis
    # y_diff_ori = y_train_original == y_train_original_bis

    if x_diff.all() and y_diff.all():
        print('Same vectors!')

    elif x_diff.all():
        print('Just same inputs vectors')
    elif y_diff.all():
        print('Just same outputs vectors')
    else:
        print('Different vectors!')
