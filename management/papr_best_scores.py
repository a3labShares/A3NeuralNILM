import yaml
import os
import copy
from neuralnilm.experiment import configure_logger
from utils.io_utils import load_json
from default_configs import PROJECT_DIR


def find_best_valid_f1(logger_filename):
    best_valid_f1 = 0.
    with open(logger_filename, "r") as open_file:
        for line in open_file:
            if "Check" in line:
                best_valid_f1 = line.split(" ")[-1]
    return best_valid_f1


if __name__ == "__main__":

    logger = configure_logger(os.path.join(PROJECT_DIR, "scripts", "management", "papr_best_scores.log"))

    temp_run_paths = os.listdir(os.path.join(PROJECT_DIR, "experiments"))
    run_paths = []
    for temp_run_path in temp_run_paths:
        if temp_run_path.split('_')[0] == 'config':
            run_paths.append(temp_run_path)
    best_f1_scores = {}

    for run_path in run_paths:
        complete_run_path = os.path.join(PROJECT_DIR, "experiments", run_path)
        temp_paths = os.listdir(os.path.join(PROJECT_DIR, "experiments", run_path))
        complete_test_paths = []
        for temp_path in temp_paths:
            if temp_path.split('_')[0] == 'test':
                complete_test_paths.append(os.path.join(complete_run_path, temp_path))

        for complete_test_path in complete_test_paths:
            test_files = os.listdir(complete_test_path)
            score_files = []
            for test_file in test_files:
                if 'scores' in test_file.split('_'):
                    score_files.append(test_file)

            scores_dict = {}
            for score_file in score_files:
                # Little trick to get the appliance full name
                # === #
                i = 1
                appliance = score_file.split('_')[0]
                while score_file.split('_')[i] != 'scores':
                    appliance += '_{}'.format(score_file.split('_')[i])
                    i += 1
                # === #
                if appliance not in scores_dict.keys():
                    scores_dict[appliance] = {}

                building = score_file.split('.')[0]
                building = building.split('_')[-1]

                with open(os.path.join(complete_test_path, score_file), 'r') as stream:
                    try:
                        scores_dict[appliance][building] = yaml.load(stream)
                    except yaml.YAMLError as exc:
                        raise Exception(exc)

            for appliance in scores_dict.keys():
                overall_precision = 0.
                overall_recall = 0.

                if len(scores_dict[appliance].keys()) == 1:
                    buildings = scores_dict[appliance].keys()[0]
                else:
                    buildings = 'average between '
                    for building in scores_dict[appliance].keys():
                        buildings += '{} '.format(building)

                for building in scores_dict[appliance].keys():
                    overall_precision += scores_dict[appliance][building]['precision_score_(energy_based)']
                    overall_recall += scores_dict[appliance][building]['recall_score_(energy_based)']

                overall_precision /= len(scores_dict[appliance].keys())
                overall_recall /= len(scores_dict[appliance].keys())

                overall_f1_score = 2 * overall_precision * overall_recall / (overall_precision + overall_recall)

                run_params = load_json(path=complete_run_path, filename='run.json')
                test_params = load_json(path=complete_test_path, filename='test.json')

                dataset = run_params["experiment"]["dataset"]

                if run_params["experiment"]["reactive_power"]:
                    score_field = 'A+R'
                else:
                    score_field = 'A'

                if run_params["experiment"]["main_mode"] == "noised":
                    score_field += " noised"
                else:
                    score_field += " denoised"

                if dataset not in best_f1_scores.keys():
                    best_f1_scores[dataset] = {}

                if score_field not in best_f1_scores[dataset].keys():
                    best_f1_scores[dataset][score_field] = {}

                if appliance not in best_f1_scores[dataset][score_field].keys():
                    best_f1_scores[dataset][score_field][appliance] = {}

                if buildings not in best_f1_scores[dataset][score_field][appliance].keys():
                    best_f1_scores[dataset][score_field][appliance][buildings] = {
                        'f1': 0.,
                        'valid_f1': 0.,
                        'precision': 0.,
                        'recall': 0.,
                        'test_params': {},
                        'run_params': {},
                        'test_path': ''}

                if best_f1_scores[dataset][score_field][appliance][buildings]['f1'] < overall_f1_score <= 1:
                    best_f1_scores[dataset][score_field][appliance][buildings]['f1'] = overall_f1_score
                    best_f1_scores[dataset][score_field][appliance][buildings]['precision'] = overall_precision
                    best_f1_scores[dataset][score_field][appliance][buildings]['recall'] = overall_recall
                    best_f1_scores[dataset][score_field][appliance][buildings][
                        'test_params'] = copy.deepcopy(test_params)
                    best_f1_scores[dataset][score_field][appliance][buildings][
                        'run_params'] = copy.deepcopy(run_params)
                    best_f1_scores[dataset][score_field][appliance][buildings]['test_path'] = complete_test_path

    for dataset in best_f1_scores.keys():
        logger.info("#############################################################")
        logger.info("#############################################################")
        logger.info('DATASET: {}'.format(dataset))
        logger.info("#############################################################")
        logger.info("#############################################################")
        logger.info('')
        for score_field in best_f1_scores[dataset].keys():
            logger.info('Power type: {}. (A+R = active + reactive / A = active)'.format(score_field))
            logger.info('')
            for appliance in best_f1_scores[dataset][score_field].keys():
                logger.info("#############################################################")
                logger.info('Appliance: {}'.format(appliance))
                logger.info("#############################################################")
                for buildings in best_f1_scores[dataset][score_field][appliance].keys():
                    fields = best_f1_scores[dataset][score_field][appliance][buildings]
                    logger.info('Building: {}'.format(buildings))
                    logger.info('Precision: {}.'.format(fields['precision']))
                    logger.info('Recall: {}.'.format(fields['recall']))
                    logger.info('F1: {}.'.format(fields['f1']))
                    logger.info('Test path: {}'.format(fields['test_path']))
                    logger.info('Disaggregation stride: {}.'.format(fields['test_params']['test']['stride']))
                    logger.info("Appliance configuration:")
                    conf = fields["run_params"]["configuration"]["appliance_configuration"]
                    for key in conf.keys():
                        logger.info("{}: {}".format(key, conf[key]))
                    logger.info('')
