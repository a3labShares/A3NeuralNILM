import os
import numpy as np
import random as rn

# get reproducible results: https://keras.io/getting-started/faq/#how-can-i-obtain-reproducible-results-using-keras-during-development
os.environ['PYTHONHASHSEED'] = '0'
np.random.seed(42)
rn.seed(12345)

import yaml
from keras import optimizers
from keras import backend as K
import utils.gpu_utils
from utils.disaggregate import median_disag, mean_disag
from utils.parameter_utils import load_params
from utils.net_utils import train_loop
from utils.dataset_utils import get_dataset, read_dataset
from neuralnilm.experiment import configure_logger, close_logger
from netcreator.searcher import get_network
from management.constraint import constrained_test, constrained_train
import argparse
from neuralnilm.metrics import run_metrics


def train(params):

    train_dataset = None
    logger = None

    try:

        if params["overwrite"]:
            appliances_to_train = params["appliances"]
        else:
            appliances_to_train = []
            for appliance in params["appliances"]:
                if not os.path.isfile(
                        os.path.join(params["paths"]["appliance_paths"][appliance], "{}_activations.h5".format(appliance))):
                    appliances_to_train.append(appliance)

        for appliance in appliances_to_train:
            logger = configure_logger(os.path.join(
                params["paths"]["appliance_paths"][appliance], "{}_training.log".format(appliance)))

            train_dataset = get_dataset(params["experiment"]["dataset"])

            train_dataset.load(dataset_path=params["paths"]["dataset_path"],
                               dataset_name=params["experiment"]["dataset"],
                               target_appliance_list=[appliance],  # single appliance as target
                               # target_appliance_list=['washing_machine', 'microwave'],  # all appliances as target, to load all data
                               phase="train",
                               dataset_id=params["ids"]["hashed_id"],
                               logger=logger,
                               reactive_power=params["experiment"]["reactive_power"],
                               seq_per_batch=params["experiment"]["sequences_per_batch"],
                               main_mode=params["experiment"]["main_mode"],
                               source_type=params["experiment"]["source_type"],
                               time_windows=params["experiment"]["time_windows"],
                               valid_ratio=params["experiment"]["valid_ratio"],
                               background_appliance_list=params["experiment"]["background_appliance_list"],
                               test_mode=None,
                               nus_enabled=params.get("nus_enabled"),
                               nus_subsampling_ratio=params.get("nus_subsampling_ratio"),
                               nus_expansion_ratio=params.get("nus_expansion_ratio"),
                               us_enabled=params.get("us_enabled"),
                               us_subsampling_ratio=params.get("us_subsampling_ratio"))

            # if params["experiment"].get("gpus") > 1:  # if more then 1 gpus available, use multi_gpu_model (WIP)
            #
            #     import tensorflow as tf
            #     from keras.utils.training_utils import multi_gpu_model
            #
            #     with tf.device("/cpu:0"):
            #         net = get_network(architecture=params["experiment"]["architecture"],
            #                           input_dims=train_dataset.get_input_dimensions(),
            #                           output_dims=train_dataset.get_output_dimensions(),
            #                           input_params=params["configurations"][appliance],
            #                           name=appliance)
            #
            #     net_multi_gpu = multi_gpu_model(net, gpus=params["experiment"].get("gpus"))
            #
            # else:

            net = get_network(architecture=params["experiment"]["architecture"],
                              input_dims=train_dataset.get_input_dimensions(),
                              output_dims=train_dataset.get_output_dimensions(),
                              input_params=params["configurations"][appliance],
                              name=appliance)

            learning_rate = params["experiment"]["learning_rate"]

            if params["experiment"]["optimizer"] == "hardcoded_sgd":
                optimizer = optimizers.sgd(lr=learning_rate, momentum=0.9, nesterov=True)
            else:
                optimizer = optimizers.get({"class_name": params["experiment"]["optimizer"],
                                            "config": {"lr": learning_rate}})

            # if params["experiment"].get("gpus") > 1:
            #     net_multi_gpu.compile(optimizer=optimizer, loss=params["experiment"]["loss"])
            #
            # else:
            net.compile(optimizer=optimizer, loss=params["experiment"]["loss"])

            # Start training phase
            x_valid, y_valid = train_dataset.get_valid_data(appliance)

            train_output_dict = train_loop(logger=logger,
                                           experiment_params=params["experiment"],
                                           train_dataset=train_dataset,
                                           target_appliance=appliance,
                                           model=net.model,
                                           x_valid=x_valid,
                                           y_valid=y_valid)

            net.model.set_weights(train_output_dict["best_weights"])
            # also with multi_gpu must be saved the template model
            net.save(net_path=params["paths"]["appliance_paths"][appliance],
                     activation_filename="{}_activations.h5".format(appliance),
                     spec_filename="{}_specs.json".format(appliance),
                     overwrite=True)

    except:
        # skip next stage
        params["phases"]["test"] = False
        params["phases"]["evaluate"] = False

        if logger:
            logger.exception("Unhandled error:")

    finally:
        if train_dataset:
            train_dataset.close()

        close_logger(logger)
        K.clear_session()


def test(params):

    test_dataset = None
    logger = None

    try:

        if params["overwrite"]:
            appliances_to_test = params["appliances"]
        else:
            # Here we look for all estimates file for each appliance. If estimates for a building are not found,
            # the appliance is put in the tested appliance list.
            # === #
            appliances_to_test = []
            test_dataset = get_dataset(params["experiment"]["dataset"])

            for appliance in params["appliances"]:
                test_appliance = False
                test_buildings = test_dataset.get_test_buildings(
                    appliance,
                    test_mode=params["test"]["test_mode"])
                test_files = os.listdir(params["paths"]["test_paths"][appliance])

                i = 0
                while not test_appliance and i < len(test_buildings):
                    building_not_found = True
                    estimates_file = "{}_estimates_building_{}.csv".format(appliance, test_buildings[i])
                    test_file_nr = 0
                    while building_not_found and test_file_nr < len(test_files):
                        test_file = test_files[test_file_nr]
                        if estimates_file == test_file:
                            building_not_found = False
                        test_file_nr += 1

                    if building_not_found:
                        test_appliance = True

                    i += 1

                if test_appliance:
                    appliances_to_test.append(appliance)
            # === #

        for appliance in appliances_to_test:
            logger = configure_logger(os.path.join(
                params["paths"]["test_paths"][appliance], "{}_test.log".format(appliance)))

            net = get_network(architecture=params["experiment"]["architecture"])
            net.load(net_path=params["paths"]["appliance_paths"][appliance],
                     spec_filename="{}_specs.json".format(appliance),
                     activation_filename="{}_activations.h5".format(appliance))

            test_dataset = get_dataset(params["experiment"]["dataset"])

            test_dataset.load(dataset_path=params["paths"]["dataset_path"],
                              dataset_name=params["experiment"]["dataset"],
                              target_appliance_list=[appliance],
                              phase="test",
                              dataset_id=params["ids"]["hashed_id"],
                              logger=logger,
                              reactive_power=params["experiment"]["reactive_power"],
                              seq_per_batch=params["experiment"]["sequences_per_batch"],
                              main_mode=params["experiment"]["main_mode"],
                              source_type=params["experiment"]["source_type"],
                              time_windows=params["test"]["time_windows"],
                              valid_ratio=1.,
                              background_appliance_list=params["experiment"]["background_appliance_list"],
                              test_mode=params["test"]["test_mode"],
                              nus_enabled=params.get("nus_enabled"),
                              nus_subsampling_ratio=params.get("nus_subsampling_ratio"),
                              nus_expansion_ratio=params.get("nus_expansion_ratio"),
                              us_enabled=params.get("us_enabled"),
                              us_subsampling_ratio=params.get("us_subsampling_ratio"))

            test_buildings = test_dataset.get_test_buildings(appliance)

            for building in test_buildings:
                padded_building_mains = test_dataset.get_mains(padding="appliance_sequence_length",
                                                               appliance=appliance,
                                                               building=building)

                appliance_metadata = test_dataset.get_source_metadata(appliance)
                logger.info("Starting disaggregation for building {}.".format(building))

                if params["experiment"]["disag_type"] == 'mean':
                    disag = mean_disag
                elif params["experiment"]["disag_type"] == 'median':
                    disag = median_disag
                else:
                    raise TypeError("Unsupported disaggregation type: " + str(params["experiment"]["disag_type"]))

                # Set STD for active and reactive power
                active_power_std = appliance_metadata["input_stats"]["std"][0]
                reactive_power_std = None

                if params["experiment"]["reactive_power"]:
                    reactive_power_std = appliance_metadata["input_stats"]["std"][1]

                estimates = disag(mains=padded_building_mains,
                                  net=net,
                                  sequences_per_batch=params["experiment"]["sequences_per_batch"],
                                  std=active_power_std,
                                  r_std=reactive_power_std,
                                  max_target_power=appliance_metadata["max_appliance_power"],
                                  stride=params["test"]["stride"])

                estimates_filename = os.path.join(params["paths"]["test_paths"][appliance],
                                                  '{}_estimates_building_{}.csv'.format(appliance, str(building)))
                np.savetxt(estimates_filename, estimates, delimiter=',', fmt='%.1d')
                logger.info("Done.")

    except:
        # skip next stage
        params["phases"]["evaluate"] = False

        if logger:
            logger.exception("Unhandled error:")

    finally:
        test_dataset.close()
        close_logger(logger)
        K.clear_session()


def evaluate(params):

    logger = None

    try:

        for appliance in params["appliances"]:
            logger = configure_logger(os.path.join(
                params["paths"]["test_paths"][appliance], "{}_evaluate.log".format(appliance)))

            test_dataset = get_dataset(params["experiment"]["dataset"])

            test_buildings = test_dataset.get_test_buildings(appliance=appliance, test_mode=params["test"]["test_mode"])

            for building in test_buildings:
                logger.info('Calculating scores for {}, building {}.'.format(appliance, building))

                estimates_filename = os.path.join(params["paths"]["test_paths"][appliance],
                                                  '{}_estimates_building_{}.csv'.format(appliance, str(building)))

                if not os.path.isfile(estimates_filename):
                    print("File does not exist:", estimates_filename)
                    continue

                predictions = np.loadtxt(estimates_filename, delimiter=',')

                if params["experiment"]["main_mode"] == 'noised':
                    mains = read_dataset(dataset_name=params["experiment"]["dataset"],
                                         window=params["test"]["time_windows"][building],
                                         trace="aggregate",
                                         building=building,
                                         experiment_id=params["ids"]["hashed_id"],
                                         nus_enabled=params["nus_enabled"],
                                         nus_subsampling_ratio=params.get("nus_subsampling_ratio"),
                                         nus_expansion_ratio=params.get("nus_expansion_ratio"),
                                         us_enabled=params.get("us_enabled"),
                                         us_subsampling_ratio=params.get("us_subsampling_ratio"))

                elif params["experiment"]["main_mode"] == 'denoised':

                    mains = read_dataset(dataset_name=params["experiment"]["dataset"],
                                         window=params["test"]["time_windows"][building],
                                         trace=test_dataset.get_all_appliance_list(),
                                         building=building,
                                         experiment_id=params["ids"]["hashed_id"],
                                         nus_enabled=params.get("nus_enabled"),
                                         nus_subsampling_ratio=params.get("nus_subsampling_ratio"),
                                         nus_expansion_ratio=params.get("nus_expansion_ratio"),
                                         us_enabled=params.get("us_enabled"),
                                         us_subsampling_ratio=params.get("us_subsampling_ratio"))

                else:
                    raise ValueError('Invalid main mode {}.'.format(params["experiment"]["main_mode"]))

                ground_truth = read_dataset(dataset_name=params["experiment"]["dataset"],
                                            window=params["test"]["time_windows"][building],
                                            trace=[appliance],
                                            building=building,
                                            experiment_id=params["ids"]["hashed_id"],
                                            nus_enabled=params.get("nus_enabled"),
                                            nus_subsampling_ratio=params.get("nus_subsampling_ratio"),
                                            nus_expansion_ratio=params.get("nus_expansion_ratio"),
                                            us_enabled=params.get("us_enabled"),
                                            us_subsampling_ratio=params.get("us_subsampling_ratio"))

                ground_truth_filename = os.path.join(
                    params["paths"]["test_paths"][appliance],
                    '{}_ground_truth_building_{}.csv'.format(appliance, str(building)))

                np.savetxt(ground_truth_filename, ground_truth, delimiter=',', fmt='%.1d')

                scores = run_metrics(y_true=ground_truth, y_pred=predictions, mains=mains)

                logger.info("Saving scores...")
                scores_filename = os.path.join(params["paths"]["test_paths"][appliance],
                                               '{}_scores_building_{}.yaml'.format(appliance, building))

                if os.path.isfile(scores_filename) and not params["overwrite"]:
                    logger.info('WARNING: found existing score file, not overwriting.')
                else:
                    if os.path.isfile(scores_filename):
                        logger.info('WARNING: overwriting existing file {}.'.format(scores_filename))
                        os.remove(scores_filename)
                    with open(scores_filename, 'w') as fh:
                        yaml.dump(scores, stream=fh, default_flow_style=False)
                        fh.close()
                    logger.info("Scores saved.")
                    logger.info("Energy based precision: {}".format(scores['precision_score_(energy_based)']))
                    logger.info("Energy based recall: {}".format(scores['recall_score_(energy_based)']))
                    logger.info("Energy based f1: {}".format(scores['f1_score_(energy_based)']))

    except:

        if logger:
            logger.exception("Unhandled error:")

    finally:
        close_logger(logger)
        K.clear_session()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="NeuralNILM")
    parser.add_argument("--parameter_file", default=None, type=str)

    unsorted_params = vars(parser.parse_args())

    parameter_file = unsorted_params["parameter_file"]

    params = load_params(parameter_file)

    try:

        if params["phases"]["train"]:
            print "***********************************"
            print "Running training phase."
            print "***********************************"

            if params["experiment"]["constrained_train"]:
                constrained_train(params)
            else:
                train(params)

        if params["phases"]["test"]:
            print "***********************************"
            print "Running test phase."
            print "***********************************"

            if params["test"]["constrained_test"]:
                constrained_test(params)
            else:
                test(params)

        if params["phases"]["evaluate"]:
            print "***********************************"
            print "Running evaluation phase."
            print "***********************************"

            evaluate(params)

    except:

        K.get_session().close()
