import os
import shutil
import numpy as np
import pandas as pd
from datetime import timedelta
from collections import OrderedDict
from default_configs import DB_DIR
from utils.source_metadata import get_dataset_metadata
from nilmtk import DataSet
from neuralnilm.source import MultiSource, RealApplianceSource, SameLocation
# from utils.source_utils import get_source


class NILMDataSetReader(object):
    def __init__(self):
        self.dataset_name = str()
        self.dataset_filename = str()
        self.temp_dataset_filename = str()

        self.source_dict = dict()
        self.valid_batch_dict = dict()
        self.mains_dict = dict()
        self.max_sequence_length_dict = dict()

        self.reactive_power = bool()
        self.test_mode = None

    def get_all_appliance_list(self):
        return get_dataset_metadata(self.dataset_name, 'appliances_list')

    def get_source_metadata(self, appliance=None):
        return get_dataset_metadata(self.dataset_name, appliance)

    def load(self, dataset_path, dataset_name, target_appliance_list, phase, dataset_id, logger, reactive_power,
             seq_per_batch, main_mode, source_type, time_windows, valid_ratio=0.2, background_appliance_list='all',
             test_mode=None, nus_enabled=False, nus_subsampling_ratio=10, nus_expansion_ratio=10, us_enabled=False, us_subsampling_ratio=10):

        self.reactive_power = reactive_power
        self.test_mode = test_mode
        self.dataset_filename = os.path.join(dataset_path, dataset_name + '.h5')
        temp_dataset_dir = os.path.join(os.path.dirname(self.dataset_filename), 'temp_db')

        if __debug__:
            # use original dataset
            self.temp_dataset_filename = self.dataset_filename

        else:
            # copy dataset
            if not os.path.isdir(temp_dataset_dir):
                os.mkdir(temp_dataset_dir)
            self.temp_dataset_filename = os.path.join(temp_dataset_dir,
                                                      "{}_{}_{}".format(dataset_name, str(target_appliance_list),
                                                                        dataset_id))
            shutil.copy(self.dataset_filename, self.temp_dataset_filename)

        # for appliance_i, appliance in enumerate(target_appliance_list):  # why enumerate?
        for appliance in target_appliance_list:
            source = self.get_source(target_appliance=appliance,
                                     dataset_filename=self.temp_dataset_filename,
                                     time_windows=time_windows,
                                     reactive_power=reactive_power,
                                     logger=logger,
                                     phase=phase,
                                     test_mode=test_mode,
                                     seq_per_batch=seq_per_batch,
                                     main_mode=main_mode,
                                     source_type=source_type,
                                     background_appliances=background_appliance_list,
                                     nus_enabled=nus_enabled,
                                     nus_subsampling_ratio=nus_subsampling_ratio,
                                     nus_expansion_ratio=nus_expansion_ratio,
                                     us_enabled=us_enabled,
                                     us_subsampling_ratio=us_subsampling_ratio)

            for building, active_mains in source.sources[0]['source'].mains.items():
                if building not in self.mains_dict.keys():
                    self.max_sequence_length_dict[building] = 0
                    self.mains_dict[building] = {"active_power": active_mains}

                    if self.reactive_power:
                        self.mains_dict[building] = {"reactive_power": source.sources[0]['source'].r_mains[building]}

                if self.max_sequence_length_dict[building] < source.seq_length:
                    self.max_sequence_length_dict[building] = source.seq_length

            valid_data = source.get_batch(validation=True, valid_ratio=valid_ratio)

            self.source_dict.update({appliance: source})
            self.valid_batch_dict.update({appliance: valid_data})

    def close(self):
        print('Deleting sources.')
        del self.source_dict

        if not __debug__:
            print('Deleting temporary database.')
            os.remove(self.temp_dataset_filename)

    def get_valid_data(self, appliance):
        valid_batch = self.valid_batch_dict[appliance]
        x_valid, y_valid = valid_batch.data
        return x_valid, y_valid

    # def get_source(self, target_appliance, dataset_filename, logger, phase, seq_per_batch, main_mode, reactive_power,
    #                source_type, background_appliances, time_windows, test_mode=None, nus_enabled=False, nus_subsampling_ratio=10, nus_expansion_ratio=10,
    #                us_enabled=False, us_subsampling_ratio=10):
    #     raise NotImplemented

    def get_source(self, target_appliance, dataset_filename, logger, phase, seq_per_batch, main_mode, reactive_power,
                   time_windows, source_type, background_appliances='all', test_mode=None, nus_enabled=False, nus_subsampling_ratio=10, nus_expansion_ratio=10,
                   us_enabled=False, us_subsampling_ratio=10):

        if background_appliances == 'all':
            background_appliances = self.get_all_appliance_list()

        # return get_source(dataset=self.dataset_name,
        #                   time_windows=time_windows,
        #                   target_appliance=target_appliance,
        #                   dataset_filename=dataset_filename,
        #                   reactive_power=reactive_power,
        #                   logger=logger,
        #                   phase=phase,
        #                   test_mode=test_mode,
        #                   seq_per_batch=seq_per_batch,
        #                   main_mode=main_mode,
        #                   source_type=source_type,
        #                   background_appliances=background_appliances,
        #                   nus_enabled=nus_enabled,
        #                   nus_subsampling_ratio=nus_subsampling_ratio,
        #                   nus_expansion_ratio=nus_expansion_ratio,
        #                   us_enabled=us_enabled,
        #                   us_subsampling_ratio=us_subsampling_ratio)

        target_is_start_and_end_and_mean = False

        # code from get_source in utils/source_utils.py
        if reactive_power:
            n_inputs = 2
        else:
            n_inputs = 1

        appliances = [target_appliance]
        for appliance in background_appliances:
            if appliance != target_appliance:
                appliances.append(appliance)

        metadata = {
            'appliances': [],
            'max_appliance_powers': [],
            'on_power_thresholds': [],
            'min_on_durations': [],
            'min_off_durations': []
        }

        # target_appliance_metadata = get_appliance_metadata(target_appliance)
        target_appliance_metadata = get_dataset_metadata(self.dataset_name, target_appliance)
        sample_period = get_dataset_metadata(self.dataset_name, 'sample_period')

        if phase == 'train':
            train_buildings = target_appliance_metadata['train_buildings']
            validation_buildings = target_appliance_metadata['validation_buildings']
        elif phase == 'test':
            train_buildings = target_appliance_metadata['test_buildings'][test_mode]
            validation_buildings = target_appliance_metadata['test_buildings'][test_mode]
        else:
            raise ValueError('No valid phase given {}, only "test" or "train" accepted.')

        for appliance in appliances:
            appliance_metadata = get_dataset_metadata(self.dataset_name, appliance)
            for key in metadata.keys():
                metadata[key].append(appliance_metadata[key[:-1]])

        if target_appliance_metadata['train_building_real'] is None:
            target_appliance_metadata['train_building_real'] = target_appliance_metadata['train_buildings']

        # apply uniform subsampling
        if us_enabled:
            seq_length = target_appliance_metadata['seq_length'] // us_subsampling_ratio
            sample_period = sample_period * us_subsampling_ratio
        else:
            seq_length = target_appliance_metadata['seq_length']

        if source_type == 'real_appliance_source' or source_type == 'multisource':

            real_appliance_source1 = RealApplianceSource(
                n_inputs=n_inputs,
                logger=logger,
                filename=dataset_filename,
                appliances=metadata['appliances'],
                max_appliance_powers=metadata['max_appliance_powers'],
                on_power_thresholds=metadata['on_power_thresholds'],
                min_on_durations=metadata['min_on_durations'],
                min_off_durations=metadata['min_off_durations'],
                divide_input_by_max_input_power=False,
                window_per_building=time_windows,
                seq_length=seq_length,
                output_one_appliance=True,
                train_buildings=train_buildings,
                validation_buildings=validation_buildings,
                n_seq_per_batch=seq_per_batch,
                skip_probability=0.75,
                skip_probability_for_first_appliance=0.5,
                standardise_input=False,
                input_stats=target_appliance_metadata['input_stats'],
                independently_center_inputs=True,
                sample_period=sample_period,
                target_is_start_and_end_and_mean=target_is_start_and_end_and_mean,
                nus_enabled=nus_enabled,
                nus_subsampling_ratio=nus_subsampling_ratio,
                nus_expansion_ratio=nus_expansion_ratio,
                us_enabled=us_enabled,
                us_subsampling_ratio=us_subsampling_ratio
            )

            if source_type != 'multisource':
                multi_source = MultiSource(
                    sources=[
                        {
                            'source': real_appliance_source1,
                            'train_probability': 1,
                            'validation_probability': 1
                        }
                    ],
                    standardisation_source=real_appliance_source1,
                )

                return multi_source

        if source_type == 'same_location_source' or source_type == 'multisource':

            same_location_source1 = SameLocation(
                n_inputs=n_inputs,
                logger=logger,
                filename=dataset_filename,
                appliances=metadata['appliances'],
                target_appliance=target_appliance_metadata['appliance'],
                window_per_building=time_windows,
                seq_length=seq_length,
                train_buildings=train_buildings,
                validation_buildings=validation_buildings,
                n_seq_per_batch=seq_per_batch,
                skip_probability=0.5,
                standardise_input=False,
                offset_probability=1,
                divide_target_by=target_appliance_metadata['max_appliance_power'],
                input_stats=target_appliance_metadata['input_stats'],
                independently_center_inputs=True,
                main_mode=main_mode,
                on_power_threshold=target_appliance_metadata['on_power_threshold'],
                min_on_duration=target_appliance_metadata['min_on_duration'],
                min_off_duration=target_appliance_metadata['min_off_duration'],
                include_all=False,
                allow_incomplete=False,
                sample_period=sample_period,
                target_is_start_and_end_and_mean=target_is_start_and_end_and_mean,
                nus_enabled=nus_enabled,
                nus_subsampling_ratio=nus_subsampling_ratio,
                nus_expansion_ratio=nus_expansion_ratio,
                us_enabled=us_enabled,
                us_subsampling_ratio=us_subsampling_ratio
            )

            if source_type != 'multisource':
                multi_source = MultiSource(
                    sources=[
                        {
                            'source': same_location_source1,
                            'train_probability': 1,
                            'validation_probability': 1
                        }
                    ],
                    standardisation_source=same_location_source1,
                )

                return multi_source

        multi_source = MultiSource(
            sources=[
                {
                    'source': same_location_source1,
                    'train_probability': 0.5,
                    'validation_probability': 1
                },
                {
                    'source': real_appliance_source1,
                    'train_probability': 0.5,
                    'validation_probability': 0
                }
            ],
            standardisation_source=same_location_source1,
        )

        return multi_source

    def get_input_dimension(self, appliance):
        try:
            return {appliance: [self.source_dict[appliance].n_seq_per_batch,
                                self.source_dict[appliance].seq_length,
                                self.source_dict[appliance].n_inputs]}
        except (TypeError, ValueError) as err:
            return 'Error: {}'.format(err)

    def get_input_dimensions(self):
        input_dimensions = dict()

        for source_name in self.source_dict.keys():
            input_dimensions.update(self.get_input_dimension(source_name))

        return input_dimensions

    def get_output_dimension(self, appliance):
        try:
            return {appliance: [self.source_dict[appliance].n_seq_per_batch,
                                self.source_dict[appliance].seq_length,
                                1]}
        except (TypeError, ValueError) as err:
            return 'Error: {}'.format(err)

    def get_output_dimensions(self):
        output_dimensions = dict()

        for source_name in self.source_dict.keys():
            output_dimensions.update(self.get_output_dimension(source_name))

        return output_dimensions

    def get_batch(self, appliance, validation=False, valid_ratio=0.1, standardize=True):
        batch = self.source_dict[appliance].get_batch(validation=validation, valid_ratio=valid_ratio,
                                                      standardize=standardize)
        return batch.data

    def get_batches(self, validation=False, valid_ratio=0.1, standardize=True):
        batches = {}
        for source_name, source in self.source_dict.items():
            x, y = self.get_batch(source_name, validation=validation, valid_ratio=valid_ratio, standardize=standardize)
            batches.update({source_name: [x, y]})

        return batches

    def get_test_buildings(self, appliance, test_mode=None):

        if self.test_mode is None and test_mode is None:
            print("WARNING: Test mode not defined, returning 'None'.")
            buildings = None

        else:
            appliance_metadata = self.get_source_metadata(appliance)
            # Here we give priority to the specified test_mode over the (possibly set) 'self' one.
            if test_mode is not None:
                mode = test_mode
            else:
                mode = self.test_mode
            buildings = appliance_metadata["test_buildings"][mode]

        return buildings

    def get_mains(self, appliance, building=None, padding=None):
        mains = dict()
        source = self.source_dict[appliance]

        if building is None:
            for building, active_mains in source.sources[0]['source'].mains.items():
                mains[building] = [active_mains]
                if self.reactive_power:
                    mains[building].append(source.sources[0]['source'].r_mains[building])

        elif isinstance(building, int):
            mains[building] = [source.sources[0]['source'].mains[building]]
            if self.reactive_power:
                mains[building].append(source.sources[0]['source'].r_mains[building])

        if padding is not None:
            for building in mains.keys():
                if padding == "max_sequence_length":
                    # pad_width = self.max_sequence_length_dict[building]
                    pass
                elif padding == "appliance_sequence_length":
                    pad_width = source.seq_length
                elif isinstance(padding, int):
                    pad_width = padding
                else:
                    raise ValueError("Pad type not recognized {}, only 'max_sequence_length', "
                                     "'appliance_sequence_length', or 'int' number accepted.".format(padding))

                for main_i in range(len(mains[building])):

                    mains[building][main_i] = np.pad(mains[building][main_i], pad_width=pad_width, mode='constant')
                    # if nus_enabled:
                    #     # fix end padding to multiple integer of the given pad_with
                    #     pad_width_diff = len(mains[building][main_i]) % pad_width
                    #     mains[building][main_i] = np.pad(mains[building][main_i], pad_width=(pad_width, pad_width - pad_width_diff), mode='constant')

        for building in mains.keys():
            mains[building] = np.array(mains[building])

        if len(mains.keys()) == 1:
            mains = mains[mains.keys()[0]]

        return mains


class UkDaleDataSetReader(NILMDataSetReader):

    def __init__(self):
        super(UkDaleDataSetReader, self).__init__()

        self.dataset_name = 'ukdale'

    # def get_all_appliance_list(self):
    #     return get_dataset_metadata("ukdale", 'appliances_list')

    # def get_source(self, target_appliance, dataset_filename, logger, phase, seq_per_batch, main_mode, reactive_power,
    #                time_windows, source_type, background_appliances='all', test_mode=None, nus_enabled=False, nus_subsampling_ratio=10, nus_expansion_ratio=10,
    #                us_enabled=False, us_subsampling_ratio=10):
    #
    #     if background_appliances == 'all':
    #         background_appliances = self.get_all_appliance_list()
    #
    #     return get_source(dataset='ukdale',
    #                       time_windows=time_windows,
    #                       target_appliance=target_appliance,
    #                       dataset_filename=dataset_filename,
    #                       reactive_power=reactive_power,
    #                       logger=logger,
    #                       phase=phase,
    #                       test_mode=test_mode,
    #                       seq_per_batch=seq_per_batch,
    #                       main_mode=main_mode,
    #                       source_type=source_type,
    #                       background_appliances=background_appliances,
    #                       nus_enabled=nus_enabled,
    #                       nus_subsampling_ratio=nus_subsampling_ratio,
    #                       nus_expansion_ratio=nus_expansion_ratio,
    #                       us_enabled=us_enabled,
    #                       us_subsampling_ratio=us_subsampling_ratio)

    # def get_source_metadata(self, appliance=None):
    #     return get_dataset_metadata("ukdale", appliance)


class AmpdDataSetReader(NILMDataSetReader):

    def __init__(self):
        super(AmpdDataSetReader, self).__init__()

        self.dataset_name = 'ampds'

    # def get_all_appliance_list(self):
    #     return get_dataset_metadata("ampds", 'appliances_list')
    #
    # def get_source(self, target_appliance, dataset_filename, logger, reactive_power, phase, seq_per_batch, main_mode,
    #                time_windows, source_type, background_appliances='all', test_mode=None, nus_enabled=False, nus_subsampling_ratio=10, nus_expansion_ratio=10,
    #                us_enabled=False, us_subsampling_ratio=10):
    #
    #     if background_appliances == 'all':
    #         background_appliances = self.get_all_appliance_list()
    #
    #     return get_source(dataset='ampds',
    #                       time_windows=time_windows,
    #                       reactive_power=reactive_power,
    #                       target_appliance=target_appliance,
    #                       dataset_filename=dataset_filename,
    #                       logger=logger,
    #                       phase=phase,
    #                       test_mode=test_mode,
    #                       seq_per_batch=seq_per_batch,
    #                       main_mode=main_mode,
    #                       source_type=source_type,
    #                       background_appliances=background_appliances,
    #                       nus_enabled=nus_enabled,
    #                       nus_subsampling_ratio=nus_subsampling_ratio,
    #                       nus_expansion_ratio=nus_expansion_ratio,
    #                       us_enabled=us_enabled,
    #                       us_subsampling_ratio=us_subsampling_ratio)
    #
    # def get_source_metadata(self, appliance=None):
    #     return get_dataset_metadata("ampds", appliance)


class ReddDataSetReader(NILMDataSetReader):

    def __init__(self):
        super(ReddDataSetReader, self).__init__()

        self.dataset_name = 'redd'

    # def get_all_appliance_list(self):
    #     return get_dataset_metadata("redd", 'appliances_list')
    #
    # def get_source(self, target_appliance, dataset_filename, logger, reactive_power, phase, seq_per_batch, main_mode,
    #                time_windows, source_type, background_appliances='all', test_mode=None, nus_enabled=False, nus_subsampling_ratio=10, nus_expansion_ratio=10,
    #                us_enabled=False, us_subsampling_ratio=10):
    #     if background_appliances == 'all':
    #         background_appliances = self.get_all_appliance_list()
    #
    #     return get_source(dataset='redd',
    #                       time_windows=time_windows,
    #                       reactive_power=reactive_power,
    #                       target_appliance=target_appliance,
    #                       dataset_filename=dataset_filename,
    #                       logger=logger,
    #                       phase=phase,
    #                       test_mode=test_mode,
    #                       seq_per_batch=seq_per_batch,
    #                       main_mode=main_mode,
    #                       source_type=source_type,
    #                       background_appliances=background_appliances,
    #                       nus_enabled=nus_enabled,
    #                       nus_subsampling_ratio=nus_subsampling_ratio,
    #                       nus_expansion_ratio=nus_expansion_ratio,
    #                       us_enabled=us_enabled,
    #                       us_subsampling_ratio=us_subsampling_ratio)
    #
    # def get_source_metadata(self, appliance=None):
    #     from utils.source_utils import get_redd_appliance_metadata
    #
    #     return get_redd_appliance_metadata(appliance)


def get_dataset(dataset):
    if dataset == 'ukdale':
        return UkDaleDataSetReader()
    elif dataset == 'ampds':
        return AmpdDataSetReader()
    elif dataset == 'redd':
        return ReddDataSetReader()
    else:
        raise ValueError('{} dataset not supported.'.format(dataset))

# def get_dataset(params):
#
#     dataset = params["experiment"]["dataset"]
#     kwarg = {
#         "dataset_path": params["paths"]["dataset_path"],
#         "dataset_name": params["experiment"]["dataset"],
#         "target_appliance_list":[appliance],
#         "phase": "train",
#         "dataset_id":params["ids"]["hashed_id"],
#         "logger": logger,
#         "reactive_power":params["experiment"]["reactive_power"],
#         "seq_per_batch":params["experiment"]["sequences_per_batch"],
#         "main_mode":params["experiment"]["main_mode"],
#         "source_type":params["experiment"]["source_type"],
#         "time_windows":params["experiment"]["time_windows"],
#         "valid_ratio":params["experiment"]["valid_ratio"],
#         "background_appliance_list":params["experiment"]["background_appliance_list"],
#         "debug":params["debug"]
#     }
#
#     if dataset == 'ukdale':
#         return UkDaleDataSetReader()
#     elif dataset == 'ampds':
#         return AmpdDataSetReader()
#     else:
#         raise ValueError('{} dataset not supported.'.format(dataset))


# def read_dataset(dataset_name, trace, building, experiment_id, window, power_type="active", debug=True, nus_enabled=False, nus_subsampling_ratio=10, nus_expansion_ratio=10):
#
#     series = pd.Series()
#
#     if dataset_name == 'ukdale':
#         sample_period = 6
#     elif dataset_name == 'ampds':
#         sample_period = 60
#     else:
#         raise ValueError('Dataset {} not supported.'.format(dataset_name))
#
#     if debug:
#         # use original dataset
#         temp_dataset_filename = os.path.join(DB_DIR, dataset_name + ".h5")
#
#     else:
#         # copy dataset
#         temp_dataset_filename = os.path.join(DB_DIR, "{}_{}_{}.h5".format(dataset_name, trace, experiment_id))
#         shutil.copy(os.path.join(DB_DIR, dataset_name + ".h5"), temp_dataset_filename)
#
#     dataset = DataSet(temp_dataset_filename)
#     dataset.set_window(*window)
#     elec = dataset.buildings[building].elec
#
#     # if trace == "aggregate":
#     #     meter = elec.mains()
#     # else:
#     #     meter = elec[trace.replace("_", " ")]  # e.g. 'dish_washer' must be 'dish washer'
#
#     if trace == "aggregate":
#         meter = elec.mains()
#         if power_type == "active":
#             series = meter.power_series_all_data(
#                 sample_period=sample_period, physical_quantity='power', ac_type='active')
#
#             series = series.fillna(value=0.)  # To fill NaNs found after re-sampling.
#             # values = series.values
#         elif power_type == "reactive":
#             if dataset_name == 'ukdale':
#                 active_series = meter.power_series_all_data(
#                     sample_period=sample_period, physical_quantity='power', ac_type='active')
#                 apparent_series = meter.power_series_all_data(
#                     sample_period=sample_period, physical_quantity='power', ac_type='apparent')
#
#                 active_series = active_series.fillna(value=0.)  # To fill NaNs found after re-sampling.
#                 apparent_series = apparent_series.fillna(value=0.)  # To fill NaNs found after re-sampling.
#
#                 quad_aprt = np.square(apparent_series.values)
#                 quad_acti = np.square(active_series.values)
#                 values = np.sqrt(quad_aprt - quad_acti)
#
#             elif dataset_name == 'ampds':
#                 series = meter.power_series_all_data(
#                         sample_period=sample_period, physical_quantity='power', ac_type='reactive')
#
#                 series = series.fillna(value=0.)  # To fill NaNs found after re-sampling.
#                 # values = series.values
#
#             else:
#                 raise ValueError('Dataset {} not supported.'.format(dataset_name))
#
#         else:
#             raise ValueError('Mains type {} not supported.'.format(trace))
#     else:
#
#         meter = elec[trace.replace("_", " ")]  # e.g. 'dish_washer' must be 'dish washer'
#         if power_type == "active":
#             series = meter.power_series_all_data(sample_period=sample_period)
#         else:
#             if dataset_name == "ampds":
#                 series = meter.power_series_all_data(sample_period=sample_period, ac_type="reactive")
#             elif dataset_name == "ukdale":
#                 raise ValueError("No reactive power is provided as ground truth for UK-DALE.")
#             else:
#                 raise ValueError('Dataset {} not supported.'.format(dataset_name))
#
#         series = series.fillna(value=0.)  # To fill NaNs found after re-sampling.
#
#     if nus_enabled:
#
#         dataset_tz = dataset.metadata['timezone']
#         subsampled_labels = pd.Series(0, index=pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period), tz=dataset_tz))
#
#         # set to 1 the samples at subsample ratio
#         subsampled_ratio_indexes = pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period * nus_subsampling_ratio),
#                                                  tz=dataset.metadata.get('timezone'))
#         subsampled_labels[subsampled_ratio_indexes] = 1
#
#         # load ground truth for each appliance and specific building
#         for appl in APPLIANCES[dataset_name]:
#             csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.splitext(os.path.join(temp_dataset_filename))[0], building, appl.replace(' ', '_'))
#
#             # avoid ambigous error for date near dst
#             appliance_ground_truth = pd.Series.from_csv(csv_filename).tz_localize('UTC')
#             w0 = pd.Timestamp(window[0]).tz_localize(tz=dataset_tz).tz_convert('UTC')
#             w1 = pd.Timestamp(window[1]).tz_localize(tz=dataset_tz).tz_convert('UTC')
#             appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index < w0], inplace=True)
#             appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index > w1], inplace=True)
#             appliance_ground_truth = appliance_ground_truth.tz_convert(tz=dataset_tz)
#
#             for each in range(-nus_expansion_ratio // 2, nus_expansion_ratio // 2):
#                 subsampled_labels[appliance_ground_truth.index + timedelta(seconds=each * sample_period)] = 1
#
#         try:
#             # labels to drop
#             labels = subsampled_labels[subsampled_labels == 0].index
#             labels = labels[(labels >= series.index[0]) & (labels <= series.index[-1])]
#             # print("Activatoin length: {}".format(len(activation)))
#             if labels.date.any():
#                 # do avoid strage errors, drop labels only if exist
#                 # print("Activation length after subsampling: {}".format(len(activation)))
#                 series.drop(labels=labels, inplace=True)
#
#             if not series.any():
#                 raise ValueError("Activation series subsampled to empty series.")
#
#         except:
#             raise Exception("NUS - unhandled error in activations subsampling:\n")
#
#     values = series.values
#
#     # Temporary code to kill the bias in microwave's ground truth for building 5
#     # === #
#     if trace == "microwave" and dataset_name == "ukdale" and building == 5:
#         values[values < 53] = 0.
#     # === #
#
#     # series = series.dropna()  # To test differences in lengths, don't use it.
#
#     if not debug:
#         os.remove(temp_dataset_filename)
#
#     return values

_NUS_CACHE = dict()  # key: bulding_1, buildin_2


def read_dataset(dataset_name, trace, building, experiment_id, window, power_type="active", nus_enabled=False, nus_subsampling_ratio=10, nus_expansion_ratio=10,
                 us_enabled=False, us_subsampling_ratio=10):

    # if dataset_name == 'ukdale':
    #     sample_period = 6
    # elif dataset_name == 'ampds':
    #     sample_period = 60
    # elif dataset_name == 'redd':
    #     sample_period = 3
    # else:
    #     raise ValueError('Dataset {} not supported.'.format(dataset_name))

    series = pd.Series()

    key_cache = '{}_{}_{}_{}_{}_{}_{}_{}_{}_{}_{}'.format(dataset_name, trace, building, experiment_id, window, power_type, nus_enabled, nus_subsampling_ratio, nus_expansion_ratio,
                 us_enabled, us_subsampling_ratio)

    if key_cache not in _NUS_CACHE.keys():

        sample_period = get_dataset_metadata(dataset_name, 'sample_period')

        if __debug__:
            # use original dataset
            temp_dataset_filename = os.path.join(DB_DIR, dataset_name + ".h5")

        else:
            # copy dataset
            temp_dataset_filename = os.path.join(DB_DIR, 'temp_db', "{}_{}_{}.h5".format(dataset_name, trace, experiment_id))
            shutil.copy(os.path.join(DB_DIR, dataset_name + ".h5"), temp_dataset_filename)

        dataset = DataSet(temp_dataset_filename)
        dataset.set_window(*window)
        elec = dataset.buildings[building].elec
        dataset_tz = dataset.metadata['timezone']

        if us_enabled:
            sample_period = sample_period * us_subsampling_ratio

        # series = pd.Series(0, index=pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period), tz=dataset_tz, closed='left'))

        # if trace == "aggregate":
        #     meter = elec.mains()
        # else:
        #     meter = elec[trace.replace("_", " ")]  # e.g. 'dish_washer' must be 'dish washer'

        meter = elec.mains()
        if dataset_name == 'redd':
            series = meter.power_series_all_data(sample_period=sample_period)
            series -= series

        if trace == "aggregate":
            meter = elec.mains()
            if power_type == "active":
                series = meter.power_series_all_data(
                    sample_period=sample_period, physical_quantity='power', ac_type='active')
                series = series.fillna(value=0.)  # To fill NaNs found after re-sampling.
                values = series.values

            if power_type == "reactive":  # TODO: check dropnan as for "active"
                if dataset_name == 'ukdale':
                    active_series = meter.power_series_all_data(
                        sample_period=sample_period, physical_quantity='power', ac_type='active')
                    apparent_series = meter.power_series_all_data(
                        sample_period=sample_period, physical_quantity='power', ac_type='apparent')

                    active_series = active_series.fillna(value=0.)  # To fill NaNs found after re-sampling.
                    apparent_series = apparent_series.fillna(value=0.)  # To fill NaNs found after re-sampling.

                    quad_aprt = np.square(apparent_series.values)
                    quad_acti = np.square(active_series.values)
                    values = np.sqrt(quad_aprt - quad_acti)

                elif dataset_name == 'ampds':
                    series = meter.power_series_all_data(
                        sample_period=sample_period, physical_quantity='power', ac_type='reactive')

                    series = series.fillna(value=0.)  # To fill NaNs found after re-sampling.
                    # values = series.values

                else:
                    raise ValueError('Dataset {} not supported.'.format(dataset_name))

            else:
                raise ValueError('Mains type {} not supported.'.format(trace))
        elif isinstance(trace, list):
            for appliance in trace:
                meter = elec[appliance.replace("_", " ")]  # e.g. 'dish_washer' must be 'dish washer'
                if power_type == "active":
                    _series = meter.power_series_all_data(sample_period=sample_period)
                else:
                    if dataset_name == "ampds":
                        _series = meter.power_series_all_data(sample_period=sample_period, ac_type="reactive")
                    elif dataset_name == "ukdale":
                        raise ValueError("No reactive power is provided as ground truth for UK-DALE.")
                    else:
                        raise ValueError('Dataset {} not supported.'.format(dataset_name))

                # series += _series.fillna(value=0.)  # To fill NaNs found after re-sampling.
                if series.any():
                    series += _series
                else:
                    series = _series.copy()
                series = series.dropna()

        else:
            raise ValueError("Trace must be str or list of appliances.")

        # if np.isnan(series).any():
        #     series = series.fillna(value=0.)

        if __debug__:
            original_main_length = len(series)
        #
        #     from scipy import signal
        #     import matplotlib.pyplot as plt
        #
        #     f, t, Sxx = signal.spectrogram(series.values, 1.0/sample_period)
        #     plt.pcolormesh(t, f, Sxx)
        #     plt.ylabel('Frequency [Hz]')
        #     plt.xlabel('Time [sec]')
        #     plt.show()

        if nus_enabled:  # and 'building_{}'.format(building) not in _NUS_CACHE.keys():

            subsampled_labels = pd.Series(0, index=pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period), tz=dataset_tz, closed='left'))

            # set to 1 the samples at subsample ratio
            subsampled_ratio_indexes = pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period * nus_subsampling_ratio),
                                                     tz=dataset.metadata.get('timezone'), closed='left')
            subsampled_labels[subsampled_ratio_indexes] = 1

            # load ground truth for each appliance and specific building
            # for appl in APPLIANCES[dataset_name]:
            for appl in get_dataset_metadata(dataset_name, 'appliances_list'):

                # for REDD dataset missing "washer dryer" in building 2
                if dataset_name == 'redd' and building == 2 and appl == 'washer_dryer':
                    continue

                if __debug__:
                    csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.splitext(os.path.join(temp_dataset_filename))[0], building, appl.replace(' ', '_'))
                else:
                    tmp_csv_dir = os.path.dirname(os.path.dirname(temp_dataset_filename))
                    csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.join(tmp_csv_dir, dataset_name), building, appl.replace(' ', '_'))

                # avoid ambigous error for date near dst
                appliance_ground_truth = pd.Series.from_csv(csv_filename).tz_localize('UTC')
                w0 = pd.Timestamp(window[0]).tz_localize(tz=dataset_tz).tz_convert('UTC')
                w1 = pd.Timestamp(window[1]).tz_localize(tz=dataset_tz).tz_convert('UTC')
                appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index < w0], inplace=True)
                appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index > w1], inplace=True)
                appliance_ground_truth = appliance_ground_truth.tz_convert(tz=dataset_tz)

                for each in range(-nus_expansion_ratio // 2, nus_expansion_ratio // 2):
                    # subsampled_labels[appliance_ground_truth.index + timedelta(seconds=each * sample_period)] = 1
                    expansion_indices = appliance_ground_truth.index + timedelta(seconds=each * sample_period)
                    # check if expansion indices in subsampled_labels range, remove outliers
                    expansion_indices = expansion_indices[(expansion_indices >= subsampled_labels.index[0]) & (expansion_indices <= subsampled_labels.index[-1])]
                    subsampled_labels[expansion_indices] = 1

            try:
                # labels to drop
                labels = subsampled_labels[subsampled_labels == 0].index
                labels = labels[(labels >= series.index[0]) & (labels <= series.index[-1])]
                # print("Activatoin length: {}".format(len(activation)))
                if labels.date.any():
                    # to avoid strage errors, drop labels only if exist
                    # print("Activation length after subsampling: {}".format(len(activation)))
                    # series.drop(labels=labels, inplace=True)
                    series = series.tz_convert('UTC').drop(labels=labels.tz_convert('UTC')).tz_convert(tz=dataset_tz)

                if not series.any():
                    raise ValueError("Activation series subsampled to empty series.")

                #_NUS_CACHE.update({'building_{}'.format(building): labels})  # caching labels

            except Exception as err:
                # raise Exception("NUS - unhandled error in activations subsampling:\n")
                print("NUS - unhandled error in activations subsampling:")
                print(err)

        # elif nus_enabled and 'building_{}'.format(building) in _NUS_CACHE.keys():
        #     # use cached labels
        #     labels = _NUS_CACHE.get('building_{}'.format(building))
        #
        #     if labels.date.any():
        #         # to avoid strage errors, drop labels only if exist
        #         # print("Activation length after subsampling: {}".format(len(activation)))
        #         # series.drop(labels=labels, inplace=True)
        #         series = series.tz_convert('UTC').drop(labels=labels.tz_convert('UTC')).tz_convert(tz=dataset_tz)
        #
        #     if not series.any():
        #         raise ValueError("Activation series subsampled to empty series.")

        # values = series.values

        if __debug__:
            nus_main_length = len(series)
            print("Dataset stats for building {}".format(building))
            print("Original dataset length: {}".format(original_main_length))
            print("NUS dataset length: {}".format(nus_main_length))
            print("NUS overall reduction: {:.2f}".format(100 * float(nus_main_length)/original_main_length))

            # from scipy import signal
            # import matplotlib.pyplot as plt
            #
            # f, t, Sxx = signal.spectrogram(series.values, 1.0/(sample_period*20))
            # plt.pcolormesh(t, f, Sxx)
            # plt.ylabel('Frequency [Hz]')
            # plt.xlabel('Time [sec]')
            # plt.show()

        # Temporary code to kill the bias in microwave's ground truth for building 5
        # === #
        if trace == "microwave" and dataset_name == "ukdale" and building == 5:
            series.values[series.values < 53] = 0.
        # === #

        # series = series.dropna()  # To test differences in lengths, don't use it.

        if not __debug__:
            os.remove(temp_dataset_filename)

        _NUS_CACHE.update({key_cache: series})
        # print("CACHING")

    else:
        series = _NUS_CACHE.get(key_cache)
        # print("CACHED")

    return series  # series.values
