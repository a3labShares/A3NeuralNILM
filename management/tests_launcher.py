import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import yaml
from default_configs import DB_DIR
from main import test, evaluate
from utils.source_metadata import get_dataset_metadata
from tables.file import _open_files
import gc

EXP_DIR = '/home/marco/Workspace/Univ/NILM/Test vari/experiments_resutls_with_interpolation/experiments_ukdale_denoised_sr20_er20/'

strides = [1, 8, 16, 32]
test_modes = ['seen', 'unseen']
disag_types = ['median', 'mean']

for stride in strides:
    for test_mode in test_modes:
        for disag_type in disag_types:
            for dir in os.listdir(EXP_DIR):
                if os.path.isdir(os.path.join(EXP_DIR, dir)):
                    for appliance in os.listdir(os.path.join(EXP_DIR, dir)):
                        if os.path.isdir(os.path.join(EXP_DIR, dir, appliance)) and 'test' not in appliance.split('_'):

                            file_config = os.path.join(EXP_DIR, dir, appliance, 'config.yaml')
                            with open(file_config, 'r') as f:
                                params = yaml.load(f)

                            params["phases"]["train"] = False
                            params["experiment"]["disag_type"] = disag_type  # 'median' or 'mean'

                            # fix paths
                            params['paths']['dataset_path'] = DB_DIR
                            params['paths']['appliance_paths'][appliance] = os.path.join(EXP_DIR, dir, appliance)
                            params['paths']['base_paths'] = EXP_DIR
                            params['paths']['run_paths'][appliance] = os.path.join(EXP_DIR, dir)

                            params["test"]["stride"] = stride
                            params["test"]["test_mode"] = test_mode

                            appliance_test_folder = os.path.join(EXP_DIR, dir, appliance + '_test_' + test_mode + '_' + disag_type + '_stride_' + str(stride))
                            params['paths']['test_paths'][appliance] = appliance_test_folder

                            if params.get('nus_enabled'):
                                params["ids"]["hashed_id"] = 'sr' + str(params['nus_subsampling_ratio']) + '_er' + str(params['nus_expansion_ratio']) + \
                                                             '_test_' + test_mode + '_' + disag_type + '_stride_' + str(stride)
                            elif params.get('us_enabled'):
                                params["ids"]["hashed_id"] = 'sr' + str(params['nus_subsampling_ratio']) + \
                                                             '_test_' + test_mode + '_' + disag_type + '_stride_' + str(stride)
                            else:
                                params["ids"]["hashed_id"] = '_test_' + test_mode + '_' + disag_type + '_stride_' + str(stride)

                            params["experiment"]["source_type"] = "same_location_source"
                            params["overwrite"] = False

                            # # fix appliances unicode name u'sds' -> 'sds'
                            # params["appliances"] = [str(x) for x in params["appliances"]]

                            print(appliance_test_folder)

                            # if not os.path.isfile(os.path.join(params['paths']['run_paths'][appliance], appliance, appliance + '_activations.h5')):
                            #     # activations must exist
                            #     continue

                            metadata = get_dataset_metadata(params['experiment']['dataset'], appliance)

                            # dir exist
                            if os.path.isdir(appliance_test_folder):

                                test_buildings = metadata['test_buildings'][test_mode]

                                # check if scores and csv exist
                                test_check = list()
                                evaluate_check = list()
                                for building_i in test_buildings:
                                    # test checks
                                    if os.path.isfile(os.path.join(appliance_test_folder, appliance + "_scores_building_{}.yaml".format(building_i))):
                                        test_check.append(True)
                                    else:
                                        test_check.append(False)

                                    # evaluation check
                                    if os.path.isfile(os.path.join(appliance_test_folder, appliance + "_estimates_building_{}.csv".format(building_i))):
                                        evaluate_check.append(True)
                                    else:
                                        evaluate_check.append(False)

                                if all(test_check):
                                    print("Appliance already tested!")
                                    continue  # scores already computed

                                # check if estimates exist, only evaluation phase required
                                # evaluate_check = list()
                                # for building_i in test_buildings:
                                #     if os.path.isfile(os.path.join(appliance_test_folder, appliance + "_estimates_building_{}.csv".format(building_i))):
                                #         evaluate_check.append(True)
                                #     else:
                                #         evaluate_check.append(False)

                                if all(evaluate_check):
                                    print("\n\nEvaluating " + appliance)
                                    evaluate(params)
                                    continue

                                # print("\n\nTesting " + appliance + "\n\n")
                                # test(params)
                                # print("\n\nEvaluating " + appliance + "\n\n")
                                # evaluate(params)

                            # else:  # create new dir
                            #
                            #     os.mkdir(appliance_test_folder, )
                            #     # print("\n\nTesting " + appliance + "\n\n")
                            #     # test(params)
                            #     print("\n\nEvaluating " + appliance + "\n\n")
                            #     evaluate(params)

            # free memory from h5 temp data
            # _open_files.close_all()
            # gc.collect()

print('DONE.')
