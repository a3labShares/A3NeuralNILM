import argparse
import os
from utils import io_utils
import yaml
import hashlib
from default_configs import DEFAULT_PARAMS
from utils.dataset_utils import get_dataset


def params_setup(params=None):

    # if parameter_file:
    #     with open(parameter_file, 'r') as tmp_file:
    #         params = yaml.load(tmp_file)
    #         tmp_file.close()
    #     os.remove(parameter_file)
    # elif params is None:
    #     params = DEFAULT_PARAMS

    if params is None:
        params = DEFAULT_PARAMS

    check_or_create_path(params["paths"]["base_path"])

    if params["appliances"] == "all":
        params["appliances"] = get_dataset(params["experiment"]["dataset"]).get_all_appliance_list()

    if params["experiment"]["main_mode"] == "noised":
        params["experiment"]["background_appliance_list"] = "all"

    if params["experiment"]["background_appliance_list"] == ["all"]:
        params["experiment"]["background_appliance_list"] = "all"

    if params["experiment"]["background_appliance_list"] != "all":
        params["experiment"]["background_appliance_list"] = sorted(params["experiment"]["background_appliance_list"])

    if not params["experiment"]["constrained_train"]:
        params["experiment"]["constrained_train_params"] = 'none'
    else:
        params["experiment"]["optimizer"] = 'none'

    if not params["test"]["constrained_test"]:
        params["test"]["constrained_test_params"] = 'none'

    params.update({"ids": {"run_ids": {}, "test_ids": {}, "concatenated_id": "none"}})
    params["paths"].update({"run_paths": {}, "appliance_paths": {}, "test_paths": {}})

    is_new_run = False
    is_new_test = False

    for appliance in params["appliances"]:

        run_params_dict = {"configuration": {"appliance_configuration": params["configurations"][appliance]}}
        if params["experiment"]["constrained_train"]:
            constrained_configs = {}
            for appl in params["appliances"]:
                constrained_configs[appl] = params["configurations"][appl]
            run_params_dict["configuration"]["constrained_configurations"] = constrained_configs

        run_params_dict["experiment"] = params["experiment"]
        run_id = get_run_id(run_params_dict, params["paths"]["base_path"], "run.json")
        params["ids"]["run_ids"][appliance] = run_id
        params["paths"]["run_paths"][appliance] = os.path.join(params["paths"]["base_path"], run_id)
        check_or_create_path(params["paths"]["run_paths"][appliance])

        io_utils.save_json(path=params["paths"]["run_paths"][appliance],
                           filename="run.json",
                           dictionary=run_params_dict)

        params["paths"]["appliance_paths"][appliance] = os.path.join(params["paths"]["run_paths"][appliance],
                                                                     appliance)
        is_new_appliance_run = check_or_create_path(params["paths"]["appliance_paths"][appliance])

        if not is_new_appliance_run:
            # appliance and config already trained? check activation
            if os.path.isfile(os.path.join(params["paths"]["appliance_paths"][appliance], appliance + "_activations.h5")):
                continue

        test_param_dict = {"test": params["test"]}
        if params["test"]["constrained_test"]:
            constrained_configs = {}
            for appl in params["appliances"]:
                constrained_configs[appl] = params["configurations"][appl]
            test_param_dict["constrained_configs"] = constrained_configs

        test_id = get_id(test_param_dict, params["paths"]["run_paths"][appliance], "test.json", "test")
        params["ids"]["test_ids"][appliance] = test_id
        params["paths"]["test_paths"][appliance] = os.path.join(params["paths"]["run_paths"][appliance], test_id)
        is_new_test_path = check_or_create_path(params["paths"]["test_paths"][appliance])
        is_new_appliance_test = is_new_test_path
        if not is_new_appliance_test:
            test_files = os.listdir(params["paths"]["test_paths"][appliance])
            dummy_dataset = get_dataset(params["experiment"]["dataset"])
            source_metadata = dummy_dataset.get_source_metadata(appliance)
            test_buildings = source_metadata["test_buildings"][params["test"]["test_mode"]]
            for building in test_buildings:
                if "{}_estimates_building_{}.csv".format(appliance, building) not in test_files:
                    is_new_appliance_test = True
                if "{}_scores_building_{}.yaml".format(appliance, building) not in test_files:
                    is_new_appliance_test = True

        io_utils.save_json(path=params["paths"]["test_paths"][appliance],
                           filename="test.json",
                           dictionary=test_param_dict)

        if params["ids"]["concatenated_id"] == "none":
            params["ids"]["concatenated_id"] = "{}_{}".format(params["ids"]["run_ids"][appliance],
                                                              params["ids"]["test_ids"][appliance])
        else:
            params["ids"]["concatenated_id"] += "{}_{}".format(params["ids"]["run_ids"][appliance],
                                                               params["ids"]["test_ids"][appliance])

        params["ids"]["concatenated_id"] += "_{}".format(appliance)

        if not is_new_run:
            is_new_run = is_new_appliance_run

        if not is_new_test:
            is_new_test = is_new_appliance_test

        params["ids"]["hashed_id"] = hashlib.md5(params["ids"]["concatenated_id"]).hexdigest()

        # always save complete config (e.g.: rerun with paths)
        with open(os.path.join(params["paths"]["appliance_paths"][appliance], "config.yaml"), 'w') as f:
            yaml.dump(params, f, default_flow_style=False)

    return params, is_new_run, is_new_test


def load_params(parameter_file=None):  # used in main to load json from mian_launcher or use DEFAULT config

    if parameter_file:
        with open(parameter_file, 'r') as f:
            # params = json.load(f)
            params = yaml.load(f)
    else:
        params, _, _ = params_setup()  # load DEFAULT params

    return params


def check_or_create_path(path):
    if os.path.isdir(path):
        print "{} path already exists, not overwriting.".format(path)
        return False
    else:
        os.mkdir(path)
        print "{} directory created.".format(path)
        return True


def get_id(params, dirpath, filename, folder_prefix):
    dirnames = os.listdir(dirpath)
    dirnames = sorted(dirnames)
    identifier = None
    dir_counter = 0
    requested_dir_position = -1

    while identifier is None and dir_counter < len(dirnames):
        if folder_prefix == dirnames[dir_counter].split("_")[0]:
            requested_dir_position = dir_counter
            current_json = io_utils.load_json(os.path.join(dirpath, dirnames[dir_counter]), filename)
            if params == current_json:
                identifier = dirnames[dir_counter]
        dir_counter += 1

    if identifier is None:
        if requested_dir_position == -1:
            identifier = "{}_{}".format(folder_prefix, format(1, '05d'))
        else:
            last_dirname = dirnames[requested_dir_position]
            last_id_number = last_dirname.split("_")[1]
            identifier = "{}_{}".format(folder_prefix, format(int(last_id_number) + 1, '05d'))

    return identifier


def get_run_id(run_params, dirpath, filename):

    exp_params = run_params["experiment"]
    config_params = run_params["configuration"]
    dirnames = os.listdir(dirpath)
    dirnames = sorted(dirnames)
    identifiers = [None, None]
    last_ids = [0, 0]
    dir_counter = 0

    while any(identifiers) is not None and dir_counter < len(dirnames):
        if dirnames[dir_counter].split("_")[0] == 'config' and dirnames[dir_counter].split("_")[2] == 'exp':
            run_params = io_utils.load_json(os.path.join(dirpath, dirnames[dir_counter]), filename)
            if run_params["configuration"] == config_params:
                identifiers[0] = dirnames[dir_counter].split("_")[1]
            elif int(dirnames[dir_counter].split("_")[1]) > last_ids[0]:
                last_ids[0] = int(dirnames[dir_counter].split("_")[1])

            # if run_params["experiment"] == exp_params:
            #     identifiers[1] = dirnames[dir_counter].split("_")[3]
            # elif int(dirnames[dir_counter].split("_")[3]) > last_ids[1]:
            #     last_ids[1] = int(dirnames[dir_counter].split("_")[3])
            identifiers[1] = dirnames[dir_counter].split("_")[3]

        dir_counter += 1

    for identifier_i, identifier in enumerate(identifiers):
        if identifier is None:
            identifiers[identifier_i] = format(last_ids[identifier_i] + 1, '05d')

    return "config_{}_exp_{}".format(identifiers[0], identifiers[1])


def str2bool(v):
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')



