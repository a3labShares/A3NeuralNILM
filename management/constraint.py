from __future__ import division
import gc
import os
import shutil
import numpy as np
from utils.net_utils import get_joint_network, compile_joined_net, train_loop
from utils.dataset_utils import get_dataset
from utils.disaggregate import mains_to_batches
from netcreator.searcher import get_network
from neuralnilm.experiment import configure_logger


def constrained_train(params):
    logger_filename = os.path.join(params["paths"]["appliance_paths"][params["appliances"][0]], "constrained_train.log")
    logger = configure_logger(logger_filename)

    train_dataset = get_dataset(params["experiment"]["dataset"])

    train_dataset.load(dataset_id=params["ids"]["concatenated_id"],
                       dataset_path=params["paths"]["dataset_path"],
                       phase="train",
                       reactive_power=params["experiment"]["reactive_power"],
                       target_appliance_list=params["appliances"],
                       time_windows=params["experiment"]["time_windows"],
                       appliance_paths=params["paths"]["appliance_paths"],
                       denoised_mains=params["experiment"]["denoised_mains"],
                       generate_synthetic_input=params["experiment"]["generate_synthetic_input"],
                       background_appliance_list=params["experiment"]["background_appliance_list"],
                       valid_ratio=params["experiment"]["valid_ratio"],
                       logger=logger)

    max_power_dict = dict()
    constrained_net_dict = dict()
    x_valid_dict = dict()
    y_valid_dict = dict()
    max_power_sum = 0

    try:
        for appliance_i, appliance in enumerate(params["appliances"]):
            appliance_metadata = train_dataset.get_appliance_metadata(appliance)
            max_power_dict[appliance] = appliance_metadata["max_power"]
            max_power_sum += appliance_metadata["max_power"]
            net = get_network(architecture=params["experiment"]["architecture"],
                              input_dims=train_dataset.get_appliance_input_dimension(appliance),
                              output_dims=train_dataset.get_appliance_output_dimension(appliance),
                              input_params=params["configurations"][appliance],
                              name=appliance)
            constrained_net_dict[appliance] = net

        logger.info('Joining networks...')

        model, output_appliance_list = get_joint_network(net_dict=constrained_net_dict,
                                                         stride=params["experiment"]["constrained_train_params"]["stride"],
                                                         max_power_dict=max_power_dict,
                                                         max_power_sum=max_power_sum)

        logger.info('Done')

        logger.info('Generating valid data...')
        for target_appliance in params["appliances"]:

            x_valid, y_valid = train_dataset.gen_batch(batch_size=params["experiment"]["valid_batch_size"],
                                                       target_appliance=target_appliance,
                                                       validation=True,
                                                       output_appliance_list=output_appliance_list)

            x_valid_dict[target_appliance] = x_valid
            y_valid_dict[target_appliance] = y_valid

        logger.info("Start pre-training.")

        for target_appliance in params["appliances"]:
            # Shut down back-propagation for non-target appliances
            for appliance in params["appliances"]:
                if appliance != target_appliance:
                    constrained_net_dict[appliance].model.trainable = False

            logger.info("Pre-training {}...".format(target_appliance))

            pre_train_params = copy.deepcopy(params["experiment"]["constrained_train_params"])
            pre_train_params["sum_loss_weight"] = 0.

            model = compile_joined_net(model=model,
                                       output_appliance_list=output_appliance_list,
                                       target_appliance=target_appliance,
                                       constraint_params=pre_train_params,
                                       optimizer=params["experiment"]["optimizer"])

            train_output_dict = train_loop(epochs=params["experiment"]["constrained_train_params"]["pre_train_epochs"],
                                           epoch_check=params["experiment"]["epoch_check"],
                                           early_stopping=params["experiment"]["early_stopping"],
                                           train_dataset=train_dataset,
                                           target_appliance=target_appliance,
                                           model=model,
                                           train_batch_size=params["experiment"]["train_batch_size"],
                                           auto_learning_rate_change=params["experiment"]["auto_learning_rate_change"],
                                           max_no_improvement_delta=params["experiment"]["max_no_improvement_delta"],
                                           logger=logger,
                                           output_appliance_list=output_appliance_list,
                                           x_valid=x_valid_dict[target_appliance],
                                           y_valid=y_valid_dict[target_appliance])

            model.set_weights(train_output_dict["best_weights"])

            logger.info("Pre-training for {} ended at epoch: {}."
                        .format(target_appliance, train_output_dict["last_epoch"]))

        logger.info("Pre-training ended.")

        logger.info("Start training.")

        for appliance in params["appliances"]:
            constrained_net_dict[appliance].model.trainable = True

        for target_appliance in params["appliances"]:
            # Shut down back-propagation for non-target appliances
            logger.info("Training {}...".format(target_appliance))

            model = compile_joined_net(model=model,
                                       output_appliance_list=output_appliance_list,
                                       target_appliance=target_appliance,
                                       constraint_params=params["experiment"]["constrained_train_params"],
                                       optimizer=params["experiment"]["optimizer"])

            train_output_dict = train_loop(logger=logger,
                                           epochs=params["experiment"]["constrained_train_params"]["pre_train_epochs"],
                                           epoch_check=params["experiment"]["epoch_check"],
                                           early_stopping=params["experiment"]["early_stopping"],
                                           train_dataset=train_dataset,
                                           target_appliance=target_appliance,
                                           model=model,
                                           train_batch_size=params["experiment"]["train_batch_size"],
                                           auto_learning_rate_change=params["experiment"]["auto_learning_rate_change"],
                                           max_no_improvement_delta=params["experiment"]["max_no_improvement_delta"],
                                           output_appliance_list=output_appliance_list,
                                           x_valid=x_valid_dict[target_appliance],
                                           y_valid=y_valid_dict[target_appliance])

            model.set_weights(train_output_dict["best_weights"])

            logger.info("Training for {} ended at epoch: {}.".format(target_appliance, train_output_dict["last_epoch"]))

        for net_name, net in constrained_net_dict.items():
            net.save(net_path=params["paths"]["appliance_paths"][net_name],
                     activation_filename="{}_activations.h5".format(net_name),
                     spec_filename="{}_specs.json".format(net_name),
                     overwrite=True)

    finally:
        train_dataset.close()
        for appliance in params["appliances"][1:]:
            shutil.copy(logger_filename,
                        os.path.join(params["paths"]["appliance_paths"][appliance], logger_filename.split("/")[-1]))


def gated_constrained_train(params):
    logger_filename = os.path.join(params["paths"]["appliance_paths"][params["appliances"][0]],
                                   "gated_constrained_train.log")

    logger = configure_logger(logger_filename)

    train_dataset = get_dataset(params["experiment"]["dataset"])

    train_dataset.load(dataset_id=params["ids"]["concatenated_id"],
                       dataset_path=params["paths"]["dataset_path"],
                       phase="train",
                       reactive_power=params["experiment"]["reactive_power"],
                       target_appliance_list=params["appliances"],
                       time_windows=params["experiment"]["time_windows"],
                       appliance_paths=params["paths"]["appliance_paths"],
                       denoised_mains=params["experiment"]["denoised_mains"],
                       generate_synthetic_input=params["experiment"]["generate_synthetic_input"],
                       background_appliance_list=params["experiment"]["background_appliance_list"],
                       valid_ratio=params["experiment"]["valid_ratio"],
                       logger=logger)

    max_power_dict = dict()
    net_dict = dict()
    x_valid_dict = dict()
    y_valid_dict = dict()
    max_power_sum = 0

    try:
        for appliance_i, appliance in enumerate(params["appliances"]):
            appliance_metadata = train_dataset.get_appliance_metadata(appliance)
            max_power_dict[appliance] = appliance_metadata["max_power"]
            max_power_sum += appliance_metadata["max_power"]
            net = get_network(architecture=params["experiment"]["architecture"],
                              input_dims=train_dataset.get_appliance_input_dimension(appliance),
                              output_dims=train_dataset.get_appliance_output_dimension(appliance),
                              input_params=params["configurations"][appliance],
                              name=appliance)
            net_dict[appliance] = net

        logger.info('Joining networks...')

        model_dict, constrained_model_dict = get_constrained_models(net_dict=net_dict,
                                                                    appliance_list=params["appliances"],
                                                                    max_power_dict=max_power_dict,
                                                                    stride=params["experiment"][
                                                                        "constrained_train_params"]["stride"],
                                                                    max_power_sum=max_power_sum)

        # New compile, in development
        for model in model_dict.values():
            optimizer = optimizers.get(params["experiment"]["optimizer"])
            model.compile(optimizer=optimizer, loss=params["experiment"]["constrained_train_params"]["appliance_loss"])

        constraint_weights_dict = {}
        for appliance, model in constrained_model_dict.items():
            optimizer = optimizers.get(params["experiment"]["optimizer"])
            constraint_weights_dict[appliance] = K.variable(1.)
            model.compile(optimizer=optimizer, loss="mse", loss_weights=constraint_weights_dict[appliance])

        # New train phase, in development
        valid_batch_dict = {}
        stop_dict = {}
        best_f1_score_dict = {}
        no_improvement_counter_dict = {}
        for appliance in params["appliances"]:
            x_valid, y_valid = train_dataset.gen_batch(batch_size=params["experiment"]["valid_batch_size"],
                                                       target_appliance=appliance,
                                                       validation=True)
            y_valid = y_valid[:, :, 0]

            valid_batch_dict[appliance] = (x_valid, y_valid)
            stop_dict[appliance] = False
            best_f1_score_dict[appliance] = 0.
            no_improvement_counter_dict[appliance] = 0

        epoch = 1
        stop = False

        while epoch <= params["experiment"]["epochs"] and not stop:
            for appliance_i, appliance in enumerate(params["appliances"]):
                x_train, y_train = train_dataset.gen_batch(batch_size=params["experiment"]["valid_batch_size"],
                                                           target_appliance=appliance,
                                                           output_appliance_list=params["appliances"],
                                                           validation=True)

                model_dict[appliance].train_on_batch(x_train, y_train[appliance_i])

                loss = model_dict[appliance].evaluate(x_train, y_train[appliance_i])
                constraint_weights_dict[appliance].set_value(loss)

                x_aggregate = []
                for eval_appliance in params["appliances"]:
                    if eval_appliance == appliance:
                        x_aggregate.append(x_train)
                    else:
                        x_aggregate.append(model_dict[eval_appliance].predict(x_train))

                constrained_model_dict[appliance].train_on_batch(x_aggregate, y_train[-1])

        logger.info('Done')
    finally:
        train_dataset.close()

def constrained_test(params):
    logger_filename = os.path.join(params["paths"]["test_paths"][params["appliances"][0]], "constrained_test.log")
    logger = configure_logger(logger_filename)

    test_dataset = get_dataset(params["experiment"]["dataset"])

    test_dataset.load(dataset_path=params["paths"]["dataset_path"],
                      dataset_name=params["experiment"]["dataset"],
                      target_appliance_list=[params["appliances"][0]],
                      time_windows=params["test"]["time_windows"],
                      test_mode=params["test"]["test_mode"],
                      dataset_id="test_" + params["ids"]["concatenated_id"],
                      logger=logger,
                      phase='test',
                      valid_ratio=1.,
                      reactive_power=params["experiment"]["reactive_power"],
                      seq_per_batch=params["experiment"]["sequences_per_batch"],
                      main_mode=params["experiment"]["main_mode"],
                      source_type=params["experiment"]["source_type"],
                      background_appliance_list=params["experiment"]["background_appliance_list"])

    generic_metadata = test_dataset.get_source_metadata()

    max_power_dict = dict()
    constrained_net_dict = dict()
    estimate_dict = dict()
    max_power_sum = 0

    try:
        for appliance_i, appliance in enumerate(params["appliances"]):
            appliance_metadata = test_dataset.get_source_metadata(appliance)
            max_power_dict[appliance] = appliance_metadata["max_appliance_power"]
            max_power_sum += appliance_metadata["max_appliance_power"]
            net = get_network(architecture=params["experiment"]["architecture"])
            net.load(net_path=params["paths"]["appliance_paths"][appliance],
                     spec_filename="{}_specs.json".format(appliance),
                     activation_filename="{}_activations.h5".format(appliance))
            constrained_net_dict[appliance] = net
            estimate_dict[appliance] = {}

        logger.info('Joining networks...')

        model, output_appliance_list = get_joint_network(constrained_net_dict,
                                                         stride=params["test"]["constrained_test_params"]["stride"],
                                                         max_power_dict=max_power_dict,
                                                         max_power_sum=max_power_sum,
                                                         constraint_window=params["test"]["constrained_test_params"]["constraint_window"],
                                                         freeze_layers=params["test"]["constrained_test_params"]["freeze_layers"])

        model = compile_joined_net(model, output_appliance_list, params["test"]["constrained_test_params"])

        original_weights = model.get_weights()

        logger.info('Done.')

        logger.info('Predicting with joined network...')
        sequence_length = model.input_shape[1]

        if params["test"]["stride"] == "sequence_length":
            params["test"]["stride"] = sequence_length

        if sequence_length == params["test"]["stride"]:
            overlapping_sequences = 0
        elif sequence_length > params["test"]["stride"]:
            overlapping_sequences = sequence_length // params["test"]["stride"]
        else:
            raise ValueError("Found stride {} to be higher than sequence length {}.".format(params["test"]["stride"],
                                                                                            sequence_length))

        test_buildings = test_dataset.get_test_buildings(params["appliances"][0])

        for building in test_buildings:
            # Pad mains according to constrained net's input length
            mse_score_per_sequence = []
            padded_building_mains = test_dataset.get_mains(padding=sequence_length,
                                                           appliance=params["appliances"][0],
                                                           building=building)

            std_batches = mains_to_batches(mains=padded_building_mains,
                                           n_seq_per_batch=64,
                                           seq_length=sequence_length,
                                           std=generic_metadata["input_stats"]["std"][0],
                                           r_std=generic_metadata["input_stats"]["std"][1],
                                           stride=params["test"]["stride"])

            non_std_batches = mains_to_batches(mains=padded_building_mains,
                                               n_seq_per_batch=64,
                                               seq_length=sequence_length,
                                               stride=params["test"]["stride"])

            for appliance in estimate_dict.keys():
                dummy_estimates = np.ones((overlapping_sequences + 1,
                                           (len(std_batches)) * params["test"]["stride"] * params["experiment"][
                                               "sequences_per_batch"] + sequence_length), dtype=np.float32)
                dummy_estimates *= float('nan')
                estimate_dict[appliance][building] = dummy_estimates

            overlapped_sequence_counter = 0
            logger.info("Disaggregating {} batches for building {}...".format(len(std_batches), building))

            for batch_i, std_batch in enumerate(std_batches):
                batch_start = batch_i * std_batch.shape[0] * params["test"]["stride"]
                non_std_batch = non_std_batches[batch_i]

                for sequence_i in range(std_batch.shape[0]):
                    net_target = []
                    target_main_sequence = non_std_batch[sequence_i:sequence_i + 1, :, 0]
                    target_main_sequence /= max_power_sum

                    for i in range(len(output_appliance_list)):
                        net_target.append(target_main_sequence)

                    net_target.append(target_main_sequence)

                    loss_before_fit = model.evaluate(std_batch[sequence_i:sequence_i + 1, :, :], net_target)[-1]

                    loss = loss_before_fit
                    best_loss = loss_before_fit
                    best_weights = original_weights

                    fitting_counter = 0
                    max_fitting_attempts = params["test"]["constrained_test_params"]["max_fitting_attempts"]
                    constraint_loss_threshold = params["test"]["constrained_test_params"]["loss_threshold"]

                    while loss > constraint_loss_threshold and fitting_counter < max_fitting_attempts:
                        model.train_on_batch(std_batch[sequence_i:sequence_i + 1, :, :], net_target)
                        loss = model.evaluate(std_batch[sequence_i:sequence_i + 1, :, :], net_target)[-1]
                        if loss < best_loss:
                            best_loss = loss
                            best_weights = model.get_weights()

                        fitting_counter += 1
                        print("Loss after fit: {}.".format(loss))

                    start_i = batch_start + (sequence_i * params["test"]["stride"])
                    end_i = start_i + std_batch[sequence_i].shape[0]

                    mse_score_per_sequence.append(best_loss)
                    model.set_weights(best_weights)
                    appliance_estimates = model.predict(std_batch[sequence_i:sequence_i + 1, :, :])

                    print("Exit fit, combining predictions and re-setting weights.")

                    for appliance_i, appliance in enumerate(output_appliance_list):
                        appliance_output = appliance_estimates[appliance_i]
                        estimate_dict[appliance][building][overlapped_sequence_counter, start_i:end_i] = \
                            appliance_output[0, :sequence_length]

                    model.set_weights(original_weights)

                    if overlapped_sequence_counter != overlapping_sequences:
                        overlapped_sequence_counter += 1
                    else:
                        overlapped_sequence_counter = 0

            estimates_sum = None

            for appliance in output_appliance_list:
                appliance_metadata = test_dataset.get_source_metadata(appliance)
                estimates = np.nanmedian(estimate_dict[appliance][building], 0)
                # estimates = estimates[:padded_mains.shape[-1]]  # Remove redundant samples from batch splitting
                estimates *= appliance_metadata["max_appliance_power"]
                estimates[estimates < 0] = 0
                estimates = estimates[sequence_length:-sequence_length]  # Remove padding
                estimates = np.round(estimates).astype(int)
                if estimates_sum is None:
                    estimates_sum = np.zeros(shape=estimates.shape, dtype=int)
                    estimates_sum = np.sum([estimates, estimates_sum], axis=0)
                else:
                    estimates_sum = np.sum([estimates, estimates_sum], axis=0)

                estimates_filename = os.path.join(params["paths"]["test_paths"][appliance],
                                                  '{}_estimates_building_{}.csv'.format(appliance, str(building)))
                np.savetxt(estimates_filename, estimates, delimiter=',', fmt='%.1d')

                mse_score_per_sequence = np.array(mse_score_per_sequence)
                mse_score_per_sequence_filename = os.path.join(params["paths"]["test_paths"][appliance],
                                                               "mse_per_sequence_building_{}.csv".format(str(building)))
                np.savetxt(mse_score_per_sequence_filename, mse_score_per_sequence, delimiter=',', fmt='%.8f')

                estimates_sum_filename = os.path.join(params["paths"]["test_paths"][appliance],
                                                      "estimates_sum_building_{}.csv".format(str(building)))
                np.savetxt(estimates_sum_filename, estimates_sum, delimiter=',', fmt='%.1d')

            gc.collect()

        logger.info('Done.')
    finally:
        test_dataset.close()
        for appliance in params["appliances"][1:]:
            new_filename = os.path.join(params["paths"]["test_paths"][appliance], logger_filename.split("/")[-1])
            if not os.path.isfile(new_filename):
                shutil.copy(logger_filename, new_filename)
