# A3NeuralNILM

**Public release 0.4.0**

Project derived from the [Neural NILM Prototype](https://github.com/JackKelly/neuralnilm_prototype).

# Requirements

Python version:

    >= 2.7, < 3.x

Modules:

*neuralnilm_prototype*

Minor dependencies, but it is required the version [v0.4.0-a3lab](https://gitlab.com/a3labShares/neuralnilm_prototype/tags/v0.4.0-a3lab). Once downloaded or cloned the repo, execute in the main folder:

    python setup.py develop

*nilmtk*

It is required the version [v0.2.2-a3lab](https://gitlab.com/a3labShares/nilmtk/tags/v0.2.2-a3lab).Once downloaded or cloned the repo, execute in the main folder:

    python setup.py develop

*nilm_metadata*

It is required the version [v0.2.2-a3lab](https://gitlab.com/a3labShares/nilm_metadata/tags/v0.2.2-a3lab). Once downloaded or cloned the repo, execute in the main folder:

    python setup.py develop
    
*NetCreator (A3Lab utils)*

It is required the version [v0.1.0](https://gitlab.com/a3labShares/NetCreator/tags/v0.1.0). Once downloaded or cloned the repo, execute in the main folder:

    python setup.py develop

Other modules via *pip*:

    pandas==0.18.1
    numpy==1.11.0
    scikit-learn
    numexpr
    tables
    matplotlib
    networkx
    future
    h5py
    ipython
    theano==0.9
    keras

# Quick start

To run the code with a _default configuration_ just execute in _scripts_:

    python main.py
   
Note that the default _DB_ directory, with the available datasets in h5 format, should be available in the project parent directory.

# Publications

If you use A3NeuralNILM in academic work then please consider citing our papers:

- M. Valenti, R. Bonfigli, E. Principi, and S. Squartini, _“Exploiting the reactive power in deep neural models for non-intrusive load monitoring,”_ in 2018 International Joint Conference on Neural Networks (IJCNN), to appear.
- R. Bonfigli, A. Felicetti, E. Principi, M. Fagiani, S. Squartini, and F. Piazza, [_“Denoising autoencoders for non-intrusive load monitoring: Improvements and comparative evaluation,”_](https://doi.org/10.1016/j.enbuild.2017.11.054) Energy and Buildings, vol. 158, pp. 1461 – 1474, 2018.

#

# Quick start

To run the code with a _default configuration_ just execute:

    python main.py
   
Note that the default _DB_ directory, with the available datasets in h5 format, should be available in the project parent directory.



# Credits

Here is reported the original README to keep the module origins.

    # Neural NILM Prototype

    Early prototype for the Neural NILM (non-intrusive load monitoring)
    software.  This software will be completely re-written as the [Neural
    NILM project](https://github.com/JackKelly/neuralnilm).

    This is the software that was used to run the experiments for our
    [Neural NILM paper](http://arxiv.org/abs/1507.06594).

    Note that `Neural NILM Prototype` is completely unsupported because
    it is replaced by the
    [Neural NILM project](https://github.com/JackKelly/neuralnilm).  Furthermore,
    the code for `Neural NILM Prototype` is a bit of a mess!

    Directories:

    * `neuralnilm` contains re-usable library code
    * `scripts` contains runnable experiments
    * `notebooks` contains IPython Notebooks (mostly for testing stuff
      out)

    The script which specified the experiments I ran in my paper is
    [e567.py](https://github.com/JackKelly/neuralnilm_prototype/blob/master/scripts/e567.py).

    (It's a pretty horrible bit of code!  Written in a rush!)  In that
    script, you can see the `SEQ_LENGTH` for each appliance and the
    `N_SEQ_PER_BATCH` (the number of training examples per batch).
    Basically, the sequence length varied from 128 (for the kettle) up to
    1536 (for the dish washer).  And the number of sequences per batch was
    usually 64, although I had to reduce that to 16 for the RNN for the
    longer sequences.

    The nets took a long time to train (I don't remember exactly how long
    but it was of the order of about one day per net per appliance).  You
    can see exactly how long I trained each net in that `e567.py` script
    (look at the `def net_dict_<architecture>` functions and look for
    `epochs`.... that's the number of batches (not epochs!) given to the
    net during training).  It's 300,000 for the rectangles net, 100,000
    for the AE and 10,000 for the RNN (because the RNN was a *lot* slower
    to train... I chose these numbers because the nets appeared to stop
    learning after this number of training iterations).
