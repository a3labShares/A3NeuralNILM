import os
import sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
import yaml
from tables.file import _open_files
import numpy as np
from utils.dataset_utils import get_dataset, read_dataset
from neuralnilm.metrics import run_metrics
from default_configs import DB_DIR
from nilmtk import DataSet
import pandas as pd
from datetime import timedelta
from utils.source_metadata import get_dataset_metadata
from utils.interpolation import interp


_NUS_CACHE = dict()  # key: bulding_1, buildin_2


def subsampled_predictions_from_os(predictions, dataset_name, window, building, nus_enabled=False, nus_subsampling_ratio=10, nus_expansion_ratio=10,
                                   us_enabled=False, us_subsampling_ratio=10):
    sample_period = get_dataset_metadata(dataset_name, 'sample_period')

    temp_dataset_filename = os.path.join(DB_DIR, dataset_name + ".h5")

    dataset = DataSet(temp_dataset_filename)
    dataset.set_window(*window)
    elec = dataset.buildings[building].elec
    dataset_tz = dataset.metadata['timezone']

    timestamp_index = pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period), tz=dataset_tz, closed='left')
    series = pd.Series(predictions[:len(timestamp_index)], index=timestamp_index)

    if us_enabled:
        # series_res = series.resample("{:d}S".format(sample_period * us_subsampling_ratio))
        series = series.asfreq("{:d}S".format(sample_period * us_subsampling_ratio))

    if nus_enabled and 'building_{}'.format(building) not in _NUS_CACHE.keys():

        subsampled_labels = pd.Series(0, index=pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period), tz=dataset_tz, closed='left'))

        # set to 1 the samples at subsample ratio
        subsampled_ratio_indexes = pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period * nus_subsampling_ratio),
                                                 tz=dataset.metadata.get('timezone'), closed='left')
        subsampled_labels[subsampled_ratio_indexes] = 1

        # load ground truth for each appliance and specific building
        # for appl in APPLIANCES[dataset_name]:
        for appl in get_dataset_metadata(dataset_name, 'appliances_list'):

            # for REDD dataset missing "washer dryer" in building 2
            if dataset_name == 'redd' and building == 2 and appl == 'washer_dryer':
                continue

            csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.splitext(os.path.join(temp_dataset_filename))[0], building, appl.replace(' ', '_'))

            # avoid ambigous error for date near dst
            appliance_ground_truth = pd.Series.from_csv(csv_filename).tz_localize('UTC')
            w0 = pd.Timestamp(window[0]).tz_localize(tz=dataset_tz).tz_convert('UTC')
            w1 = pd.Timestamp(window[1]).tz_localize(tz=dataset_tz).tz_convert('UTC')
            appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index < w0], inplace=True)
            appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index > w1], inplace=True)
            appliance_ground_truth = appliance_ground_truth.tz_convert(tz=dataset_tz)

            for each in range(-nus_expansion_ratio // 2, nus_expansion_ratio // 2):
                # subsampled_labels[appliance_ground_truth.index + timedelta(seconds=each * sample_period)] = 1
                expansion_indices = appliance_ground_truth.index + timedelta(seconds=each * sample_period)
                # check if expansion indices in subsampled_labels range, remove outliers
                expansion_indices = expansion_indices[(expansion_indices >= subsampled_labels.index[0]) & (expansion_indices <= subsampled_labels.index[-1])]
                subsampled_labels[expansion_indices] = 1

        try:
            # labels to drop
            labels = subsampled_labels[subsampled_labels == 0].index
            labels = labels[(labels >= series.index[0]) & (labels <= series.index[-1])]
            # print("Activatoin length: {}".format(len(activation)))
            if labels.date.any():
                # do avoid strage errors, drop labels only if exist
                # print("Activation length after subsampling: {}".format(len(activation)))
                # series.drop(labels=labels, inplace=True)
                series = series.tz_convert('UTC').drop(labels=labels.tz_convert('UTC')).tz_convert(tz=dataset_tz)

            if not series.any():
                raise ValueError("Activation series subsampled to empty series.")

            _NUS_CACHE.update({'building_{}'.format(building): subsampled_labels})  # caching data

        except Exception as err:
            # raise Exception("NUS - unhandled error in activations subsampling:\n")
            print("NUS - unhandled error in activations subsampling:")
            print(err)

    elif nus_enabled and 'building_{}'.format(building) in _NUS_CACHE.keys():
        subsampled_labels = _NUS_CACHE.get('building_{}'.format(building))  # retrieve data from cache

        try:
            # labels to drop
            labels = subsampled_labels[subsampled_labels == 0].index
            labels = labels[(labels >= series.index[0]) & (labels <= series.index[-1])]
            # print("Activatoin length: {}".format(len(activation)))
            if labels.date.any():
                # do avoid strage errors, drop labels only if exist
                # print("Activation length after subsampling: {}".format(len(activation)))
                # series.drop(labels=labels, inplace=True)
                series = series.tz_convert('UTC').drop(labels=labels.tz_convert('UTC')).tz_convert(tz=dataset_tz)

            if not series.any():
                raise ValueError("Activation series subsampled to empty series.")

        except Exception as err:
            # raise Exception("NUS - unhandled error in activations subsampling:\n")
            print("NUS - unhandled error in activations subsampling:")
            print(err)

    return series.values


def subsampled_predictions_from_us_nus(predictions, dataset_name, window, building, nus_enabled=False, nus_subsampling_ratio=10, nus_expansion_ratio=10,
                                       us_enabled=False, us_subsampling_ratio=10, apply_uniform_subsampling=10):
    sample_period = get_dataset_metadata(dataset_name, 'sample_period')

    temp_dataset_filename = os.path.join(DB_DIR, dataset_name + ".h5")

    dataset = DataSet(temp_dataset_filename)
    dataset.set_window(*window)
    elec = dataset.buildings[building].elec
    dataset_tz = dataset.metadata['timezone']

    if us_enabled:
        timestamp_index = pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period * us_subsampling_ratio), tz=dataset_tz, closed='left')
        series = pd.Series(predictions[:len(timestamp_index)], index=timestamp_index)
        # series_res = series.resample("{:d}S".format(sample_period * us_subsampling_ratio))
        series = series.asfreq("{:d}S".format(sample_period * apply_uniform_subsampling))

    elif nus_enabled:

        subsampled_labels = pd.Series(0, index=pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period), tz=dataset_tz, closed='left'))

        # set to 1 the samples at subsample ratio
        subsampled_ratio_indexes = pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period * nus_subsampling_ratio),
                                                 tz=dataset.metadata.get('timezone'), closed='left')
        subsampled_labels[subsampled_ratio_indexes] = 1

        # load ground truth for each appliance and specific building
        # for appl in APPLIANCES[dataset_name]:
        for appl in get_dataset_metadata(dataset_name, 'appliances_list'):

            # for REDD dataset missing "washer dryer" in building 2
            if dataset_name == 'redd' and building == 2 and appl == 'washer_dryer':
                continue

            csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.splitext(os.path.join(temp_dataset_filename))[0], building, appl.replace(' ', '_'))

            # avoid ambigous error for date near dst
            appliance_ground_truth = pd.Series.from_csv(csv_filename).tz_localize('UTC')
            w0 = pd.Timestamp(window[0]).tz_localize(tz=dataset_tz).tz_convert('UTC')
            w1 = pd.Timestamp(window[1]).tz_localize(tz=dataset_tz).tz_convert('UTC')
            appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index < w0], inplace=True)
            appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index > w1], inplace=True)
            appliance_ground_truth = appliance_ground_truth.tz_convert(tz=dataset_tz)

            for each in range(-nus_expansion_ratio // 2, nus_expansion_ratio // 2):
                # subsampled_labels[appliance_ground_truth.index + timedelta(seconds=each * sample_period)] = 1
                expansion_indices = appliance_ground_truth.index + timedelta(seconds=each * sample_period)
                # check if expansion indices in subsampled_labels range, remove outliers
                expansion_indices = expansion_indices[(expansion_indices >= subsampled_labels.index[0]) & (expansion_indices <= subsampled_labels.index[-1])]
                subsampled_labels[expansion_indices] = 1

        labels = subsampled_labels[subsampled_labels == 1].index
        series = pd.Series(predictions[:len(labels)], index=labels)
        series = series.asfreq("{:d}S".format(sample_period * apply_uniform_subsampling))

    return series.values


def evaluate(params):

    try:
        for appliance in params["appliances"]:

            scores_files = list()

            test_dataset = get_dataset(params["experiment"]["dataset"])

            test_buildings = test_dataset.get_test_buildings(appliance=appliance, test_mode=params["test"]["test_mode"])

            for building in test_buildings:

                estimates_filename = os.path.join(params["paths"]["test_paths"][appliance],
                                                  '{}_estimates_building_{}.csv'.format(appliance, str(building)))

                if not os.path.isfile(estimates_filename):
                    print("File does not exist:", estimates_filename)
                    continue

                scores_filename = os.path.join(params["paths"]["test_paths"][appliance], '{}_scores_building_{}_upsampled.yaml'.format(appliance, building))

                scores_files.append(scores_filename)

                if os.path.isfile(scores_filename):
                    print("Score file exist...skip.")
                    continue

                predictions = np.loadtxt(estimates_filename, delimiter=',')

                ground_truth = read_dataset(dataset_name=params["experiment"]["dataset"],
                                            window=params["test"]["time_windows"][building],
                                            trace=[appliance],
                                            building=building,
                                            experiment_id=params["ids"]["hashed_id"],
                                            nus_enabled=params.get("nus_enabled"),
                                            nus_subsampling_ratio=params.get("nus_subsampling_ratio"),
                                            nus_expansion_ratio=params.get("nus_expansion_ratio"),
                                            us_enabled=params.get("us_enabled"),
                                            us_subsampling_ratio=params.get("us_subsampling_ratio"))

                ground_truth_original = read_dataset(dataset_name=params["experiment"]["dataset"],
                                                     window=params["test"]["time_windows"][building],
                                                     trace=[appliance],
                                                     building=building,
                                                     experiment_id=params["ids"]["hashed_id"],
                                                     nus_enabled=False,
                                                     nus_subsampling_ratio=params.get("nus_subsampling_ratio"),
                                                     nus_expansion_ratio=params.get("nus_expansion_ratio"),
                                                     us_enabled=False,
                                                     us_subsampling_ratio=params.get("us_subsampling_ratio"))

                mains = list()

                # up-sample using filter
                if params.get("us_enabled"):
                    r = params.get("us_subsampling_ratio")
                    predictions_interpolated = interp(predictions, r, alpha=0.5)
                    predictions_interpolated[predictions_interpolated < 0] = 0

                elif params.get("nus_enabled"):
                    # upsample only the prediction point at uniform-subsampling
                    temp_dataset_filename = os.path.join(DB_DIR, params["experiment"]["dataset"] + ".h5")
                    dataset = DataSet(temp_dataset_filename)
                    window = params["test"]["time_windows"][building]
                    dataset.set_window(*window)
                    dataset_tz = dataset.metadata['timezone']
                    sample_period = get_dataset_metadata(params["experiment"]["dataset"], 'sample_period')

                    uniform_subsampling_indexes = pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period * params.get("nus_subsampling_ratio")),
                                                                tz=dataset.metadata.get('timezone'), closed='left')

                    uniform_subsampling_indexes = uniform_subsampling_indexes.drop(labels=uniform_subsampling_indexes[uniform_subsampling_indexes < ground_truth.index[0]])
                    uniform_subsampling_indexes = uniform_subsampling_indexes.drop(labels=uniform_subsampling_indexes[uniform_subsampling_indexes > ground_truth.index[-1]])
                    # uniform_subsampling_indexes = pd.date_range(start=ground_truth.index[0], end=ground_truth.index[-1], freq='{}S'.format(sample_period * params.get("nus_subsampling_ratio")),
                    #                                             tz=dataset.metadata.get('timezone'))
                    ground_truth_indexes = ground_truth.copy()
                    ground_truth_indexes[:] = 0
                    ground_truth_indexes[uniform_subsampling_indexes] = 1

                    index_len = min(len(ground_truth_indexes), len(predictions))
                    ground_truth_indexes = ground_truth_indexes[:index_len]
                    predictions = predictions[:index_len]
                    predictions_us = predictions[ground_truth_indexes.values == 1]

                    r = params.get("nus_subsampling_ratio")
                    predictions_interpolated_us = interp(predictions_us, r, alpha=0.5)
                    predictions_interpolated_us[predictions_interpolated_us < 0] = 0

                    # create NUS expansions w/o US ref
                    subsampled_labels = pd.Series(0, index=pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period), tz=dataset_tz, closed='left'))

                    # load ground truth for each appliance and specific building
                    # for appl in APPLIANCES[dataset_name]:
                    for appl in get_dataset_metadata(params["experiment"]["dataset"], 'appliances_list'):

                        # for REDD dataset missing "washer dryer" in building 2
                        if params["experiment"]["dataset"] == 'redd' and building == 2 and appl == 'washer_dryer':
                            continue

                        if __debug__:
                            csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.splitext(os.path.join(temp_dataset_filename))[0], building, appl.replace(' ', '_'))
                        else:
                            tmp_csv_dir = os.path.dirname(os.path.dirname(temp_dataset_filename))
                            csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.join(tmp_csv_dir, params["experiment"]["dataset"]), building, appl.replace(' ', '_'))

                        # avoid ambigous error for date near dst
                        appliance_ground_truth = pd.Series.from_csv(csv_filename).tz_localize('UTC')
                        w0 = pd.Timestamp(window[0]).tz_localize(tz=dataset_tz).tz_convert('UTC')
                        w1 = pd.Timestamp(window[1]).tz_localize(tz=dataset_tz).tz_convert('UTC')
                        appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index < w0], inplace=True)
                        appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index > w1], inplace=True)
                        appliance_ground_truth = appliance_ground_truth.tz_convert(tz=dataset_tz)

                        for each in range(-params.get("nus_expansion_ratio") // 2, params.get("nus_expansion_ratio") // 2):
                            # subsampled_labels[appliance_ground_truth.index + timedelta(seconds=each * sample_period)] = 1
                            expansion_indices = appliance_ground_truth.index + timedelta(seconds=each * sample_period)
                            # check if expansion indices in subsampled_labels range, remove outliers
                            expansion_indices = expansion_indices[(expansion_indices >= subsampled_labels.index[0]) & (expansion_indices <= subsampled_labels.index[-1])]
                            subsampled_labels[expansion_indices] = 1

                    index_len = min(len(ground_truth), len(predictions))
                    # predictions_interpolated_us = pd.Series(predictions_interpolated_us, index=pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period),
                    #                                                                                          tz=dataset.metadata.get('timezone'), closed='left'))
                    # predictions_interpolated_us = pd.Series(predictions_interpolated_us, index=pd.date_range(start=ground_truth.index[0], periods=len(predictions_interpolated_us), freq='{}S'.format(sample_period),
                    #                                                                                          tz=dataset.metadata.get('timezone')))

                    us_index = pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period), tz=dataset.metadata.get('timezone'), closed='left')
                    us_index = us_index.drop(labels=us_index[us_index < ground_truth.index[0]])
                    us_index = us_index.drop(labels=us_index[us_index > ground_truth.index[-1]])

                    index_len_pred = min(len(predictions_interpolated_us), len(us_index))

                    predictions_interpolated_us = pd.Series(predictions_interpolated_us[:index_len_pred], index=us_index[:index_len_pred])

                    predictions_nus = pd.Series(predictions[:index_len], index=ground_truth.index[:index_len])
                    predictions_nus[subsampled_labels != 1] = 0  # keep values only for nus expansion windows
                    predictions_nus = predictions_nus.resample('{:d}S'.format(sample_period)).fillna(0)

                    predictions_interpolated = predictions_interpolated_us.add(predictions_nus, fill_value=0)  # fill last missing sample

                scores = run_metrics(y_true=ground_truth_original.values, y_pred=predictions_interpolated, mains=mains)

                print("Saving scores US/NUS...")

                with open(scores_filename, 'w') as fh:
                    yaml.dump(scores, stream=fh, default_flow_style=False)
                    fh.close()
                print("Scores saved.")
                print("Energy based precision: {}".format(scores['precision_score_(energy_based)']))
                print("Energy based recall: {}".format(scores['recall_score_(energy_based)']))
                print("Energy based f1: {}".format(scores['f1_score_(energy_based)']))

                # scores_files.append(scores_filename)

            if (len(test_buildings) > 1 and len(scores_files) > 1) or (params["test"]["test_mode"] == 'seen' and appliance == 'washer_dryer'):
                # avg scores

                # scores_files = os.listdir(os.path.join(params["paths"]["test_paths"][appliance]))
                precision = list()
                recall = list()

                for test_file in scores_files:

                    with open(test_file, 'r') as f:
                        scores = yaml.load(f)

                    precision.append(scores['precision_score_(energy_based)'])
                    recall.append(scores['recall_score_(energy_based)'])

                # average scores
                mean_precision = sum(precision) / len(precision)
                mean_recall = sum(recall) / len(recall)

                if mean_precision == 0 or mean_recall == 0:
                    mean_f1 = 0
                else:
                    mean_f1 = (2 * mean_precision * mean_recall) / (mean_recall + mean_precision)

                avg_scores = {
                    'precision_score_(energy_based)': mean_precision,
                    'recall_score_(energy_based)': mean_recall,
                    'f1_score_(energy_based)': mean_f1
                }

                avg_scores_filename = os.path.join(params["paths"]["test_paths"][appliance], '{}_scores_avg_upsampled.yaml'.format(appliance))

                with open(avg_scores_filename, 'w') as f:
                    yaml.dump(avg_scores, stream=f, default_flow_style=False)

    except Exception as err:
        print("Unhandled error:")
        print(err)
        sys.exit(-1)


if __name__ == '__main__':

    EXP_DIR = '/home/marco/Workspace/Univ/NILM/Test vari/REDD_experiments_results_upsample_subsampling/experiments_redd_denoised_sr20_er20/'

    strides = [1, 8, 16, 32]
    test_modes = ['seen', 'unseen']
    disag_types = ['median', 'mean']

    for stride in strides:
        for test_mode in test_modes:
            for disag_type in disag_types:
                for dir in os.listdir(EXP_DIR):
                    if os.path.isdir(os.path.join(EXP_DIR, dir)):
                        for appliance in os.listdir(os.path.join(EXP_DIR, dir)):
                            if os.path.isdir(os.path.join(EXP_DIR, dir, appliance)) and 'test' not in appliance.split('_'):

                                file_config = os.path.join(EXP_DIR, dir, appliance, 'config.yaml')
                                with open(file_config, 'r') as f:
                                    params = yaml.load(f)

                                params["phases"]["train"] = False
                                params["experiment"]["disag_type"] = disag_type  # 'median' or 'mean'

                                # fix paths
                                params['paths']['dataset_path'] = DB_DIR
                                params['paths']['appliance_paths'][appliance] = os.path.join(EXP_DIR, dir, appliance)
                                params['paths']['base_paths'] = EXP_DIR
                                params['paths']['run_paths'][appliance] = os.path.join(EXP_DIR, dir)

                                params["test"]["stride"] = stride
                                params["test"]["test_mode"] = test_mode

                                appliance_test_folder = os.path.join(EXP_DIR, dir, appliance + '_test_' + test_mode + '_' + disag_type + '_stride_' + str(stride))
                                params['paths']['test_paths'][appliance] = appliance_test_folder

                                params["ids"]["hashed_id"] = 'test_' + test_mode + '_' + disag_type + '_stride_' + str(stride)

                                params["experiment"]["source_type"] = "same_location_source"

                                # to repeat/overwrite evaluations
                                params["overwrite"] = 'True'

                                print(appliance_test_folder)

                                metadata = get_dataset_metadata(params['experiment']['dataset'], appliance)

                                # dir exist
                                if os.path.isdir(appliance_test_folder):

                                    print("\n\nEvaluating " + appliance + "\n\n")
                                    evaluate(params)
                                    # from main import test, evaluate
                                    # evaluate(params)

                                # free memory from h5 temp data
                                _open_files.close_all()

    print('DONE.')
