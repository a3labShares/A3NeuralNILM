import numpy as np
from scipy.signal import lfilter, firls, filtfilt
import matplotlib.pyplot as plt


def upsample(s, n, phase=0):
    """Increase sampling rate by integer factor n with included offset phase.
    """

    return np.roll(np.kron(s, np.r_[1, np.zeros(n-1)]), phase)


def interp(s, r, l=4, alpha=1.0):
    """Interpolation - increase sampling rate by integer factor r. Interpolation
    increases the original sampling rate for a sequence to a higher rate. interp
    performs lowpass interpolation by inserting zeros into the original sequence
    and then applying a special lowpass filter. l specifies the filter length
    and alpha the cut-off frequency. The length of the FIR lowpass interpolating
    filter is 2*l*r+1. The number of original sample values used for
    interpolation is 2*l. Ordinarily, l should be less than or equal to 10. The
    original signal is assumed to be band limited with normalized cutoff
    frequency 0=alpha=1, where 1 is half the original sampling frequency (the
    Nyquist frequency). The default value for l is 4 and the default value for
    alpha is 0.5.
    """
    # b_firwin = firwin(2*l*r+1, alpha/r) * r

    # bands = (0, 1.0 / r, 1.0 / r, 1)
    # desired = (r, r, 0, 0)
    # b_firls = firls(2*l*r+1, bands, desired)
    # MATLAB => [a, b] = interp(signal, 5, 4, 1)
    # MATLAB => [a, b] = interp(signal, 5, 4, 0.5) -> b = intfilt(5,4,0.5) -> firls(????)

    if alpha == 1:
        bands = (0, 1./r, 1./r, 1)
        desired = (r, r, 0, 0)
    else:
        bands = (0, (1 - alpha)/r, (1 + (1 - alpha))/r, 1)
        desired = (r, r, 0, 0)

    b_firls = firls(2 * l * r + 1, bands, desired)

    # print(np.round(b_firwin, 4))
    # print(np.round(b_firls, 4))
    # plt.plot(b_firls)
    # plt.show()

    a = 1
    # return lfilter(b_firls, a, upsample(s, r))
    return filtfilt(b_firls, a, upsample(s, r)) / r


if __name__ == '__main__':

    # interpolation([0], 6)
    # np.random.seed(42)
    r = 10
    l = 4

    Fs = 8000
    f = 10
    sample = 8000
    x = np.arange(sample)
    y = np.sin(2 * np.pi * f * x / Fs)
    # signal = np.random.random(200)
    signal = y.reshape(-1, r)[:, 0]
    # signal_interp_zi, signal_interp_nzi = interp(signal, r)
    signal_interp = interp(signal, r, alpha=0.5)

    fig, axs = plt.subplots(4)
    axs[0].plot(y)
    axs[1].plot(signal)
    # axs[2].plot(signal_interp_zi)
    # axs[3].plot(signal_interp_nzi)
    axs[2].plot(signal_interp)

    plt.show()

    # print(np.allclose(y, signal_interp_zi))
    # print(np.allclose(y, signal_interp_nzi))
    # print(np.allclose(signal_interp_zi, signal_interp_nzi))

    print(signal[0:20])
    # print(signal_interp_zi[0:10])
    # print(signal_interp_nzi[0:10])
    # print(signal_interp_nzi[0:10])
    print(np.round(signal_interp[0:20], 4))
    # print("MES error (zi): {:f}".format(np.square(y-signal_interp_zi).mean()))
    # print("MES error (nzi): {:f}".format(np.square(y - signal_interp_nzi).mean()))
    print("MES error: {:e}".format(np.square(y-signal_interp).mean()))
