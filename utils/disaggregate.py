from __future__ import division
import numpy as np

DISAG_CHECK = 100


def median_disag(mains, net, sequences_per_batch, std, r_std, max_target_power, stride=1):
    n_seq_per_batch = sequences_per_batch
    seq_length = net.model.input_shape[1]
    if stride is None:
        stride = seq_length

    batches = mains_to_batches(mains=mains, n_seq_per_batch=n_seq_per_batch, seq_length=seq_length, std=std,
                               r_std=r_std, stride=stride)

    n = seq_length
    q = n // stride
    Q = q + 1
    # r = n - q * stride
    # r = n % stride
    # s = (n - Q * r) / q

    cross_estimates = np.ones((Q, (len(batches)) * stride * n_seq_per_batch + n), dtype=np.float32)
    cross_estimates *= float('nan')

    # Iterate over each batch
    i = 0
    for batch_i, net_input in enumerate(batches):

        if (batch_i + 1) % DISAG_CHECK == 0:
            print(str(batch_i + 1) + '/' + str(len(batches)) + ' batches disaggregated.')

        net_output = net.predict(net_input)

        batch_start = batch_i * n_seq_per_batch * stride

        for seq_i in range(n_seq_per_batch):
            start_i = batch_start + (seq_i * stride)
            end_i = start_i + seq_length

            # The net output is not necessarily the same length
            # as the mains (because mains might not fit exactly into
            # the number of batches required)

            cross_estimates[i, start_i:end_i] = net_output[seq_i, :n, 0]

            if i != q:
                i += 1
            else:
                i = 0

    else:
        if len(batches) % DISAG_CHECK:
            print(str(len(batches)) + '/' + str(len(batches)) + ' batches disaggregated.')

    estimates = np.nanmedian(cross_estimates, 0)

    estimates *= max_target_power
    estimates[estimates < 0] = 0

    estimates = estimates[seq_length:-seq_length]  # remove padding
    estimates = np.round(estimates).astype(int)

    return estimates


def mains_to_batches(mains, n_seq_per_batch, seq_length, std=None, r_std=None, stride=1):
    n_mains_samples = mains.shape[1]
    input_shape = (n_seq_per_batch, seq_length, mains.shape[0])

    n_batches = (n_mains_samples / stride) / n_seq_per_batch
    n_batches = int(np.ceil(n_batches))

    # fix mains length
    # mains = np.pad(mains, ((0, 0), (0, (n_batches * stride * n_seq_per_batch) - mains.shape[1])), 'constant')
    # _batches = np.lib.stride_tricks.as_strided(mains, shape=(1, mains.shape[1]-stride+1, mains.stride[0]))

    batches = []
    for batch_i in xrange(int(n_batches)):
        batch = np.zeros(input_shape, dtype=np.float32)
        batch_start = batch_i * n_seq_per_batch * stride
        for seq_i in xrange(n_seq_per_batch):
            mains_start_i = batch_start + (seq_i * stride)
            mains_end_i = mains_start_i + seq_length
            seq = mains[:, mains_start_i:mains_end_i]

            if seq.shape[1] != seq_length:
                # slide window outside right border, no more data
                break

            if std is not None:
                seq_standardised = standardize(seq[0, :], how='std=1', std=std)
                if mains.shape[0] == 2 and r_std is not None:
                    r_seq_standardised = standardize(seq[1, :], how='std=1', std=r_std)
                    seq_standardised = np.array([seq_standardised, r_seq_standardised])
                else:
                    seq_standardised = np.array([seq_standardised])

                batch[seq_i, :seq.shape[1], :] = seq_standardised.T
            else:
                batch[seq_i, :seq.shape[1], :] = seq.T

        batches.append(batch)

    return batches


def standardize(X, how='range=2', mean=None, std=None, midrange=None, ptp=None):
    if how == 'std=1':
        if mean is None:
            mean = X.mean()
        if std is None:
            std = X.std()
        centered = X - mean
        if std == 0:
            return centered
        else:
            return centered / std
    elif how == 'range=2':
        if midrange is None:
            midrange = (X.max() + X.min()) / 2
        if ptp is None:
            ptp = X.ptp()
        return (X - midrange) / (ptp / 2)
    else:
        raise RuntimeError("unrecognised how '" + how + "'")


def mean_disag(mains, net, sequences_per_batch, std, r_std, max_target_power, stride=1):

    n_seq_per_batch = sequences_per_batch
    seq_length = net.model.input_shape[1]
    if stride is None:
        stride = seq_length

    batches = mains_to_batches(mains=mains, n_seq_per_batch=n_seq_per_batch, seq_length=seq_length, std=std, r_std=r_std, stride=stride)

    n = seq_length
    q = n // stride
    Q = q + 1
    # r = n - q * stride
    # s = (n - Q * r) / q

    cross_estimates = np.ones((Q, (len(batches)) * stride * n_seq_per_batch + n), dtype=np.float32)
    cross_estimates *= float('nan')

    # Iterate over each batch
    i = 0
    for batch_i, net_input in enumerate(batches):

        if (batch_i + 1) % DISAG_CHECK == 0:
            print(str(batch_i + 1) + '/' + str(len(batches)) + ' batches disaggregated.')

        net_output = net.predict(net_input)
        # net_output = net.y_pred(net_input)
        batch_start = batch_i * n_seq_per_batch * stride
        #        i=0
        for seq_i in range(n_seq_per_batch):
            start_i = batch_start + (seq_i * stride)
            end_i = start_i + seq_length

            # n = len(estimates[start_i:end_i])
            # The net output is not necessarily the same length
            # as the mains (because mains might not fit exactly into
            # the number of batches required)
            # estimates[start_i:end_i] += net_output[seq_i, :n, 0]

            cross_estimates[i, start_i:end_i] = net_output[seq_i, :n, 0]

            if i != q:
                i += 1
            else:
                i = 0


    else:
        if len(batches) % DISAG_CHECK:
            print(str(len(batches)) + '/' + str(len(batches)) + ' batches disaggregated.')

    estimates = np.nanmean(cross_estimates, 0)

    # n_overlaps = seq_length / stride
    # estimates /= n_overlaps
    estimates *= max_target_power
    estimates[estimates < 0] = 0

    # add as median
    estimates = estimates[seq_length:-seq_length]  # remove padding
    estimates = np.round(estimates).astype(int)

    return estimates
