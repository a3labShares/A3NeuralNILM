import os
from keras import backend as K
import GPUtil

if K.backend() == "tensorflow":

    import tensorflow as tf

    # Set GPU order ato respect PCI BUS
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"

    try:

        DEVICE_ID = GPUtil.getAvailable(order='memory', limit=2, maxLoad=1, maxMemory=1)[0]
        os.environ["CUDA_VISIBLE_DEVICES"] = str(DEVICE_ID)

    except Exception as err:

        print(err)
        # system dependent, user choice
        print("Setting default CUDA_VISIBLE_DEVICES")
        # cineca: do not set CUDA_VISIBLE_DEVICES
        # local machine: set to avoid unwanted use of GPUS -> os.environ["CUDA_VISIBLE_DEVICES"] = "-1"

    finally:

        print('Set TF session and random seed.')

        tf.set_random_seed(1234)
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        sess = tf.Session(config=config)
        K.set_session(sess)
