import numpy as np
import matplotlib.pyplot as plt
import math
import os
from default_configs import PROJECT_DIR, TIME_WINDOWS
from utils.dataset_utils import read_dataset

project_dir = PROJECT_DIR

start_ends = [(0, -1), (36700, 36900), (35000, 42000)]

appliances = ['washing_machine']

unconstrained_test = {'heat_pump': 'config_00001_exp_00001/test_00001/',
                      'fridge': 'config_00002_exp_00001/test_00001/',
                      'washing_machine': 'config_00071_exp_00006/test_00014',
                      'electric_oven': 'config_00004_exp_00001/test_00001/',
                      'dish_washer': 'config_00031_exp_00006/test_00002/',
                      'microwave': 'config_00034_exp_00006/test_00001/'}

constrained_test = {'heat_pump': 'config_00001_exp_00001/test_00002',
                    'fridge': 'config_00002_exp_00001/test_00002',
                    'washing_machine': 'config_00071_exp_00006/test_00014',
                    'electric_oven': 'config_00004_exp_00001/test_00003',
                    'dish_washer': 'config_00031_exp_00006/test_00002/',
                    'microwave': 'config_00034_exp_00006/test_00001/'}

mains_mode = 'noised'
dataset = 'ukdale'
building = 2
time_window = TIME_WINDOWS[dataset]["test"][building]

output_dir = os.path.join(project_dir, 'plots')

paths = {}
datas = {}

if mains_mode == 'noised':
    datas['mains_ground_truth'] = read_dataset(
        dataset, 'aggregate', building, 42, time_window)

for appliance in appliances:

    datas['{}_ground_truth'.format(appliance)] = read_dataset(
        dataset, appliance, building, 42, time_window)

    if mains_mode == 'denoised':
        if 'mains_ground_truth' not in datas:
            datas['mains_ground_truth'] = np.zeros(datas['{}_ground_truth'.format(appliance)].shape)

        datas['mains_ground_truth'] += datas['{}_ground_truth'.format(appliance)]

    paths['constrained_{}_estimates'.format(appliance)] = os.path.join(
        PROJECT_DIR, 'experiments',
        constrained_test[appliance],
        '{}_estimates_building_{}.csv'.format(appliance, building))

    paths['unconstrained_{}_estimates'.format(appliance)] = os.path.join(
        PROJECT_DIR, 'experiments',
        unconstrained_test[appliance],
        '{}_estimates_building_{}.csv'.format(appliance, building))

for key in paths.keys():
    data = np.loadtxt(paths[key], delimiter=',')

    datas[key] = data

min_length = 300000000

for key in datas.keys():
    if datas[key].shape[0] < min_length:
        min_length = datas[key].shape[0]

for key in datas.keys():
    datas[key] = datas[key][:min_length]

datas['constrained_estimates_sum'] = np.zeros((min_length, ))
datas['unconstrained_estimates_sum'] = np.zeros((min_length, ))

for appliance in appliances:
    datas['constrained_estimates_sum'] += datas['constrained_{}_estimates'.format(appliance)]
    datas['unconstrained_estimates_sum'] += datas['unconstrained_{}_estimates'.format(appliance)]

constrained_rmse = math.sqrt(((datas['mains_ground_truth'] - datas['constrained_estimates_sum']) ** 2).mean(axis=0))
unconstrained_rmse = math.sqrt(((datas['mains_ground_truth'] - datas['unconstrained_estimates_sum']) ** 2).mean(axis=0))

print('Overall unconstrained RMSE: {}.'.format(unconstrained_rmse))
print('Overall constrained RMSE: {}.'.format(constrained_rmse))

for j, start_end in enumerate(start_ends):
    n_of_figures = len(appliances) + 1
    for appliance_i, appliance in enumerate(appliances):
        plt.figure(1, figsize=(12, 5 * n_of_figures), dpi=300)

        plt.subplot(n_of_figures, 1, appliance_i + 1)
        plt.plot(datas['{}_ground_truth'.format(appliance)][start_end[0]:start_end[1]])
        plt.plot(datas['unconstrained_{}_estimates'.format(appliance)][start_end[0]:start_end[1]])
        plt.plot(datas['constrained_{}_estimates'.format(appliance)][start_end[0]:start_end[1]])
        plt.legend(['{}_ground_truth'.format(appliance),
                    'unconstrained_{}_estimates'.format(appliance),
                    'constrained_{}_estimates'.format(appliance)],
                   loc='upper right')

    plt.subplot(n_of_figures, 1, n_of_figures)

    local_ground_truth = datas['mains_ground_truth'][start_end[0]:start_end[1]]
    local_unconstrained_estimates_sum = datas['unconstrained_estimates_sum'][start_end[0]:start_end[1]]
    local_constrained_estimates_sum = datas['constrained_estimates_sum'][start_end[0]:start_end[1]]

    plt.plot(local_ground_truth)
    plt.plot(local_unconstrained_estimates_sum)
    plt.plot(local_constrained_estimates_sum)
    plt.legend(['mains_ground_truth', 'unconstrained_estimates_sum', 'constrained_estimates_sum'], loc='upper right')

    local_unconstrained_rmse = math.sqrt(((local_ground_truth - local_unconstrained_estimates_sum) ** 2).mean(axis=0))
    local_constrained_rmse = math.sqrt(((local_ground_truth - local_constrained_estimates_sum) ** 2).mean(axis=0))
    print('Local unconstrained RMSE between {} and {}: {}.'.format(start_end[0], start_end[1], local_unconstrained_rmse))
    print('Local constrained RMSE between {} and {}: {}.'.format(start_end[0], start_end[1], local_constrained_rmse))

    figure_name = os.path.join(
        output_dir,
        'comparison_{}_building_{}_start_{}_end_{}.png'.format(dataset, building, start_end[0], start_end[1]))

    plt.savefig(figure_name)

    plt.close()
