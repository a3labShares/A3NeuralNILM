import argparse
import copy
import sys
import os
import shutil
import time
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
from default_configs import *
from utils.parameter_utils import params_setup, str2bool
from utils.source_metadata import get_dataset_metadata


def get_configs():
    params = copy.deepcopy(DEFAULT_PARAMS)

    params['skip_trained_config'] = True

    params['nus_enabled'] = True
    params['nus_subsampling_ratio'] = 20
    params['nus_expansion_ratio'] = 20

    params['us_enabled'] = False
    params['us_subsampling_ratio'] = 20

    params['phases']['train'] = False
    params['phases']['test'] = False
    params['phases']['evaluate'] = False

    params['overwrite'] = True

    params['experiment']['background_appliance_list'] = 'all'

    params['experiment']['dataset'] = 'ukdale'  # ukdale
    params['experiment']['main_mode'] = 'denoised'  # noised
    params['experiment']['reactive_power'] = False

    params['experiment']['source_type'] = 'same_location_source'  # for no data augmentation
    # params['experiment']['source_type'] = 'multisource'  # for data augmentation

    params['test']['test_mode'] = 'seen'

    params['experiment']['time_windows'] = get_dataset_metadata(params['experiment']['dataset'], 'time_windows')['train']
    params['test']['time_windows'] = get_dataset_metadata(params['experiment']['dataset'], 'time_windows')['test']

    params['experiment']['epochs'] = 200000
    params['experiment']['epoch_check'] = 10
    params['experiment']['early_stopping'] = 2000

    params['experiment']['disag_type'] = 'median'

    if params['nus_enabled'] and params['us_enabled']:
        print("Both NUS and US set to 'True', only one can be performed.")

    # set exp path
    PROJECT_DIR = '/home/marco/Workspace/Univ/NILM/Test vari/experiments_resutls_with_interpolation/'
    if params['nus_enabled']:

        params['paths']['base_path'] = os.path.join(PROJECT_DIR, "experiments_" + params['experiment']['dataset'] +
                                                    "_" + params['experiment']['main_mode'] + "_sr" + str(params['nus_subsampling_ratio']) +
                                                    "_er" + str(params['nus_expansion_ratio']))
    elif params['us_enabled']:
        params['paths']['base_path'] = os.path.join(PROJECT_DIR, "experiments_" + params['experiment']['dataset'] +
                                                    "_" + params['experiment']['main_mode'] + "_sr" + str(params['us_subsampling_ratio']))
    else:
        params['paths']['base_path'] = os.path.join(PROJECT_DIR, "experiments_" + params['experiment']['dataset'] +
                                                    "_" + params['experiment']['main_mode'])

    # load dataset to get metadata
    # tmp_dataset = get_dataset(params['experiment']['dataset'])

    appliances = get_dataset_metadata(params['experiment']['dataset'], 'appliances_list')

    temp_params_list = []
    conv_dimensions = [8, 16, 32, 128]  #[32, 128]  #
    conv_windows = [4, 16, 32]
    pooling_windows = [1, 2, 4] # [2, 4]  #
    dense_dimensions = [128, 512, 4096] # [512, 4096]  #

    for a in conv_dimensions:
        for b in conv_windows:
            for c in pooling_windows:
                for d in dense_dimensions:

                    for appliance in appliances:
                        # check feasibility
                        if params['us_enabled']:
                            seq_length = get_dataset_metadata(params['experiment']['dataset'], appliance)['seq_length'] // params['us_subsampling_ratio']
                        else:
                            seq_length = get_dataset_metadata(params['experiment']['dataset'], appliance)['seq_length']

                        for cnn_layers in range(1, 3):  # test with 1 cnn, and 2 cnn layers

                            temp_params = copy.deepcopy(params)
                            temp_params['appliances'] = [appliance]

                            if cnn_layers == 1:
                                if ((seq_length - (b-1)) / c) < 0:
                                    continue

                                temp_params['configurations'] = {
                                    appliance: {
                                        'conv_dimensions': [a],
                                        'enc_activation': 'linear',
                                        'dec_activation': 'relu',
                                        'conv_windows': [b],
                                        'conv_strides': [1],
                                        'conv_dropouts': 0.,
                                        'pooling_windows': [c],
                                        'pooling_strides': [c],
                                        'pooling_type': 'max',
                                        'dense_dimensions': [d],
                                        'dense_activation': 'relu',
                                        'dense_dropouts': 0.,
                                        'batch_normalization': False,
                                        'output_activation': 'relu'
                                    }
                                }

                            elif cnn_layers == 2:  # test with 2 cnn layers
                                if (((seq_length - (b-1)) / c) - (b-1)) / c < 0:
                                    continue

                                temp_params['configurations'] = {
                                    appliance: {
                                        'conv_dimensions': [a, 2*a],
                                        'enc_activation': 'linear',
                                        'dec_activation': 'relu',
                                        'conv_windows': [b, b],
                                        'conv_strides': [1, 1],
                                        'conv_dropouts': 0.,
                                        'pooling_windows': [c, c],
                                        'pooling_strides': [c, c],
                                        'pooling_type': 'max',
                                        'dense_dimensions': [d],
                                        'dense_activation': 'relu',
                                        'dense_dropouts': 0.,
                                        'batch_normalization': False,
                                        'output_activation': 'relu'
                                    }
                                }

                            temp_params_list.append(temp_params)

    return temp_params_list


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='NeuralNILM Launcher')
    parser.add_argument('--overwrite', default=False, type=str2bool)
    parser.add_argument('--machine', default='local', type=str)  # options: local, pbs, slurm
    parser.add_argument('--device', default='gpu', type=str)

    unsorted_params = vars(parser.parse_args())
    overwrite = unsorted_params['overwrite']
    machine = unsorted_params['machine']
    device = unsorted_params['device']

    try:

        # temp_config_dir = os.path.join(PROJECT_DIR, 'temp_configs')
        # if not os.path.isdir(temp_config_dir):
        #     os.mkdir(temp_config_dir)

        params_list = get_configs()

        for params_config in params_list:
            temp = copy.deepcopy(params_config)

            temp, new_run, new_test = params_setup(params=temp)

            if overwrite or new_run or new_test:
                os.system("echo New path created or overwrite set to 'True', submitting experiment.")

                appl = ''
                if temp['appliances'] != 'all':
                    appl = temp['appliances'][0]
                else:
                    raise ValueError("Option 'all' not supported in filed 'appliances'!")

                parameter_filename = os.path.join(temp['paths']['run_paths'][appl], appl, 'config.yaml')

                if machine == 'local':

                    # os.system('python ../main.py -O --parameter_file {}'.format(parameter_filename))
                    print(parameter_filename)

                else:  # jobs schedulers: pbs or slurm

                    template_submit_filename = ''

                    if machine == 'pbs':
                        template_submit_filename = os.path.join(PROJECT_DIR, 'submits', 'template_submit_{}.pbs'.format(device))
                    elif machine == 'slurm':
                        template_submit_filename = os.path.join(PROJECT_DIR, 'submits', 'template_submit_{}.slr'.format(device))

                    if not os.path.isfile(template_submit_filename):
                        raise ValueError("Submit file 'template_submit_{}.pbs/.slr' not found in path: {}".format(device, template_submit_filename))

                    os.system("echo New path created or overwrite set to 'True', submitting experiment.")

                    output_submit_filename = ''
                    if machine == 'pbs':
                        output_submit_filename = os.path.join(PROJECT_DIR, 'submits', 'submit-{}-{}.pbs'.format(appl, temp['ids']['run_ids'][appl]))
                        shutil.copy(template_submit_filename, output_submit_filename)
                    elif machine == 'slurm':
                        # slurm does not use the "output_submit_filename" as base name for the output/error log
                        #output_submit_filename = os.path.join(PROJECT_DIR, 'submits', 'submit-{}-{}.slr'.format(appl, temp['ids']['run_ids'][appl]))
                        output_submit_filename = template_submit_filename

                    if machine == 'pbs':
                        os.system('qsub -v parameter_file={} {}'.format(parameter_filename, output_submit_filename))
                        time.sleep(2)
                    elif machine == 'slurm':
                        os.system('sbatch {} {}'.format(output_submit_filename, parameter_filename))

            else:
                os.system("echo No new experiment detected and overwrite set to 'False', not submitting.")

    finally:
        pass
        # sess.close()
