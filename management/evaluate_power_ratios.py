import os
import shutil
import numpy as np
import matplotlib.pyplot as plt
import csv

from nilmtk import DataSet

from neuralnilm.source import _preprocess_activations

from utils.dataset_utils import AmpdDataSetReader
from utils.io_utils import save_json
from default_configs import DB_DIR, APPLIANCES, PROJECT_DIR

# Active ground truth / active aggregate = agt_aa
# Reactive ground truth / Active ground truth = rgt_agt
# Reactive ground truth / reactive aggregate = rgt_ra

appliances = APPLIANCES["ampds"]
mains_mode = 'noised'
building = 1
time_window = ('2012-10-01', '2014-04-01')
output_dir = os.path.join(PROJECT_DIR, "power_evaluation")
if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

do_plots = False
plots_output_dir = os.path.join(output_dir, "plots")
if do_plots:
    if not os.path.isdir(plots_output_dir):
        os.mkdir(plots_output_dir)

ratios_dict = {}

dataset_reader = AmpdDataSetReader()

temp_dataset_filename = os.path.join(DB_DIR, "ampds_4242_evaluation.h5")

shutil.copy(os.path.join(DB_DIR, "ampds.h5"), temp_dataset_filename)

nilmtk_dataset = DataSet(temp_dataset_filename)
nilmtk_dataset.set_window(*time_window)
elec = nilmtk_dataset.buildings[building].elec

mains_meter = elec.mains()

active_mains = mains_meter.power_series_all_data(
    sample_period=60,
    physical_quantity='power',
    ac_type='active')

reactive_mains = mains_meter.power_series_all_data(
    sample_period=60,
    physical_quantity='power',
    ac_type='reactive')

for appliance in appliances:
    # Active ground truth / active aggregate = agt_aa
    # Reactive ground truth / Active ground truth = rgt_agt
    # Reactive ground truth / reactive aggregate = rgt_ra

    ratios_dict[appliance] = {
        "agt_aa": 0.,
        "rgt_agt": 0.,
        "rgt_ra": 0.
    }

    source_metadata = dataset_reader.get_source_metadata(appliance)

    appliance_meter = elec[appliance.replace("_", " ")]  # e.g. 'dish_washer' must be 'dish washer'

    activation_series_list = appliance_meter.activation_series(
        on_power_threshold=source_metadata["on_power_threshold"],
        min_on_duration=source_metadata["min_on_duration"],
        min_off_duration=source_metadata["min_off_duration"],
        resample=True,
        physical_quantity='power',
        ac_type='active')

    activation_series_list = _preprocess_activations(
        activation_series_list,
        max_power=source_metadata["max_appliance_power"],
        sample_period=60,
        clip_appliance_power=False)

    reactive_appliance_trace = appliance_meter.power_series_all_data(
        sample_period=60,
        physical_quantity='power',
        ac_type='reactive')

    for activation_i, activation in enumerate(activation_series_list):

        activation_active_mains = active_mains[activation.index]
        activation_reactive_mains = reactive_mains[activation.index]

        reactive_ground_truth = reactive_appliance_trace[activation.index].values
        active_ground_truth = activation.values

        active_aggregate = activation_active_mains.values
        reactive_aggregate = activation_reactive_mains.values

        sum_rgt = np.sum(reactive_ground_truth)
        sum_agt = np.sum(active_ground_truth)
        sum_aa = np.sum(active_aggregate)
        sum_ra = np.sum(reactive_aggregate)

        ratios_dict[appliance]["agt_aa"] += float(sum_agt / sum_aa)
        ratios_dict[appliance]["rgt_agt"] += float(sum_rgt / sum_agt)
        ratios_dict[appliance]["rgt_ra"] += float(sum_rgt / sum_ra)

        if do_plots:
            plt.figure(1, figsize=(12, 5), dpi=300)
            plt.plot(reactive_ground_truth)
            plt.plot(active_ground_truth)
            plt.plot(reactive_aggregate)
            plt.plot(active_aggregate)
            plt.legend(
                ["Reactive ground truth",
                 "Active ground truth",
                 "Reactive aggregate",
                 "Active aggregate"],
                loc='upper right')

            plt.savefig(os.path.join(plots_output_dir, "{}_ampds_activation_{}.png".format(appliance, activation_i+1)))

            plt.close()

    ratios_dict[appliance]["agt_aa"] /= len(activation_series_list)
    ratios_dict[appliance]["rgt_agt"] /= len(activation_series_list)
    ratios_dict[appliance]["rgt_ra"] /= len(activation_series_list)

keys = ("Ratio", "rgt_agt", "agt_aa", "rgt_ra")
columns = [keys]
for appliance in ratios_dict.keys():
    new_column = (appliance, )
    for key in keys[1:]:
        new_column += (ratios_dict[appliance][key], )
    columns.append(new_column)

rows = zip(*columns)
f = open(os.path.join(output_dir, "ratios.csv"), 'wt')
try:
    writer = csv.writer(f)
    for row in rows:
        writer.writerow(row)
finally:
    f.close()

save_json(output_dir, "ratios.json", ratios_dict)
