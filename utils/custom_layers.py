from keras.layers.merge import _Merge
from keras import backend as K
from keras.backend import zeros
from tensorflow import scatter_add


class AverageCombine(_Merge):
    def __init__(self, hop=1, **kwargs):
        super(AverageCombine, self).__init__(**kwargs)
        self.hop = hop
        self.supports_masking = False

    def _merge_function(self, inputs):
        if not isinstance(inputs, list):
            raise ValueError('A `Combine` layer should be called '
                             'on a list of inputs.')

        # Output dimension
        n_output_elements = inputs[0].shape[1] + (len(inputs) - 1) * self.hop
        n_input_elements = inputs[0].shape[1]

        output = zeros((inputs[0].shape[0], n_output_elements))
        weights = zeros((inputs[0].shape[0], n_output_elements))

        start_idx = 0
        for i in range(0, len(inputs)):
            # weights = inc_subtensor(weights[:, start_idx:start_idx + n_input_elements], 1)
            # output = inc_subtensor(output[:, start_idx:start_idx + n_input_elements], inputs[i])
            weights = scatter_add(weights[:, start_idx:start_idx + n_input_elements], 1)
            output = scatter_add(output[:, start_idx:start_idx + n_input_elements], inputs[i])
            start_idx += self.hop

        output /= weights

        return output

    def compute_output_shape(self, input_shape):
        if not isinstance(input_shape, list):
            raise ValueError('A `Combine` layer should be called '
                             'on a list of inputs.')

        input_shapes = input_shape
        output_shape = list(input_shapes[0])

        if output_shape[-1] is not None:
            output_shape[-1] = input_shapes[0][-1] + (len(input_shapes) - 1) * self.hop

        return tuple(output_shape)


class MedianCombine(_Merge):
    def __init__(self, hop=1, **kwargs):
        super(MedianCombine, self).__init__(**kwargs)
        self.hop = hop

    def _merge_function(self, inputs):
        if not isinstance(inputs, list):
            raise ValueError('A `Combine` layer should be called '
                             'on a list of inputs.')

        # Output dimension
        n_output_elements = inputs[0].shape[1] + (len(inputs) - 1) * self.hop

        output = zeros((inputs[0].shape[0], n_output_elements))

        start_idx = 0
        for i in range(0, len(output)):

            # weights = inc_subtensor(weights[:, start_idx:start_idx + n_input_elements], 1)
            # output = inc_subtensor(output[:, start_idx:start_idx + n_input_elements], inputs[i])
            weights = scatter_add(weights[:, start_idx:start_idx + n_input_elements], 1)
            output = scatter_add(output[:, start_idx:start_idx + n_input_elements], inputs[i])
            start_idx += self.hop

        output /= weights

        return output


class WeightScaleAndAdd(_Merge):
    def __init__(self, scale_factors, mean=0., std=1., maximum=0., **kwargs):
        self.scale_factors = []

        for scale_factor in scale_factors:
            if isinstance(scale_factor, int):
                self.scale_factors.append(K.cast_to_floatx(float(scale_factor)))
            elif isinstance(scale_factor, float):
                self.scale_factors.append(K.cast_to_floatx(scale_factor))
            else:
                raise ValueError('Scale factors must be float or int.')

        self.mean = K.cast_to_floatx(mean)
        self.std = K.cast_to_floatx(std)
        self.maximum = K.cast_to_floatx(maximum)
        self.supports_masking = False
        super(WeightScaleAndAdd, self).__init__(**kwargs)

    def _merge_function(self, inputs):

        if len(inputs) != len(self.scale_factors):
            raise ValueError('You must give as many scale factors as inputs.')

        output = inputs[0] * self.scale_factors[0]
        for i in range(1, len(inputs)):
            output += inputs[i] * self.scale_factors[i]

        output = (output - self.mean) / self.std

        if self.maximum != 0.:
            output /= (K.max(output) + self.maximum)

        return output


def combine(inputs, **kwargs):
    return AverageCombine(**kwargs)(inputs)
