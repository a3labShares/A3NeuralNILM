import os
import shutil
from default_configs import DB_DIR
from utils.source_metadata import get_dataset_metadata
import numpy as np
from utils.dataset_utils import get_dataset
from nilmtk import DataSet
import pandas as pd
from datetime import timedelta


def read_dataset(dataset_name, trace, building, experiment_id, window, power_type="active", nus_enabled=False, nus_subsampling_ratio=10, nus_expansion_ratio=10,
                 us_enabled=False, us_subsampling_ratio=10):

    series = pd.Series()
    sample_period = get_dataset_metadata(dataset_name, 'sample_period')
    temp_dataset_filename = os.path.join(DB_DIR, dataset_name + ".h5")

    dataset = DataSet(temp_dataset_filename)
    dataset.set_window(*window)
    elec = dataset.buildings[building].elec
    dataset_tz = dataset.metadata['timezone']

    if us_enabled:
        sample_period = sample_period * us_subsampling_ratio

    meter = elec.mains()
    if dataset_name == 'redd':
        series = meter.power_series_all_data(sample_period=sample_period)
        series -= series

    if trace == "aggregate":
        meter = elec.mains()
        if power_type == "active":
            series = meter.power_series_all_data(
                sample_period=sample_period, physical_quantity='power', ac_type='active')
            series = series.fillna(value=0.)  # To fill NaNs found after re-sampling.
            values = series.values

        if power_type == "reactive":  # TODO: check dropnan as for "active"
            if dataset_name == 'ukdale':
                active_series = meter.power_series_all_data(
                    sample_period=sample_period, physical_quantity='power', ac_type='active')
                apparent_series = meter.power_series_all_data(
                    sample_period=sample_period, physical_quantity='power', ac_type='apparent')

                active_series = active_series.fillna(value=0.)  # To fill NaNs found after re-sampling.
                apparent_series = apparent_series.fillna(value=0.)  # To fill NaNs found after re-sampling.

                quad_aprt = np.square(apparent_series.values)
                quad_acti = np.square(active_series.values)
                values = np.sqrt(quad_aprt - quad_acti)

            elif dataset_name == 'ampds':
                series = meter.power_series_all_data(
                    sample_period=sample_period, physical_quantity='power', ac_type='reactive')

                series = series.fillna(value=0.)  # To fill NaNs found after re-sampling.
                # values = series.values

            else:
                raise ValueError('Dataset {} not supported.'.format(dataset_name))

        else:
            raise ValueError('Mains type {} not supported.'.format(trace))
    elif isinstance(trace, list):
        for appliance in trace:
            meter = elec[appliance.replace("_", " ")]  # e.g. 'dish_washer' must be 'dish washer'
            if power_type == "active":
                _series = meter.power_series_all_data(sample_period=sample_period)
            else:
                if dataset_name == "ampds":
                    _series = meter.power_series_all_data(sample_period=sample_period, ac_type="reactive")
                elif dataset_name == "ukdale":
                    raise ValueError("No reactive power is provided as ground truth for UK-DALE.")
                else:
                    raise ValueError('Dataset {} not supported.'.format(dataset_name))

            # series += _series.fillna(value=0.)  # To fill NaNs found after re-sampling.
            if series.any():
                series += _series
            else:
                series = _series.copy()
            series = series.dropna()

    else:
        raise ValueError("Trace must be str or list of appliances.")

    original_main_length = len(series)

    if nus_enabled:  # and 'building_{}'.format(building) not in _NUS_CACHE.keys():

        subsampled_labels = pd.Series(0, index=pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period), tz=dataset_tz, closed='left'))

        # set to 1 the samples at subsample ratio
        subsampled_ratio_indexes = pd.date_range(start=window[0], end=window[1], freq='{}S'.format(sample_period * nus_subsampling_ratio),
                                                 tz=dataset.metadata.get('timezone'), closed='left')
        subsampled_labels[subsampled_ratio_indexes] = 1

        # load ground truth for each appliance and specific building
        # for appl in APPLIANCES[dataset_name]:
        for appl in get_dataset_metadata(dataset_name, 'appliances_list'):

            # for REDD dataset missing "washer dryer" in building 2
            if dataset_name == 'redd' and building == 2 and appl == 'washer_dryer':
                continue

            if __debug__:
                csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.splitext(os.path.join(temp_dataset_filename))[0], building, appl.replace(' ', '_'))
            else:
                tmp_csv_dir = os.path.dirname(os.path.dirname(temp_dataset_filename))
                csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.join(tmp_csv_dir, dataset_name), building, appl.replace(' ', '_'))

            # avoid ambigous error for date near dst
            appliance_ground_truth = pd.Series.from_csv(csv_filename).tz_localize('UTC')
            w0 = pd.Timestamp(window[0]).tz_localize(tz=dataset_tz).tz_convert('UTC')
            w1 = pd.Timestamp(window[1]).tz_localize(tz=dataset_tz).tz_convert('UTC')
            appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index < w0], inplace=True)
            appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index > w1], inplace=True)
            appliance_ground_truth = appliance_ground_truth.tz_convert(tz=dataset_tz)

            for each in range(-nus_expansion_ratio // 2, nus_expansion_ratio // 2):
                # subsampled_labels[appliance_ground_truth.index + timedelta(seconds=each * sample_period)] = 1
                expansion_indices = appliance_ground_truth.index + timedelta(seconds=each * sample_period)
                # check if expansion indices in subsampled_labels range, remove outliers
                expansion_indices = expansion_indices[(expansion_indices >= subsampled_labels.index[0]) & (expansion_indices <= subsampled_labels.index[-1])]
                subsampled_labels[expansion_indices] = 1

        try:
            # labels to drop
            labels = subsampled_labels[subsampled_labels == 0].index
            labels = labels[(labels >= series.index[0]) & (labels <= series.index[-1])]
            # print("Activatoin length: {}".format(len(activation)))
            if labels.date.any():
                # to avoid strage errors, drop labels only if exist
                # print("Activation length after subsampling: {}".format(len(activation)))
                # series.drop(labels=labels, inplace=True)
                series = series.tz_convert('UTC').drop(labels=labels.tz_convert('UTC')).tz_convert(tz=dataset_tz)

            if not series.any():
                raise ValueError("Activation series subsampled to empty series.")

        except Exception as err:
            # raise Exception("NUS - unhandled error in activations subsampling:\n")
            print("NUS - unhandled error in activations subsampling:")
            print(err)

        nus_main_length = len(series)
        print("Dataset stats for building {}".format(building))
        print("Original dataset length: {}".format(original_main_length))
        print("NUS dataset length: {}".format(nus_main_length))
        print("NUS overall reduction: {:.2f}".format(100 * float(nus_main_length)/original_main_length))


if __name__ == '__main__':

    dataset = "redd"
    appliances = ["dish_washer"]   # not import, just select one that use all buldings
    time_windows = {
        "train": {
            # 1: ('2013-04-12', '2014-10-21'),  # UKDALE
            # 2: ('2013-05-22', '2013-09-26'),
            # 3: ('2013-02-27', '2013-03-25'),
            # 4: ('2013-03-09', '2013-09-11'),
            # 5: ('2014-06-29', '2014-09-01')
            1: ("2011-04-18", "2011-05-20"),  # REDD
            2: ("2011-04-17", "2011-04-29"),
            3: ("2011-04-16", "2011-04-28")
        },
        "test": {
            # 1: ('2014-10-22', '2014-12-15'),  # UKDALE
            # 2: ('2013-09-27', '2013-10-10'),
            # 3: ('2013-03-25', '2013-04-01'),
            # 4: ('2013-09-11', '2013-10-01'),
            # 5: ('2014-09-01', '2014-09-07'),
            1: ("2011-05-21", "2011-05-24"),  # REDD
            2: ("2011-04-30", "2011-05-01"),
            3: ("2011-05-21", "2011-05-27")
        }
    }

    for appliance in appliances:

        test_dataset = get_dataset(dataset)
        test_buildings = test_dataset.get_test_buildings(appliance=appliance, test_mode="unseen")  # or "unseen"

        for building in test_buildings:

            for exp_mode in ['test', 'train']:

                print("MODE:", exp_mode)

                ground_truth = read_dataset(dataset_name=dataset,
                                            window=time_windows[exp_mode][building],
                                            trace=[appliance],
                                            building=building,
                                            experiment_id='how long is the dataset?',
                                            nus_enabled=True,
                                            nus_subsampling_ratio=20,
                                            nus_expansion_ratio=20,
                                            us_enabled=False,
                                            us_subsampling_ratio=10)
