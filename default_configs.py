import os

# Default paths

PROJECT_DIR = os.path.dirname((os.path.abspath(__file__)))
DB_DIR = os.path.join(os.path.dirname(PROJECT_DIR), "DB")

# APPLIANCES = {
#     "ukdale": ["fridge", "microwave", "kettle", "dish_washer", "washing_machine"],
#     "ampds": ["dish_washer", "electric_oven", "fridge", "heat_pump", "tumble_dryer", "washing_machine"],
#     "redd": ["fridge", "microwave", "dish_washer", "dryer"]
# }
#
# TIME_WINDOWS = {
#     "ukdale": {
#         "train": {
#             1: ('2013-04-12', '2014-10-21'),
#             2: ('2013-05-22', '2013-09-26'),
#             3: ('2013-02-27', '2013-03-25'),
#             4: ('2013-03-09', '2013-09-11'),
#             5: ('2014-06-29', '2014-09-01')
#         },
#         "test": {
#             1: ('2014-10-22', '2014-12-15'),
#             2: ('2013-09-27', '2013-10-10'),
#             3: ('2013-03-25', '2013-04-01'),
#             4: ('2013-09-11', '2013-10-01'),
#             5: ('2014-09-01', '2014-09-07')
#         }
#     },
#     "ampds": {
#         "train": {1: ('2012-10-01', '2014-04-01')},
#         "test": {1: ('2012-04-01', '2012-10-01')}
#     },
#     "redd": {
#         "train": {
#             1: ("2011-04-18", "2011-05-20"),
#             2: ("2011-04-17", "2011-04-29"),
#             3: ("2011-04-16", "2011-04-28")
#         },
#         "test": {
#             1: ("2011-05-21", "2011-05-24"),
#             2: ("2011-04-30", "2011-05-01"),
#             3: ("2011-05-21", "2011-05-27")
#         }
#     }
# }

# Default params

DEFAULT_PARAMS = {
    "nus_enabled": False,
    "nus_subsampling_ratio": 5,
    "nus_expansion_ratio": 5,
    "us_enabled": False,
    "us_subsampling_ratio": 5,
    "phases": {
        "train": True,
        "test": True,
        "evaluate": True,
    },
    "appliances": ["dish_washer"],
    "overwrite": True,
    "experiment": {
        "disag_type": "median",
        "time_windows": {
            1: ('2013-04-12', '2014-10-21'),  # ukdale
            2: ('2013-05-22', '2013-09-26'),
            3: ('2013-02-27', '2013-03-25'),
            4: ('2013-03-09', '2013-09-11'),
            5: ('2014-06-29', '2014-09-01')
            # 1: ('2012-10-01', '2014-04-01')  # ampds
            # 1: ("2011-04-18", "2011-05-20"),
            # 2: ("2011-04-17", "2011-04-29"),
            # 3: ("2011-04-16", "2011-04-28")  # redd
        },
        "dataset": "ukdale",
        "main_mode": "denoised",
        "reactive_power": False,
        "background_appliance_list": "all",
        "source_type": "same_location_source",
        # "source_type": "real_appliance_source",
        # "source_type": "multisource",
        "sequences_per_batch": 64,
        "valid_ratio": 0.2,
        # "epochs": 200000,
        "epochs": 100,
        "epoch_check": 10,
        "early_stopping": 2000,
        "learning_rate": 0.1,
        "auto_learning_rate_change": True,
        "max_no_improvement_delta": 0.01,
        "optimizer": "hardcoded_sgd",
        "loss": "mse",
        "architecture": "KerasConvAutoEncoder",
        "gpus": 2,
        "constrained_train": False,
        "constrained_train_params": {
            "optimizer": "sgd",
            "loss_threshold": 1,
            "appliance_loss_weight": 1.,
            "sum_loss_weight": 1.,
            "max_fitting_attempts": 1,
            "learning_rate": 0.01,
            "stride": 15
        }
    },
    "test": {
        "constrained_test": False,
        "constrained_test_params": {
            "freeze_layers": True,
            "relu_clipping_threshold": "none",
            "optimizer": "sgd",
            "appliance_loss_weight": 0.,
            "constraint_window": "max",
            "learning_rate": 0.1,
            "loss_threshold": 0.0001,
            "max_fitting_attempts": 5,
            "stride": 15,
            "sum_loss_weight": 0.01
        },
        "stride": 16,
        "test_mode": "seen",
        "time_windows": {
            1: ('2014-10-22', '2014-12-15'),  # ukdale
            2: ('2013-09-27', '2013-10-10'),
            3: ('2013-03-25', '2013-04-01'),
            4: ('2013-09-11', '2013-10-01'),
            5: ('2014-09-01', '2014-09-07')
            # 1: ('2012-04-01', '2012-10-01')  # ampds
            # 1: ("2011-05-21", "2011-05-24"),
            # 2: ("2011-04-30", "2011-05-01"),
            # 3: ("2011-05-21", "2011-05-27")  # redd
        },
    },
    "paths": {
        "base_path": os.path.join(PROJECT_DIR, "experiments"),
        "dataset_path": DB_DIR,
    },
    "configurations": {
        "kettle": {
            "conv_dimensions": [128, 256],
            "enc_activation": "linear",
            "dec_activation": "relu",
            "conv_windows": [4, 4],
            "conv_strides": [1, 1],
            "conv_dropouts": 0.,
            "pooling_windows": [1, 1],
            "pooling_strides": [1, 1],
            "pooling_type": "max",
            "dense_dimensions": [512],
            "dense_activation": "relu",
            "dense_dropouts": 0.,
            "batch_normalization": False,
            "output_activation": "relu",
        },
        "washing_machine": {
            "conv_dimensions": [128, 256],
            "enc_activation": "linear",
            "dec_activation": "relu",
            "conv_windows": [2, 2],
            "conv_strides": [1, 1],
            "conv_dropouts": 0.,
            "pooling_windows": [2, 2],
            "pooling_strides": [2, 2],
            "pooling_type": "max",
            "dense_dimensions": [512],
            "dense_activation": "relu",
            "dense_dropouts": 0.,
            "batch_normalization": False,
            "output_activation": "relu",
        },
        "dish_washer": {
            "conv_dimensions": [128],
            "enc_activation": "linear",
            "dec_activation": "relu",
            "conv_windows": [16],
            "conv_strides": [1],
            "conv_dropouts": 0.,
            "pooling_windows": [4],
            "pooling_strides": [4],
            "pooling_type": "max",
            "dense_dimensions": [512],
            "dense_activation": "relu",
            "dense_dropouts": 0.,
            "batch_normalization": False,
            "output_activation": "relu",
        },
        "fridge": {
            "conv_dimensions": [32, 64],
            "enc_activation": "linear",
            "dec_activation": "relu",
            "conv_windows": [4, 4],
            "conv_strides": [1, 1],
            "conv_dropouts": 0.,
            "pooling_windows": [2, 2],
            "pooling_strides": [2, 2],
            "pooling_type": "max",
            "dense_dimensions": [512],
            "dense_activation": "relu",
            "dense_dropouts": 0.,
            "batch_normalization": False,
            "output_activation": "relu",
        },
        "microwave": {
            "conv_dimensions": [128, 256],
            "enc_activation": "linear",
            "dec_activation": "relu",
            "conv_windows": [4, 4],
            "conv_strides": [1, 1],
            "conv_dropouts": 0.,
            "pooling_windows": [2, 2],
            "pooling_strides": [2, 2],
            "pooling_type": "max",
            "dense_dimensions": [512],
            "dense_activation": "relu",
            "dense_dropouts": 0.,
            "batch_normalization": False,
            "output_activation": "relu",
        },
        "washer_dryer": {
            "conv_dimensions": [128, 256],
            "enc_activation": "linear",
            "dec_activation": "relu",
            "conv_windows": [4, 4],
            "conv_strides": [1, 1],
            "conv_dropouts": 0.,
            "pooling_windows": [2, 2],
            "pooling_strides": [2, 2],
            "pooling_type": "max",
            "dense_dimensions": [4096],
            "dense_activation": "relu",
            "dense_dropouts": 0.,
            "batch_normalization": False,
            "output_activation": "relu",
        },
    },
}

#     "configurations":     {
#         "washing_machine": {
#             "conv_dimensions": [16, 32],
#             "enc_activation": "linear",
#             "dec_activation": "relu",
#             "conv_windows": [8, 8],
#             "conv_strides": [1, 1],
#             "conv_dropouts": 0.,
#             "pooling_windows": [1, 1],
#             "pooling_strides": [1, 1],
#             "pooling_type": "max",
#             "dense_dimensions": [1024],
#             "dense_activation": "relu",
#             "dense_dropouts": 0.,
#             "batch_normalization": False,
#             "output_activation": "sigmoid",
#         },
#         "electric_oven": {
#             "conv_dimensions": [32, 32],
#             "enc_activation": "linear",
#             "dec_activation": "relu",
#             "conv_windows": [2, 2],
#             "conv_strides": [1, 1],
#             "conv_dropouts": 0.,
#             "pooling_windows": [2, 2],
#             "pooling_strides": [2, 2],
#             "pooling_type": "max",
#             "dense_dimensions": [256],
#             "dense_activation": "relu",
#             "dense_dropouts": 0.,
#             "batch_normalization": False,
#             "output_activation": "sigmoid",
#         },
#     }
# }

#     "configurations":    {
#         "fridge": {
#             "conv_dimensions": [2, 4],
#             "enc_activation": "linear",
#             "dec_activation": "relu",
#             "conv_windows": [2, 2],
#             "conv_strides": [1, 1],
#             "conv_dropouts": 0.,
#             "pooling_windows": [2, 2],
#             "pooling_strides": [2, 2],
#             "pooling_type": "max",
#             "dense_dimensions": [],
#             "dense_activation": "relu",
#             "dense_dropouts": [],
#             "batch_normalization": False,
#             "output_activation": "relu",
#         },
#         "heat_pump": {
#             "conv_dimensions": [2, 4],
#             "enc_activation": "linear",
#             "dec_activation": "relu",
#             "conv_windows": [2, 2],
#             "conv_strides": [1, 1],
#             "conv_dropouts": 0.,
#             "pooling_windows": [2, 2],
#             "pooling_strides": [2, 2],
#             "pooling_type": "max",
#             "dense_dimensions": [],
#             "dense_activation": "relu",
#             "dense_dropouts": [],
#             "batch_normalization": False,
#             "output_activation": "relu",
#         },
#     }
# }
